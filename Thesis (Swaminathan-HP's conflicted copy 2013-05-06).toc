\select@language {ngerman}
\select@language {english}
\select@language {english}
\contentsline {chapter}{Abstract}{ii}{chapter*.1}
\contentsline {chapter}{Acknowledgements}{iii}{chapter*.5}
\contentsline {chapter}{List of Tables}{vi}{chapter*.7}
\contentsline {chapter}{List of Figures}{vii}{chapter*.8}
\contentsline {chapter}{List of Acronyms}{viii}{chapter*.9}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Battery Management System (BMS)}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Cell Balancing}{1}{subsection.1.1.1}
\contentsline {section}{\numberline {1.2}PWM Signals for Cell Balancing}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Why PWM is required and how is it useful?}{3}{subsection.1.2.1}
\contentsline {section}{\numberline {1.3}Complexity in generating PWM}{3}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Complex Overlapping Signals}{3}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Computational effort}{3}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Power Consumption}{4}{subsection.1.3.3}
\contentsline {subsection}{\numberline {1.3.4}IO ports saturation}{4}{subsection.1.3.4}
\contentsline {subsection}{\numberline {1.3.5}Extensibility}{4}{subsection.1.3.5}
\contentsline {section}{\numberline {1.4}Contributions}{4}{section.1.4}
\contentsline {section}{\numberline {1.5}Organization}{5}{section.1.5}
\contentsline {chapter}{\numberline {2}Related Work}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Cell Balancing and PWM}{6}{section.2.1}
\contentsline {chapter}{\numberline {3}Idea and Architecture}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}Idea}{12}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}System Architecture}{12}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Hardware Architecture}{13}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}BMS Controller}{13}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}PWM Controller}{14}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Communication Bus}{14}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Cell Balancing HW}{15}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Software Architecture}{15}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Application Layer}{16}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Middleware Layer}{16}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Driver Layer}{17}{subsection.3.3.3}
\contentsline {chapter}{\numberline {4}Design and Implementation}{18}{chapter.4}
\contentsline {section}{\numberline {4.1}Overall System Design}{18}{section.4.1}
\contentsline {section}{\numberline {4.2}Hardware}{19}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}STM32E407 Olimex development board}{20}{subsection.4.2.1}
\contentsline {subsubsection}{Features}{20}{section*.26}
\contentsline {subsection}{\numberline {4.2.2}Microcontroller STM32F407ZGT6}{21}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Software}{21}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Command Structure}{22}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}PWM Channels}{22}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Application Layer}{23}{subsection.4.3.3}
\contentsline {subsubsection}{APIs}{23}{section*.29}
\contentsline {subsubsection}{Design APIs}{23}{section*.30}
\contentsline {subsubsection}{Implementation}{23}{section*.32}
\contentsline {subsubsection}{Implementation}{25}{section*.35}
\contentsline {subsubsection}{Implementation}{26}{section*.38}
\contentsline {subsubsection}{Implementation}{27}{section*.41}
\contentsline {subsection}{\numberline {4.3.4}Middleware Layer}{28}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}Encoding}{28}{subsection.4.3.5}
\contentsline {subsubsection}{Design}{28}{section*.43}
\contentsline {subsubsection}{Implementation}{29}{section*.45}
\contentsline {subsection}{\numberline {4.3.6}Decoding}{29}{subsection.4.3.6}
\contentsline {subsubsection}{Design}{29}{section*.46}
\contentsline {subsubsection}{Implementation}{29}{section*.47}
\contentsline {subsection}{\numberline {4.3.7}Storing the information}{30}{subsection.4.3.7}
\contentsline {subsubsection}{Design}{30}{section*.48}
\contentsline {subsubsection}{Implementation}{30}{section*.50}
\contentsline {subsection}{\numberline {4.3.8}Driver Layer}{31}{subsection.4.3.8}
\contentsline {subsection}{\numberline {4.3.9}PWM Initialization}{31}{subsection.4.3.9}
\contentsline {subsubsection}{Design}{32}{section*.51}
\contentsline {subsubsection}{Implementation}{32}{section*.52}
\contentsline {subsection}{\numberline {4.3.10}PWM Signals Generation}{32}{subsection.4.3.10}
\contentsline {subsubsection}{Design}{32}{section*.53}
\contentsline {subsubsection}{Implementation}{32}{section*.54}
\contentsline {subsection}{\numberline {4.3.11}Command Acceptance and Acknowledgement}{34}{subsection.4.3.11}
\contentsline {subsubsection}{Design}{34}{section*.56}
\contentsline {subsubsection}{Implementation}{34}{section*.58}
\contentsline {subsection}{\numberline {4.3.12}Stop PWM}{35}{subsection.4.3.12}
\contentsline {subsubsection}{Design}{35}{section*.59}
\contentsline {subsubsection}{Implementation}{36}{section*.61}
\contentsline {chapter}{\numberline {5}Results and Verifications}{38}{chapter.5}
\contentsline {section}{\numberline {5.1}Experimental Setup}{38}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Setting up the SPI}{39}{subsection.5.1.1}
\contentsline {section}{\numberline {5.2}Unit level}{40}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Sender}{40}{subsection.5.2.1}
\contentsline {subsubsection}{Methods}{40}{section*.65}
\contentsline {subsubsection}{Results and verifications}{40}{section*.66}
\contentsline {subsection}{\numberline {5.2.2}Receiver}{40}{subsection.5.2.2}
\contentsline {subsubsection}{Methods}{40}{section*.67}
\contentsline {subsubsection}{Results and verifications}{41}{section*.68}
\contentsline {subsection}{\numberline {5.2.3}Communication channel}{41}{subsection.5.2.3}
\contentsline {subsubsection}{Methods}{41}{section*.69}
\contentsline {subsubsection}{Results and verifications}{41}{section*.70}
\contentsline {section}{\numberline {5.3}System level}{42}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Methods}{42}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Results and verifications}{42}{subsection.5.3.2}
\contentsline {section}{\numberline {5.4}Conclusion and limitation}{43}{section.5.4}
\contentsline {chapter}{\numberline {6}Conclusion}{45}{chapter.6}
\contentsline {chapter}{Bibliography}{47}{section*.74}
