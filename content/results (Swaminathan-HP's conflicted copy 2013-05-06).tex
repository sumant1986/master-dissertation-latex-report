\chapter{Validation and Analysis}
\label{results}

This chapter presents and validates the results from the prototype implementation explained in chapter \ref{implementation}.


\noindent The results are grouped into two sections 
\begin{itemize}
\item Block level 
\item System level
\end{itemize}
In section \ref{blocklevel}, the output from each and every individual ESIC module such as voltage, current, temperature and cell balancing module are presented and analysed.  
In section \ref{systemlevel}, an analytical non-linear and a linear model for the cell balancing architecture is developed and validated with a system level case study. 

\section{Block level}
\label{blocklevel}
In this section, measurement results from the hardware implementation of the individual blocks of ESIC are presented and analysed.


\subsection{Voltage Measurement}
\label{volresult}
As seen in the design of the voltage measurement module in section \ref{voltagemeasurementmodule}, the individual cell voltages in the series connected battery pack are measured using an \acf{ADC} directly attached to each cell. 
To implement this in a hardware prototype, the internal 10 bit ADC of the ATmega328p microcontroller running on Arduino platform is used as explained in section \ref{voltageimp}. 
The design equation for the voltage calculation is as explained in equation \ref{cellout} and is programmed in the microcontroller. 

High power Lithium ion phosphate cells APR18650, manufactured by A123 systems are used for prototype implementation.
The cell nominal capacity and voltage are 1.1 Ah and 3.3 V, respectively. 
The recommended maximum and minimum operating voltages are from 3.6 V and 2 V, respectively. 
The reason for choosing Lithium ion phosphate cell is because of their very high power, long cycle life and superior abuse tolerance. 
Moreover, the use of the patented Nanophosphate technology  \cite{A123}, provides increased safety, even when the cell is operated at a very high C rate avoiding the risk of explosion.
The cell is connected to an external programmable DC electronic load N3300A from Agilent technologies \cite{Eload} to discharge the cell with a constant current. 
The Arduino is connected as shown in Figure \ref{ardvol}, with the positive node of cell connected to the Analogpin 1 and the negative node to the GND pin. 
The reference voltage of 5 V is provided through the Arduino board for conversion. 
The analog cell voltage is measured and converted into digital data by the Arduino. 
 
\noindent The experimental setup is as shown in Figure \ref{volexpsetup}. 
\begin{figure}[h!]
\centering
\includegraphics[width=9cm]{./Images/voltagemeasurehw.png}
\caption{\ac{Li-ion} cell connected to the internal ADC of the Arduino.}
\label{volexpsetup}
\end{figure}


The Arduino is programmed through a Computer via USB connection to convert the cell voltage into a digital data as per the equation \ref{cellout}. 
The output from the Arduino is displayed via HTerm serial port monitor in the computer. 
The voltage readings are plotted and the discharge characteristic of the cell is as shown in Figure \ref{volplot}. \\

\begin{figure}[h!]
\centering

\begin{tikzpicture}
\begin{axis}[
scale only axis,
xlabel={Time(s)},
ylabel={Voltage(V)},
]
\addplot [color = red] table[x = Time , y = Voltage] {./Images/voltageoutputcopy.txt};
\addplot [color = red] table[x = Time , y = Voltage] {./Images/voltageoutputcopy2.txt};
\end{axis}
\end{tikzpicture}

\caption[Discharge characteristic of Li-ion cell]{Output of voltage measurement module monitoring the discharge characteristic.}
\label{volplot}
\end{figure}


\noindent\textbf{Analysis}\\
The output from the voltage measurement module, monitoring the discharge characteristic is shown in Figure \ref{volplot}.
The cell was initially fully charged to a voltage of 3.3 V and then discharged with a constant current of 5.5 A i.e 5C for the cell, through the Agilent DC electronic load N3300A \cite{Eload}.  
The discharge test is stopped when the cell voltage reaches the minimum safe operating value of 2 V. 
With 1.1 Ah cell capacity and a discharge current of 5C, it would take 12 mins to completely discharge the cell as  

\begin{align*}
\text{Discharge time} &= \frac{\text{Capacity in Ampere-hour}}{\text{Dicharge Current in Ampere}}\\
&= \frac{1.1}{5.5}\\
&=0.2 \text{ hours}\\
&= 12 \text{ mins}
\end{align*}


Therefore to discharge upto a capacity of 20\% which is the safe operating limit of the cell, the time required is 

\begin{align*}
\text{Discharge time for 20\%} &= \frac{1.1 - 20\%}{5.5}\\
&= 9.6 mins
\end{align*}


This is shown in Figure \ref{volplot} where the cell voltage falls to 2 V nearly after 570 seconds which is 9.5 minutes. 
This proves that the \ac{ADC} implementation provides accurate cell voltage measurement.



\subsection{Current Measurement}
The battery current measurement is done by using Hall effect sensor as explained in section \ref{Hall}. 
The capacity of the cell chosen is 1.1 Ah, which is very low to analyse the sensor for high input currents.
Therefore to characterize the current sensor and to measure the accuracy, a high voltage DC power supply that could provide higher currents of upto 50 A is utilised. 
Agilent Technologies DC power supply N5764 with a rating of 20V, 76A and 1520W \cite{PS} is used in this experimental setup.
The DC electonic load of N3300A from Agilent Technologies is used as a load device for the power supply.
The current sensor is connected in series with the load to the power supply. 
The sensor output is connected to Analogpin of the Arduino and power to the sensor is provided directly from the 5V power supply of the Arduino board as shown in Figure \ref{halltransfer}. 
The experimental setup is shown in Figure \ref{curexpsetup}. 
\begin{figure}[h!]
\centering
\includegraphics[width=10cm]{./Images/currentmeasurementsetup.png}
\caption[ACS758 current sensor experimental setup]{ACS758 current sensor connected to Arduino with Agilent DC power supply and Electronic Load.}
\label{curexpsetup}
\end{figure} 


Initially, the sensor IN terminal is connected to the positive port of the power supply and the OUT terminal to the load for measuring the currents in the positive direction. 
Alternatively, the connections are reversed for measuring currents in the negative direction. 
The Arduino is programmed to calculate the input current value with the analog output voltage from the sensor, based on the equations explained in section \ref{currentmeas}. The measured output from the microcontroller is read using HTerm serial port monitor. 
Figure \ref{halloutput} shows the graph between Output Voltage Vs Input Current which is the transfer characteristic of the sensor. \\

\noindent\textbf{Analysis}\\
The sensor output is a linear function of the input current as seen from the Figure \ref{halloutput}. 
The offset voltage of the sensor is cancelled out by subtracting it from the output of the sensor. 
The experimental result complies with the sensor transfer characteristics provided in the datasheet \cite{AllegroDS}. 

\begin{figure}[h!]
\centering


\begin{tikzpicture}
\begin{axis}[
axis y line=center, axis x line=middle, xmin=-50, xmax=50, ymin=-2.5, ymax=2.5,
scale only axis,
xlabel={Current (A)},
ylabel={Hall sensor output(V)},
y label style={at={(0.25,1.12)}},
x label style={at={(1.32,0.45)}},
]
\addplot [color = red] table[x = Current , y = Voltage] {./Images/currentmeasurment.txt};
\end{axis}
\end{tikzpicture}

\caption{Transfer Characteristics of the Hall effect sensor.}
\label{halloutput}
\end{figure}



\subsection{Temperature Measurement}
The design of the temperature measurement sensor as explained in section \ref{temperaturemeasurementmodule} is an ongoing research topic and is not available as an off the shelf product for pototype development. 
Instead, a sensor with similar output configuration as that of the RTD, is used to test the system for measurement capability. 
Therefore in the prototypic implementation as explained in section \ref{tempimp}, a thermistor is used which has a similar output pattern of RTD. 
The design equations, circuit implementation and calculations required to find the temperature from the change in resistance value is discussed in section \ref{temperaturemeasurementmodule}. 

\acf{NTC} thermistor sensors from BETATHERM model 100K6A372I, with a nominal resistance of 100k$\Omega$ at 25$^\circ$C is used. 
Temperature test chamber ICH from memmert technologies \cite{memmert} is used to characterise the temperature sensor. 
The thermistor is placed inside the chamber and the leads from the sensor are connected to the Arduino board with a 100k$\Omega$ balancing resistance in series as shown in Figure \ref{voltagedivider}. 
A supply voltage of 5 V is provided through the Arduino board to the sensor and balancing resistor. 
Therefore at room temperature, the nominal resistance of the thermistor and the balancing resistor are same and the output voltage is exatly half of the supply voltage. 
For increasing temperatures, the voltage across the balancing resistor increases and vice versa. 
The temperature input could be set from the front panel control in the temperature chamber. 
The voltage across the balancing resistor is measured by the internal 10 bit ADC of the Arduino. 
The thermistor resistance is calculated using the equation \ref{therm}. 
The Arduino is programmed with the Stein-Hart equation \ref{SH} to calculate the input temperature based on the resistance value of the thermistor. 
Figure \ref{tempsetup} shows the temperature chamber used for the charcterisation of the thermistor sensor. 

\begin{figure}[h!]
\centering
\includegraphics[width=8cm]{./Images/IMG_03800.png}
\caption{Temperature chamber from memmert.}
\label{tempsetup}
\end{figure} 

The temperature is swept from 20 $^\circ C$ to 30 $^\circ C$ using the front panel controls and the resistance change of the thermistor is monitored through the Arduino. 
The output of the Arduino is read through HTerm serial port monitor and the temperature readings are plotted against the thermistor resistance as shown in Figure \ref{tempres}.\\ 

\noindent\textbf{Analysis}\\
The \ac{NTC} thermistor sensor as shown in Figure \ref{tempres} exhibits a decrease in resistance for increasing temperatures.
Also, the nominal resistance of the thermistor is 100 k$\Omega$ at room temperature is also shown in the Figure \ref{tempres}.  
Therefore, a sensor with similar output configuration like \acf{RTD} as explained in section \ref{RTDdesign}, with a \acf{PTC}, could be integrated with ESIC to measure the temperature of the cell.

\begin{figure}[h!]
\centering

\begin{tikzpicture}
\begin{axis}[
scale only axis,
xlabel={Resistance(k$\Omega$)},
ylabel={Temperature($^\circ C$)},
]
\addplot [color = red] table[x = Resistance , y = Temperature] {./Images/Tempoutput.txt};
\end{axis}
\end{tikzpicture}

\caption{Transfer characteristics of thermistor sensor.}
\label{tempres}
\end{figure}




\subsection{Cell Balancing}
\label{balresults}
Before building the prototype model, simulations in LTspice IV were performed as explained in section \ref{cellbalimp}. \\

\noindent\textbf{LTspice Simulation}\\
The basic building block of the novel proposed cell balancing architecture is shown in Figure \ref{cellbalancingcircuit}. 
The cells were considered as simple capacitors and the switches are taken as standard power MOSFETs from the library as shown in Figure \ref{serbatltspice}. 
A simulation is done for 3 cells connected in series, with the device parameters as designed and calculated in the section \ref{cellbalimp}.
The initial voltages of the cells are assumed to be 3.6 V , 3.4 V and 3.2 V for cell 1, cell 2 and cell 3 respectively. 
Scaling of the cell capacitance value is done to speed up the simulation process. 
According to the novel cell balancing architecture, cell 2 should be isolated from the charge transfer path with the isolation switches $M_s^2$ and $M_p^2$ and the balancing should take place between cell 1 and cell 3 through inductor $L^1$. 

\input{content/cellbalplot}

\noindent\textbf{Analysis}\\
Figure \ref{ltspiceres} shows the plot of the simulation for cell balancing.
It can be seen that the voltage of cell 1 is decreasing and that of cell 3 is increasing while the voltage of cell 2 is constant. 
This proves that charge from cell 1 is transferred to cell 3 by the novel active cell balancing circuit, without affecting cell 2. \\

 
\noindent\textbf{Prototype Validation}\\
High power Lithium ion phosphate cells from A123 system are used for charge balancing prototype. 
N-channel power MOSFETs PSMN1R6-30PL from NXP semiconductors are used as switches in the prototype. 
The MOSFETs are rated for 30 V and could carry a maximum current of 100 A. 
The low $R_{ds}$ value of 1.4m$\Omega$ makes them the ideal choice for a prototype implementation. 
The circuit is initially built in a breadboard for establishing the proof of working and then soldered into the prototyping board for real implementation. 
The prototype of a single cell level block is shown in Figure \ref{cellblock}.
 

\begin{figure}[h!]
\centering
\includegraphics[width=6.5cm]{./Images/celllevelblock.png}
\caption[Hardware prototype of cell level block]{Hardware prototype of single cell level block of the balancing architecture.}
\label{cellblock}
\end{figure} 

The cells with their balancing hardware are connected in series and the charge balancing algorithm is implemented via the Arduino as explained in section \ref{cellbalimp}. \\

\noindent\textbf{Analysis}\\
MSO-X-3024A Agilent Technologies Oscilloscope is used to monitor the complete cell balancing process. 
A screen shot for a single $PWM$ pulse from the scope is shown in Figure \ref{osc}.

\begin{figure}[h!]
\centering
\includegraphics[width=9cm]{./Images/scope0.png}
\caption{Screen shot from Oscilloscope for a single PWM pulse during balancing.}
\label{osc}
\end{figure} 

The yellow colour waveform is the voltage of cell 1 and the blue one shows the voltage of cell 2. 
In the middle, the inductor voltage and current waveforms are shown in green and pink colour, respectively. 
As seen from the Figure \ref{osc}, when the $PWM$ signal is \emph{ON}, cell 1 is discharged to the inductor and its voltage increases.
The polarity of the measurement is reversed and therefore the waveform for inductor voltage and current is also inverted. 
The inductor current ramps up linearly till the end of $T_{ON}$.
During $T_{OFF}$ the inductor discharges the stored energy to cell 2. 
Therefore, cell 2 is charged as seen in the blue waveform in the Figure \ref{osc}. 

Moreover, the output from the Arduino is read in a computer through the HTerm serial port monitor. 
The output voltage from both the cells are plotted with time as shown in Figure \ref{hardwareres}.\\

\input{content/hardwareres}
\noindent\textbf{Analysis}\\
On applying continuous $PWM$ pulses, the energy from cell 1 is transferred to cell 3 through the inductor. 
This is shown in Figure \ref{hardwareres}, where the voltage across cell 1 is decreasing as shown by the red line and the voltage across cell 3 is increasing as shown by the blue line. 
Also the voltage across cell 2 is unchanged and remains constant throughout the cell balancing process as shown by the green line. 
This proves that the hardware prototype model of the novel active cell balancing architecture, performs according to the specification.  

\section{System level}
\label{systemlevel}

The system level analysis of the novel active cell balancing circuit involves, the verification of the qualitative behaviour of the circuit in a real scenario, where nearly 100 cells are connected in series to form a real battery pack. 
This could be simulated using circuit simulators like LTspice IV, in which case 100's of the individual cell blocks as shown in Figure \ref{spiceckt} are connected in series to realise the battery pack. 
Apparently, modelling the battery pack in LTspice IV increases the computation complexity of the simulator. 
Also when working with high frequency $PWM$ signals, the numerical solver implemented in the LTspice IV has to perform huge computations in very small time steps, to accurately capture the behaviour of the circuit.
This is possible for a small group of cells connected in series, whereas in modelling a real battery pack consisting of 100 cells, the circuit simulators runs into numerical problems, as complex computations will load the memory of the system heavily. 
Hence, performing a transistor level analysis for a real battery pack is infeasible with analog circuit simulators. 
Therefore, it is necessary to design and develop an analytical model that is faster and scalable, that could also retain the accuracy while abstracting the transistor level circuit bahaviour to a system level.
This will also provide a platform to implement and test various system level charge routing algorithms, easily and quickly. 

For accurately analysing the circuit behaviour, the system configurations and working has to be observed, for both the $T_{ON}$ and $T_{OFF}$ phases of the $PWM$ signal seperately. 
The charge balancing circuit for both $T_{ON}$ and $T_{OFF}$ could be drawn seperately and analytical equations illustrating the operation could be developed. 
Figures \ref{equi1} and \ref{equi2} provides the equivalent circuit of the cell balancing architecture during $T_{ON}$ and $T_{OFF}$ periods of the $PWM$ signal.
The equivalent circuit does not depend upon whether the charge transfer occurs only between the adjacent cells or between non adjacent cells.
The only difference in both the cases would be the amount of series resistance $R_s$ or $R_d$, that has to be added to the model. This includes the inductor series resistance $R_L$, the cell series resistance $R_C$, and the ON-resistance of the MOSFET $R_M$ that comes in the charge transfer path.
The contributions to $R_s$ are summarized in Table~\ref{tab:RLookup}.

\input{content/resistancetable}
\input{content/equations}


Both the non linear and the linearized models as explained above are significantly faster than transistor-level simulations in LTspice IV and provides sufficient accuracy as shown in the Table \ref{comparelinandspice}. 



\begin{table}[h!]
\begin{center}
\begin{tabular}{ccccc}\toprule
&$\bm{T_{ON}}$ $[ms]$  & $\bm{T_{OFF}}$ $[ms]$ & $\bm{q_s}$ $[As]$ & $\bm{q_d}$ $[As]$ \\ \midrule\midrule
\text{SPICE} &$0.12539$ &$0.16582$ &$3.14\tx{e-4}$ & $4.14\tx{e-4}$\\
\midrule
\text{linear}& $0.125$ &$0.16442$ & $3.1447\tx{e-4}$ & $4.1102\tx{e-4}$ \\ 
\text{rel. error} & $0.3\%$& $1\%$&$0.1\%$ & $0.7\%$\\
\midrule
\text{nonlinear} & $0.12546$&$0.16552$ & $3.1371\tx{e-4}$& $4.1288\tx{e-4}$ \\
\text{rel. error} &$0.05\%$ &$0.2\%$ &$0.1\%$ & $0.2\%$\\ \bottomrule
\end{tabular}
\caption[Comparison of the analytical model to the SPICE simulation]{Comparison of the linear and nonlinear analytical model to the SPICE simulation \protect\cite{ourpaper}.}
\label{comparelinandspice}
\end{center}
\end{table}

\input{content/casestudy} 

\section{Summary}
Thus the results from the implementation of the individual blocks explained in section \ref{implementation} are analysed. 
The results satisfy the expected accuracy for each block and provides an insight for the possibility of integration into ESIC. 
Also, the system level analysis with the linear and non linear model provide, more perception into the behaviour of the system to the control algorithm as analysed with the case study. 
With these results it could be finalised that the system performance is more efficient and accurate, satisfying all the specification of the \acf{DBMS}. 





