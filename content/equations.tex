
\subsection{Non-linear model}
\label{systemmodel}

The analytical model of the system could be split into two parts $T_{ON}$ and $T_{OFF}$ separately. \\


\noindent\textbf{Circuit Behaviour during $\mathbf \Ton$}\\
Applying \textsc{Kirchhoff}'s voltage law to the $T_{ON}$ equivalent circuit in Figure \ref{equi1} results in

\begin{equation}\label{kon1} 
L^1 \cdot \frac{\diff}{\diff t}i + R_s \cdot i + \frac{1}{C_{B^1}}\int_0^\Ton i(\tau) \, \diff \tau - V_{B^1} = 0
\end{equation}

\noindent where $L^1$ is the value of inductance, $C_{B^1}$ is the capacitance of the cell $B^1$ and $R_s$ is the sum of all the resistances that come in the charge transfer path during $T_{ON}$. $V_{B^1}$ is the initial voltage of the cell $B^1$. 
Differentiating the above equation would provide a second order differential equation as 

\begin{equation}\label{kon2}
L^1\cdot \frac{\diff^2}{\diff t^2} i+ R_s \cdot \frac{\diff}{\diff t}i + \frac{1}{C_{B^1}} \cdot i = 0
\end{equation}

\noindent To solve the above second order differential equation, two initial conditions are required as follows: \\

\noindent \textbf{Initial condition 1:}\\
We assume the inductor is fully discharged initially and therefore

\begin{equation}\label{init1}
i(0) = i_0 = 0
\end{equation}

\noindent\textbf{Initial condition 2:}\\
Applying \ref{init1} to equation \ref{kon1} would result in the second initial condition as follows:

\begin{equation}
L^1 \cdot \frac{\diff}{\diff t} i(0) + 0 + 0 - V_{B^1} = 0
\end{equation}

\noindent from that

\begin{equation} \label{init2} 
\frac{\diff}{\diff t}i(0) = \diff i_0 = \frac{V_{B^1}}{L^1} 
\end{equation}



\noindent\textbf{Circuit behavior during $\mathbf \Toff$}.\\
Similarly, applying \textsc{Kirchhoff}'s voltage law to the $T_{OFF}$ equivalent circuit shown in Figure \ref {equi2} results in
\begin{equation}\label{koff1}
L^1 \cdot \frac{\diff}{\diff t}i + R_d \cdot i + \frac{1}{C_{B^1}}\int_0^\Toff i(\tau) \, \diff \tau + V_d + V_{B^2} = 0
\end{equation}

\noindent with $R_d$ being the resistance along the charge transfer path during $T_{OFF}$.

\noindent Upon differentiating it would result in a second order differential equation similar to equation \ref{kon2}:

\begin{equation}\label{koff2}
L^1\cdot \frac{\diff^2}{\diff t^2} i+ R_d \cdot \frac{\diff}{\diff t}i + \frac{1}{C_{B^1}} \cdot i = 0
\end{equation}

\noindent The initial conditions for solving this second order differential equation are as follows: \\

\noindent\textbf{Initial condition 1:}\\
The initial value of current is the same as when it was during the end of $T_{ON}$ and therefore

\begin{equation}\label{initoff1}
i(0) = i_0 = i(T_{ON})= i_{peak}
\end{equation}

\noindent\textbf{Initial condition 2:}\\
Applying the above initial condition to equation \ref{koff1} yields the second condition as

\begin{equation}
L^1 \cdot \frac{\diff}{\diff t} i(0) + R_d \cdot i_{peak} + 0 + V_d + V_{B^2} = 0
\end{equation}

\begin{equation} \label{initoff2} 
\frac{\diff}{\diff t}i(0) = \diff i_0 = - \frac{V_{B^2} + V_d + R \cdot i_{peak}}{L^1} 
\end{equation}

\noindent\textbf{{Solution for Differential equations}}\\
To solve the differential equations \ref{kon2} and \ref{koff2} mentioned above, a general equation representation such as

\begin{equation}\label{secgen}
L\cdot \frac{\diff^2}{\diff t^2} i+ R \cdot \frac{\diff}{\diff t}i + \frac{1}{C} i =  0 
\end{equation}

\noindent with initial conditions 
\begin{equation}\label{initconds}
i(0) = i_0 \hspace{1cm} \frac{\diff}{\diff t}i(0) = \diff i_0
\end{equation}

\noindent is assumed. 
The solution for such a second-order system is detailed for instance in control engineering literature such as \cite{ogata2002modern}.
Following these procedures, the solution to the second order differential equation \ref{secgen} is:
\begin{equation}
\frac{\diff^2}{\diff t^2} i+ \frac{R}{L} \cdot \frac{\diff}{\diff t}i + \frac{1}{LC} i = 0
\end{equation}
\begin{equation}\label{geneq}
\frac{\diff^2}{\diff t^2} i+ 2\xi\omega_n \cdot \frac{\diff}{\diff t}i + \omega_n^2 \cdot i = 0
\end{equation}

\noindent The system behaviour is largely dependent upon its natural frequency $\omega_n$ and its damping ratio $\xi$. 
In case of the proposed novel active cell balancing circuit, $\omega_n$ and $\xi$ are given by the following equations:
\begin{align}
\omega_n = & \frac{1}{\sqrt{LC}} & \xi = & \frac{R}{2} \sqrt{\frac{C}{L}} 
\end{align}
The characteristic equation of the second order differential equation, whose roots lead to the general solution of the system, could be obtained by introducing a variable $s$ into the equation \ref{geneq}. 
This results into
\begin{equation}\label{chareq}
s^2+ 2\xi\omega_n \cdot s + \omega_n^2
\end{equation}
The roots of the characteristic equation \ref{chareq} are
\begin{equation}\label{charroot}
s_{1/2} = - \xi \omega_n \pm \omega_n \sqrt{\xi^2 - 1}
\end{equation}
and therefore the general time domain solution of the second order system in equation \ref{secgen} is therefore given by
\begin{equation}
i(t) = \gamma_1 e^{s_1 t} + \gamma_2 e^{s_2 t} 
\end{equation}
Solving for the constants $\gamma_1$ and $\gamma_2$, using the initial values from equation \ref{initconds} yields the following relation
\begin{align}\label{shortbigsol} 
i(t) = & \frac{\diff i_0 - i_0 s_2}{s_1 - s_2} e^{s_1 t} - \frac{\diff i_0 - i_0 s_1}{s_1 - s_2} e^{s_2 t} 
\end{align}
Using equation \ref{charroot}, this could be further reformulated to
\begin{align}
i(t) =  e^{-\xi \omega_n t} \cdot \bigg\{ & \frac{\diff i_0 - i_0 (-\xi \omega_n - \omega_n \sqrt{\xi^2-1})} {2\omega_n \sqrt{\xi^2-1}} e^{(\omega_n \sqrt{xi^2-1})t} \notag \\
 - & \frac{\diff i_0 - i_0 (-\xi \omega_n + \omega_n \sqrt{\xi^2-1})} {2\omega_n \sqrt{\xi^2-1}} e^{-(\omega_n \sqrt{xi^2-1})t} \bigg\} 
 \label{bigsolution}
\end{align}

\noindent From equation \ref{bigsolution}, the system model could be classified into three types based on the values of $\xi$ as 
\begin{itemize}
\item Under-damped ($\xi<1$)
\item Critically damped ($\xi=1$)
\item Overdamped ($\xi>1$)
\end{itemize}

\noindent\textbf{Under-damped}\\
The system is said to be under-damped if $\xi < 1$. This results in the roots of the characteristic equation to be complex as 
\begin{equation}
s_{1/2} = - \xi \omega_n \pm j\omega_n \sqrt{ 1 - \xi^2}
\end{equation}
The under-damped system produces a resonating signal that is slowly damped away.
Using this, the equation \ref{shortbigsol} is transformed as
\begin{align}
i(t) = &  e^{-\xi \omega_n t} \cdot \big\{ i_0 \cos(\omega_n \sqrt{1-\xi^2} t) \notag \\ 
& + \frac{\diff i_0 + i_0 \xi \omega_n}{\omega_n \sqrt{1-\xi^2}} \sin(\omega_n \sqrt{1-\xi^2} t) \big\} \label{underdampsol}
\end{align}
Abstracting $i(t) = e^{-ct} \big( A\cos(at) + B\sin(at) \big)$, will help in integrating the equation \ref{underdampsol} to obtain the transferred charge $q$ with time T as
\begin{align}
q(T) = & \frac{-A}{a^2 + c^2} \Big[c(e^{-Tc}\cos(Ta) -1) - ae^{-Tc} \sin(Ta) \Big] \notag \\
- & \frac{B}{a^2 +c^2} \Big[a(e^{-Tc} \cos(Ta) -1 ) + ce^{-Tc} \sin(Ta) \Big]
\end{align}
For the charging phase, the time $T$ is calculated assuming $t=\Ton$ and for discharging the time $t=\Toff$ is calculated when the inductor current is zero as
\begin{align}
 & 0 =  A \cos(aT_{OFF}) + B \sin(aT_{OFF}) \notag \\
 & -\frac{A}{B} = \frac{\sin(aT_{OFF})}{\cos(aT_{OFF})} \notag \\
 &  T_{OFF} = - \frac{1}{a} \cdot \arctan (\frac{A}{B})
\label{underdampToff}
\end{align}
Since there is no closed-form solution for $\Ton$ in the nonlinear model, a short binary search using the nonlinear model could be used to improve the accuracy of the estimation.
Figure \ref{currentunderdamped} gives an impression on how $i_{\xv{send}}$ and $i_{\xv{recv}}$ behave and how small $\Ton$, $\Toff$ are relatively to the time constants of the sine waves.\\
\begin{figure}[h!]
\centering 
\currentplot  {tikz/plotdata/underdampedtxlong.txt}{tikz/plotdata/underdampedtxshort.txt}{tikz/plotdata/underdampedrxlong.txt}{tikz/plotdata/underdampedrxshort.txt}
\caption[Current plots for the under-damped case]{Current plots for the under-damped case; upper row: charging current $\bm i_{\protect\xv{sendf}}$; lower row: discharging current $\bm i_{\protect\xv{recvf}}$ \protect\cite{ourpaper}.}
\label{currentunderdamped}
\end{figure}

\noindent\textbf{Over-damped}\\
For $\xi > 1$, the roots of the characteristic equation are real and the system is said to be over-damped.
Over-damping on the other hand is not resonating, but reaches very slowly its equilibrium.
Therefore, if $s_{1/2} \in \mathbb{R}$, then $i(t)=e^{-ct} \big[ A e^{at} - Be^{-at}]$ could be abstracted from equation \ref{bigsolution}.
Integration of the current would provide the transferred charge as
\begin{align}
q(T) = &\frac{B}{a+c} \Big[e^{- Ta - Tc}- 1\Big]+ \frac{A}{a-c} \Big[e^{Ta - Tc}- 1\Big]
\end{align}
$\Toff$ could be calculated as
\begin{align}
& 0 =  A e^{aT_{OFF}} - B e^{-aT_{OFF}} \notag \\
& \log(e^{aT_{OFF}}) =  \log(\frac{B}{A} e^{-aT_{OFF}}) = \log( \frac{B}{A}) - aT_{OFF} \notag \\
& 2aT_{OFF} = \log(\frac{B}{A})\\
& T_{OFF} = \frac{1}{2a} \cdot \log(\frac{B}{A})
\label{Toffoverdamped}
\end{align}

As illustrated in Figure \ref{currentoverdamped}, it can be seen that the damping has only little influence under the time frame that is of interest.\\

\begin{figure}[h!]
\centering
\currentplot{tikz/plotdata/overdampedtxlong.txt}{tikz/plotdata/overdampedtxshort.txt}{tikz/plotdata/overdampedrxlong.txt}{tikz/plotdata/overdampedrxshort.txt}
\caption[Current plots for the over-damped case]{Current plots for the over-damped case; upper row: charging current $\bm i_{\protect\xv{sendf}}$; lower row: discharging current $\bm i_{\protect\xv{recvf}}$ \protect\cite{ourpaper}.}
\label{currentoverdamped}
\end{figure}



\noindent\textbf{Critically-damped}\\
If $\xi=1$, the system is said to be critically-damped. 
The signal from a critically damped system resonates exactly once and is then damped away.
This is the fastest way to reach the equilibrium and critical damping is therefore used as a design methodology in certain situations.
The roots of the characteristic equation are co-located and the solution of current therefore becomes
\begin{align}\label{criticalsol}
i(t) = (A+ Bt) e^{-ct}
\end{align}
with $A = i_0$, $B=\diff i_0 + \omega_n i_0$, $c=\omega_n$.
Figure \ref{currentcriticaldamped} shows the corresponding plots of the charging and discharging current.
Again, the behaviour during short intervals of time does not differ much from the other cases.
Integrating $i(t)$ yields the transferred charge as in the other cases
\begin{equation}
%q(T) = B*(1/(c**2) - (s.exp(-T*c)*(T*c + 1))/(c**2)) - (A*(s.exp(-T*c) - 1))/c
q(T) = \frac{B}{c^2} \big( 1- (Tc + 1)e^{-Tc} \big) - \frac{A}{c} \big( e^{-Tc} -1 \big)
\end{equation}
The calculation of $\Toff$ is done with equation \ref{criticalsol} as follows 
\begin{align}
0  = & A+Bt \notag \\ 
t = & \frac{-A}{B}
\end{align}


\begin{figure}[h!]
\centering
\currentplot{tikz/plotdata/critdampedtxlong.txt}{tikz/plotdata/critdampedtxshort.txt}{tikz/plotdata/critdampedrxlong.txt}{tikz/plotdata/critdampedrxshort.txt}
\caption[Current plots for the critically-damped case]{Current plots for the critically-damped case; upper row: charging current $\bm i_{\protect\xv{sendf}}$; lower row: discharging current $\bm i_{\protect\xv{recvf}}$ \protect\cite{ourpaper}.}
\label{currentcriticaldamped}
\end{figure}

%\begin{figure}[h!]
%\centering
%\includegraphics[scale = 1.1]{./Images/tikzexternal/DBMS_Chl_externalize-figure10.pdf}
%\caption{Current plots for the critically-damped case;upper row: charging current $\bm i_{\protect\xv{sendf}}$; lower row: discharging current $\bm i_{\protect\xv{recvf}}$ \cite{ourpaper}.}
%\label{currentcriticaldamped}
%\end{figure}



Thus, for a shorter time frame, the values of $T_{ON}$ and $T_{OFF}$ does not differ very much for the three different cases described above. 
Therefore, a linearised model for the system behaviour could be developed, as explained in section \ref{linearmodel} to simplify the calculations. 
This linear model is simple to compute and easy to analyse, compared to the second order differential equation model, which is very time consuming and tedious to calculate.
Moreover, the optimization and control algorithms could be easily and efficiently implemented if the system model is simple and easy to compute.  
As the values of $T_{ON}$ and $T_{OFF}$ does not vary much for a shorter time frame, the linear model could be used to implement the system behaviour without compromising on the accuracy of the system. 


\subsection{Linear model}
\label{linearmodel}
A linear model for the system could be visualised by considering the inductor current to be linear instead of exponential. 
This assumption is valid as seen in the Figures \ref{currentunderdamped}, \ref{currentoverdamped}, \ref{currentcriticaldamped} where the inductor current is linear for a short interval of time. 
Therefore, the resulting current could be modelled as
\begin{align}
i_s(t) = & \frac{V_{B^1}}{L} \cdot t \notag \\
i_d(t) = & \ipeak - \frac{V_{B^2} + V_d + R_d\ipeak}{L} \cdot t \label{eq:LinI}
\end{align}

\noindent From this, the charge transferred during both $T_{ON}$ and $T_{OFF}$ could be computed by integrating the above equation as follows
\begin{align}
q_s(T) & = \int_0^T i_s(\tau) \diff \tau = \int_0^T \frac{V_{B^1}}{L} \cdot \tau \diff \tau \notag \\
& = \frac{V_{B^1}}{L} \cdot \frac{T^2}{2} \\
q_d(T) & = \int_0^T \Big[ \ipeak - \frac{V_{B^2} + V_d + R_d\ipeak}{L} \cdot \tau \Big] \diff \tau \notag \\
& = \ipeak T - \frac{V_{B^2} + V_d + R_d \ipeak}{L} \cdot \frac{T^2}{2}
\end{align}


The values of $T_{ON}$ and $T_{OFF}$ could be computed directly from equation \ref{eq:LinI} as follows 

\begin{align}
\Ton = & \ipeak \cdot \frac{L}{V_{B^1}} \notag \\
\Toff = & \ipeak \cdot \frac{L}{V_{B^2} + V_d + R_d\ipeak} \notag
\label{eq:TonToff:linear} 
\end{align}




















%\begin{figure}[h!]
%\centering
%\currentplot{tikz/plotdata/critdampedtxlong.txt}{tikz/plotdata/critdampedtxshort.txt}{tikz/plotdata/critdampedrxlong.txt}{tikz/plotdata/critdampedrxshort.txt}
%\caption{Current plots for the critically damped case; upper row: charging current $\bm i_{\protect\xv{sendf}}$; lower row: discharging current $\bm i_{\protect\xv{recvf}}$.}
%\label{currentcriticaldamped}
%\end{figure}


%Since our approach ends the system signals prematurely, all three cases can be handled and do in fact barely differ on the relevant time scale as we will see in the following sections.
%PDF figure for overdamped
%\begin{figure}[h!]
%\centering
%\includegraphics[scale = 0.9]{./Images/tikzexternal/DBMS_Chl_externalize-figure10.pdf}
%\caption{upper row: charging current $\bm i_{\protect\xv{sendf}}$; lower row: discharging current $\bm i_{\protect\xv{recvf}}$ \cite{ourpaper}.}
%\label{currentoverdamped}
%\end{figure}
%PDF figure for underdamped
%\begin{figure}[h!]
%\centering
%\includegraphics[scale = .9]{./Images/tikzexternal/DBMS_Chl_externalize-figure9.pdf}
%\caption{upper row: charging current $\bm i_{\protect\xv{sendf}}$; lower row: discharging current $\bm i_{\protect\xv{recvf}}$ \cite{ourpaper}.}
%\label{currentunderdamped}
%\end{figure}