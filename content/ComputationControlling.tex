\section{Computation and Controlling Module}
\label{Computation}
The computation module is the core intelligence of the \ac{ESIC}. 
This receives inputs from voltage, current and temperature measurement modules and calculates the \acf{SOC}, \acf{SOH} and \acf{RUL} of the cell. 
The computation module interacts with other \acp{ESIC} in the architecture through a communication module to get the status of the other smart cells.
This also calculates the driving range that could be covered by the charge left inside the cell. 
Moreover the cell ageing is monitored by the computation module which helps in reducing the maintenance cost of the battery pack. 

The controlling module is responsible for operating the cell within its \acf{SOA}. 
As explained in chapter \ref{Introduction} \ac{Li-ion} cell gets damaged if the cell is operated beyond the safe operating boundary voltages. 
Therefore control functions like \ac{OVP} or \ac{UVP} are performed depending on whether the cell voltage is higher or lower than the safe operating limits respectively.
Also, the cell temperature is controlled within its operating range, in order to increase the efficiency and also to prevent the cell from going into thermal runaway.

\subsection{Related Work}
\noindent\textbf{\acf{SOC} Estimation}\\
State of Charge is defined as the amount of capacity remaining in the cell. \ac{SOC} is an important parameter to be measured as it determines the driving range that could be achieved by the vehicle. 
Moreover cell balancing is also performed based on the difference in the value of \ac{SOC} between the cells \cite{Speltino}. 

A good comparison between various methods of \ac{SOC} estimation is done in \cite{SOCcomparison}. 
The most reliable and simple method for \ac{SOC} estimation is the discharge test method, which calculates \ac{SOC} based on a discharge test performed under controlled conditions. 
This method would be suitable only during maintenance and initial test as it is very time consuming and requires the cell to be removed out of the battery pack \cite{SOCcomparison}. 
Another method uses the integral value of current flowing in and out of the cell to calculate the \ac{SOC} of the cell. 
This method is called coulomb counting and is very simple and accurate. 
With a known initial \ac{SOC} value ($SOC_0$) the final \ac{SOC} could be calculated by integrating the current over a specified amount of time. 
The drawbacks of this method are the accumulation of current measurement errors due to integration and losses in the battery that results in different charging and discharging current densities \cite{zhengBo}.  

Another method of estimating \acf{SOC} is by means of calculating the \acf{OCV} of the cell. 
The \ac{SOC} depends upon the \ac{OCV} of the cell and could be calculated by using a look-up table. 
Although, this method is simple and easy to implement, this requires sufficient rest periods between the measurements because of the $IR$ drop developed due to the internal cell resistance.
Therefore, this method is not suitable for continuous time monitoring \cite{zhang}. 
The internal impedance of the cell is first measured by excitation with an AC source as in \cite{Willihnganz}. 
This triggered another \ac{SOC} estimation technique for Lead acid and \ac{$NiCd$} cells based on impedance spectroscopy as proposed in \cite{Kunihiro}, \cite{Huet199859}. 
The same method is also employed with fuzzy logic controls for estimating the \ac{SOC} in \ac{Li} cells \cite{Salkind1999293}. 
Artificial neural networks as explained in \cite{ehret1999state}, are also used for \ac{SOC} estimations. 
The drawback of these methods is that they require extensive training and a huge set of training data from a similar cell.

The most widely used technique for \ac{SOC} estimation is the Kalman filter method \cite{Spagnol}. 
This method utilises a cell model to calculate the \ac{SOC}. 
For a known current and temperature value, the cell voltage is measured and compared with the cell voltage from the model. 
The error is reduced by Kalman filtering and the \ac{SOC} is calculated as a function of cell voltage. 
Hybrid estimation techniques combining Kalman filtering with other \ac{SOC} estimation methods like coulomb counting is presented in \cite{Codeca}. 
Analysis and design of the \ac{EKF} is presented in a series of three papers \cite{Plett1},\cite{Plett2},\cite{Plett3}.
In \cite{Plett1} requirements for complex methods of \ac{SOC} estimation algorithms are investigated and presented. 
A suitable model of the cell for the EKF algorithm is developed and tested in \cite{Plett2}. 
Finally, the application of the EKF method in a BMS to estimate the \ac{SOC} is presented in detail in \cite{Plett3}.\\


\noindent\textbf{\acf{SOH} Estimation}\\ 
The \acf{SOH} is defined as the ratio of a cell's current capacity to the capacity of the cell when it was new. 
It is represented as a percentage value. 
\ac{SOH} is the figure of merit that depicts the life of the cell and also provides information regarding the condition of the cell whether it is able to perform according to the specification or not. 
It also helps to determine the replacement strategy of the cell, which will reduce the maintenance cost of the battery pack.
The SOH of the cell decreases because of the capacity fading effect taking place in the cell. 
This could be due to the loss of carrier concentration or may be due to the growth of the cell's internal impedance.
Changes in operating conditions of the cell like charging and discharging current, cycle number, \ac{SOC} variations and temperatures effect capacity fading \cite{Qing}. 

Various methods are proposed in literature for estimating the SOH of the cell.
In \cite{pop2007} methods like full or partial discharge test are performed to predict the SOH value.
These methods are expensive and time consuming.
The full discharge test method even damages the cell and the partial discharge test method do not provide accurate results. 
Furthermore, the voltage recovery method which measures the reduction in voltage when a load is connected to the cell is not applicable because it do not provide real time estimation and requires the cell to be removed from the battery pack \cite{Band}.
Impedance spectroscopy methods involve application of a small AC input pulse to the cell and measuring the response of the cell. 
With that, the impedance of the cell is calculated and compared with the impedance value when it was new. 
The resulting deviation denotes the SOH of the cell \cite{Spectro}, \cite{Band}. 
These methods are not precise and are not suitable for a real time monitoring as they require sophisticated monitoring equipments like a \ac{DAQ} \cite{Huet}, \cite{Bose}. 
Fuzzy realization methods, as explained in \cite{Fuzzy}, involve complex training and huge test data for estimation. 
Methods like impulse response \cite{Banaei} and model based approaches \cite{model} could be employed in the ESIC for SOH prediction.\\

\noindent\textbf{Remaining Useful Life (RUL) Estimation}\\ 
\ac{RUL} is an indication of the life of the cell.
It provides the user with information regarding the remaining capacity left inside the cell, before it becomes non-functional and a replacement activity should be made. 
This provides increased safety and also helps to plan the maintenance schedules. 
Over-prediction of RUL will lead to safety issues, because the cell would have lost its performance and using it further would lead to permanent failures and explosion hazards. 
Under-prediction wastes the active energy inside the cell and increases the cost of the battery pack. 
Therefore, a BMS system with an accurate RUL calculation algorithm will help in evaluating the performance of the cell and also with replacement planning. 
Moreover, it increases the safety of the battery pack by preventing the cells from being utilised beyond their life time.

\ac{PHM} of the battery has been a topic of research in the recent past. 
The \ac{RUL} of the cell could be calculated either by data driven methods or by model based methods.
Data driven approaches involve pattern recognition and machine learning to detect the changes in the system state \cite{tse1999prediction}.
Variations to the data driven methods like AutoRegressive model \cite{de1991analysis},\cite{liulithium},  bilinear model \cite{rao1981theory} are proposed in literature.   
They all predict the future states of the cell by analysing the patterns of degradation of a similar cell. 
This type of analysis would be beneficial if the cell model is not available or the model is too complex and computationally intensive. 
However, the data driven methods are not accurate because they do not consider the actual working conditions of the cell like temperature, voltage, charging and discharging currents etc.
Also, the collection and storage of huge amount of data is time consuming and expensive \cite{Penna}.

The model based method \cite{Patti}, \cite{Goebel}, \cite{uckun}  utilises a more detailed cell model to calculate the \ac{RUL} of the cell. 
This approach considers the operating conditions of the cell and updates the state variables of the model accordingly to calculate the life of the cell. 
Also this method could be easily verified and certified \cite{Penna}. 
Furthermore, this approach is beneficial because the same cell model could also be used for the prediction of the \ac{SOC} and SOH as explained in sections \ref{stateofcharge}, \ref{stateofhealth}. 
  
\subsection{\acf{SOC}}
\label{stateofcharge}
\acf{EKF} for the estimation of \acf{SOC} of the cell as proposed in \cite{Plett1}, \cite{Plett2} and \cite{Plett3} is preferred in this thesis. 
\ac{EKF} is an elegant and powerful computational solution for \ac{SOC} estimation. 
It consists of a set of recursive equations that are evaluated continuously to calculate the model parameters. 
Therefore for \ac{SOC} estimation, the cell is represented in terms of state space form. 
The values of the states are the internal parameters of the cell and are calculated from an accurate cell model. 
Different types of cell models, could be used in conjunction with the \ac{EKF}. 
They are 
\begin{itemize}
\item athematical model
\item Electrical model
\item Electrochemical model
\end{itemize}
The mathematical model represents the cell in terms of equations. 
These are very simple to calculate but they lack accuracy in \ac{SOC} estimation. 
The electrochemical model includes the chemical reactions that occur in each cell and they are highly accurate. 
But these models are very complex and time consuming in calculation. 
The appropriate model for implementation would be the electrical model which produces accurate results comparable to the electrochemical model and is computationally simple. 
A second order \acf{Li-ion} cell model commonly called as Randle's model is shown in Figure \ref{2cellmodel}, where $V_{OCV}(SOC)$ represents the open circuit voltage as a function of \ac{SOC}, $R_{O}$ the internal resistance of the cell, $R1, C1$ and $R2, C2$ represents the cell dynamics.  

\begin{figure}[h!]
\centering
\includegraphics[width=12cm]{./Images/cellmodel.pdf}
\caption[Randle's second order \ac{Li-ion} electrical cell model]{Randle's second order \ac{Li-ion} electrical cell model \protect\cite{cellmodel}.}
\label{2cellmodel}
\end{figure}

The initial values of these parameters are found by performing discharge tests under controlled condition. 
This model is loaded into the computation core of the ESIC. 
The ESIC then uses the measured value of current and temperature as input and determines the terminal voltage from the model. 
The output from the model and the measured cell voltage from the voltage measurement module are fed into the Kalman filter for estimating the \ac{SOC} of the cell. 
The values of the model state space vector are also adjusted from the Kalman filter's output. 
\ac{SOC} estimation is explained in detail in \cite{Spagnol}. 

\subsection{\acf{SOH}}
\label{stateofhealth}
The healthy state of the battery is defined by the \acf{SOH} value.
It is the ability of the cell to withstand a certain charging current or provide a required discharging current. 
In order to evaluate the \ac{SOH} of the cell, a term called \ac{EOL} of the cell has to be defined. 
\ac{EOL} is defined as the point when the cell's maximum power is reduced to 60\% of its initial maximum power under the same conditions of temperature and \ac{SOC}.
The maximum charging current of a cell is determined by

\begin{align}
I_{max}(charging) = \frac{V_{max} - OCV}{R_{int}} 
\label{chargeeq}
\end{align}
where $V_{max}$, $OCV$ and $R_{int}$ are the maximum voltage limit, open circuit voltage and internal resistance of the cell respectively. 
Similarly, the maximum discharging current of a cell is given by 
\begin{align}
I_{max}(discharging) = \frac{OCV - V_{min}}{R_{int}} 
\label{dischargeeq}
\end{align}
where $V_{min}$ is the minimum voltage limit of the cell. 

From equations \ref{chargeeq} and \ref{dischargeeq} it is clear that the internal resistance of the cell is the factor which determines the maximum charging and discharging currents of the cell. 
Therefore an increase in the internal resistance by a value of 160\% would give the EOL of the cell. 
With that, the SOH of the cell could be defined in terms of internal resistance of the cell as 
\begin{align}
SOH = \frac{R_{EOL} - R}{R_{EOL} - R_{new}} \cdot 100\%
\label{SOHeq}
\end{align}
where $R_{EOL}$, $R$ and $R_{new}$ are the resistance of the cell when it is at EOL, present and when it was new, respectively. 
Therefore, measuring the cell's internal resistance would yield the SOH value. 

The block diagram of the SOH prediction algorithm inside the ESIC is shown in Figure \ref{SOH}. 
The charging current is simultaneously fed into the cell and the cell model. 
The cell model is shown in Figure \ref{2cellmodel}. 
The cell voltage measured from the voltage sensor module and the voltage output from the model is compared. 
The difference is fed into the \acf{EKF} block to calculate the internal resistance of the cell and this value is compared with the cell ageing data by the SOH calculation module. 
The SOH value is calculated based on equation \ref{SOHeq} \cite{model}.

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.25]{./Images/SOHprediction.pdf}
\caption[Block diagram for SOH prediction of the cell]{Block diagram for SOH prediction of the cell \protect\cite{model}.}
\label{SOH}
\end{figure}

\subsection{\acf{RUL}}
A model based approach for RUL estimation is preferred in this thesis \cite{Patti}. \\

\noindent\textbf{Power fade}\\
Power fade is defined as the loss of cell power due to ageing. This loss is because of the increase in the resistance of the cell as shown by 
\begin{align}
P = \frac{V^{2}}{R}
\end{align} 
where $P$ is the power, $V$ is the voltage of the cell and $R$ is the internal resistance of the cell. 
Therefore, power fade is given by 
\begin{align}
Power Fade &= 1 - \frac{Power(k)}{Power(0)}\\
&= 1 - \frac{R(0)}{R(k)}
\end{align}
where $Power(0)$, $R(0)$ represents the power and internal resistance of the cell at the beginning and $Power(k)$, $R(k)$ represents after a certain time period $k$, respectively. \\

\noindent\textbf{Capacity fade}\\
Loss of cell capacity during charging and discharging is called $Capacity fade$ \cite{linden2001handbook}. It is given by 
\begin{align}
Capacity Fade(\%) &= \left(1 - \frac{Capacity(k)}{Capacity(0)}\right) \cdot 100
\end{align}
where $Capacity(0)$ and $Capacity(k)$ represents the capacities of the cell at the beginning and after a time interval of $k$ respectively. 

The RUL of the cell is calculated by using support vector machines for different input values of $Capacity fade$ and $Power fade$ as shown in Figure \ref{RULestimation}.
\begin{figure}[h!]
\centering
\includegraphics[scale = 0.6]{./Images/RUL.pdf}
\caption[Block diagram for predicting RUL of the cell]{Block diagram for predicting RUL of the cell \protect\cite{Patti}.}
\label{RULestimation}
\end{figure} 

\subsection{Controlling Module}
\label{controllingmodule}
The controlling module protects the cell from being damaged by preventing the cell to cross the threshold limits. 
It also maintains the cell within its \ac{SOA} and increases the efficiency of the cell. 
The basic functions performed by the controlling module are as follows\\

\noindent\textbf{{\acf{POST}}}\\
Each ESIC is capable of isolating the cell from the battery pack by means of the isolation switches provided in the architecture. 
When the battery pack is powered off, i.e. when the vehicle is parked, the ESIC associated with the cell turns off (opens) the series switch and the cell is isolated from the smart battery pack. 
When the vehicle is turned on, all the \acp{ESIC} perform a \acf{POST} and the cells that are healthy are connected to the battery pack by turning on the series switch.\\
 
\noindent\textbf{Over Voltage Protection}\\
Charging a \ac{Li-ion} cell beyond its maximum operating voltage of 4.2$V$ could lead to explosion of the cell.
To prevent this, the ESIC associated with the cell will isolate it from the series pack when it is fully charged, thereby routing the charging current across the cell. 
The isolation is done by means of the power MOSFET switches provided with the hardware architecture. 
As soon as a cell is taken off from the pack, the DBMS system will communicate this information to the charging station in order to reduce the charging voltage accordingly for charging the remaining cells safely. \\

\noindent\textbf{Under Voltage Protection}\\
\ac{Li-ion} cells collapse and lose their life when discharged below the minimum operating voltage of 2.7$V$. 
The ESIC will remove the cell from the pack and communicate to the spare smart cell to quickly connect to the battery pack.
A bump-less connection of the spare smart cell to the smart battery pack is made such that it does not interfere in the normal operation of the battery. 
This information is also communicated to the \ac{VMS} to notify the user regarding the failure of a single cell in the battery pack.\\ 

\noindent\textbf{Temperature Control}\\
The performance and safety of a \ac{Li-ion} cell is dependent upon the temperature. 
The ESIC increases the efficiency of the cell by heating it when the temperature is too low for the internal electrochemical reactions. 
Similarly, it also prevents the cell from thermal runaway by allowing it to cool, when the temperature increases beyond a certain maximum operating limit. 
The heating and cooling is done with help of the \ac{HVAC} which controls the flow of the cooling air into the battery pack.\\

\noindent\textbf{Spare Cell Rotation}\\
Over time and usage the cell losses its energy storage capacity and becomes weak. 
Therefore, the 100\% SOC value of an used cell is less than the 100\% SOC value of a new one.
In contrast, the spare cells in the battery, which are only used during an emergency situation, are new and their 100\% SOC value is different compared to that of the normal cells. 
Furthermore, the \ac{SOH} value of the spare cell will also be different than the rest of the cells in the pack.
When these cells are connected in series with the used cells in the pack, the variations in SOC and SOH between them will result in a reduced efficiency of the battery pack as discussed in section \ref{batterycomplexity}. 
To overcome this effect, in the DBMS architecture, the spare cells are not fixed only for emergency purpose.
Instead, the spare cells are continuously connected and disconnected from the battery pack to maintain an uniform ageing pattern for the cells.  
This is called rotation of the spare cell and this increases the efficiency and life of the battery pack. 
