


\subsection{System level model}
\label{sec:odesolving}

\noindent\textbf{Circuit Behaviour during $\mathbf \Ton$}\\

This section presented a solution approach for the \ac{ODE} in Eq.~\eqref{eq:kvl_Ton_ode} with initial conditions in Eq.~\eqref{eq:kvl_Ton_initcond} and Eq.~\eqref{eq:kvl_Toff_initcond} for $\Ton$ and $\Toff$, respectively. Combined, they form the following system:
\begin{align}
L\cdot \frac{\diff^2}{\diff t^2} i+ R \cdot \frac{\diff}{\diff t}i + \frac{1}{C} i =  0 \notag \\
i(0) = i_0 \hspace{1cm} \frac{\diff}{\diff t}i(0) = \diff i_0
\label{eq:appODE}
\end{align}
The solution process of such a second-order system is detailed in for instance control engineering literature such as \cite{ogata_moderncontrol}.
Following these solution processes, we rewrite Eq.~\eqref{eq:appODE} to
\begin{equation}\label{eq:appODE:step2}
0=\frac{\diff^2}{\diff t^2} i+ \frac{R}{L} \cdot \frac{\diff}{\diff t}i + \frac{1}{LC} i =: \frac{\diff^2}{\diff t^2} i+ 2\xi\omega_n \cdot \frac{\diff}{\diff t}i + \omega_n^2 i\mbox{.}
\end{equation}
The behavior of the system largely depends upon its natural frequency $\omega_n$ and its damping ratio $\xi$. 
In case of the proposed circuit, $\omega_n$ and $\xi$ are given by the following equations:
\begin{align}
\omega_n = & \frac{1}{\sqrt{LC}} & \xi = & \frac{1}{2} \frac{R}{L} \sqrt{LC}
\end{align}
The characteristic equation of the \ac{ODE} whose roots lead to the general solution of the system can be obtained by introducing variable $s$ into Eq.~\eqref{eq:appODE:step2}:
\begin{equation}\label{eq:appODE:char}
s^2+ 2\xi\omega_n \cdot s + \omega_n^2
\end{equation}
The roots of the characteristic Eq.~\eqref{eq:appODE:char} of the \ac{ODE} results in
\begin{equation}\label{eq:appODE:charroot}
s_{1/2} = - \xi \omega_n \pm \omega_n \sqrt{\xi^2 - 1}
\end{equation}
and the general time domain solution of \ac{ODE} system in Eq.~\eqref{eq:appODE} is therefore given by:
\begin{equation}
i(t) = \gamma_1 e^{s_1 t} + \gamma_2 e^{s_2 t} 
\end{equation}
Solving for the constants $\gamma_1$ and $\gamma_2$, using the initial values from Eq.~\eqref{eq:appODE} yields the following relation:
\begin{align} 
i(t) = & \frac{\diff i_0 - i_0 s_2}{s_1 - s_2} e^{s_1 t} - \frac{\diff i_0 - i_0 s_1}{s_1 - s_2} e^{s_2 t} 
\end{align}
Using Eq.~\eqref{eq:appODE:charroot}, this can be further reformulated to:
\begin{align}
i(t) = & e^{-\xi \omega_n t} \cdot \bigg\{ \notag \\
& \frac{\diff i_0 - i_0 (-\xi \omega_n - \omega_n \sqrt{\xi^2-1})} {2\omega_n \sqrt{\xi^2-1}} e^{(\omega_n \sqrt{xi^2-1})t} \notag \\
 - & \frac{\diff i_0 - i_0 (-\xi \omega_n + \omega_n \sqrt{\xi^2-1})} {2\omega_n \sqrt{\xi^2-1}} e^{-(\omega_n \sqrt{xi^2-1})t} \bigg\} 
 \label{eq:appODE:sol}
\end{align}

From this point, we need to differentiate between three cases, $\xi<1$ (\emph{under-damping}), $\xi=1$ (\emph{critical damping}) and $\xi>1$ (\emph{over-damping}) because they represent entirely different system behaviors.
Under-damping leads to a resonating signal that is slowly damped away.
Over-damping on the other hand is not resonating, but creeps very slowly to its equilibrium.
Critical damping is an interim situation where the system signal resonates exactly once and is then damped away.
This is the fastest way to reach the equilibrium and critical damping is therefore used as a design methodology in certain situations.
Since our approach ends the system signals prematurely, all three cases can be handled and do in fact barely differ on the relevant time scale as we will see in the following sections.
\subsection{Under-Damping($\xi<1$)}
\label{sec:model:underdamping}
\begin{figure}[t]
\currentplot{tikz/plotdata/underdampedtxlong.txt}{tikz/plotdata/underdampedtxshort.txt}{tikz/plotdata/underdampedrxlong.txt}{tikz/plotdata/underdampedrxshort.txt}
%\includestandalone{tikz/current_chart_underdamped}
\caption{Current plots for the under-damped case; upper row: charging current $\bm i_{\protect\xv{sendf}}$; lower row: discharging current $\bm i_{\protect\xv{recvf}}$.}
%\todomartin{Das va Skript funktioniert in dieser Caption irgendwie nicht, ok mit protect gehts, kennst du noch was besseres?}
\label{fig:current:underdamped}
\end{figure}
If $\xi<1$, the roots of the characteristic are not real, but complex:
\begin{equation}
s_{1/2} = - \xi \omega_n \pm j\omega_n \sqrt{ 1 - \xi^2}
\end{equation}
Using this, we can transform Eq.~\eqref{eq:appODE:sol}:
\begin{align}
i(t) = & e^{-\xi \omega_n t} \cdot \bigg\{ \frac{\diff i_0 - i_0 (-\xi \omega_n - j\omega_n \sqrt{1-\xi^2})} {2 j\omega_n \sqrt{1-\xi^2}} \notag \\
& \cdot(\cos(\omega_n \sqrt{xi^2-1})t) + j \sin(\omega_n \sqrt{xi^2-1}t) \notag \\
 - & \frac{\diff i_0 - i_0 (-\xi \omega_n + j\omega_n \sqrt{1-\xi^2})} {2j\omega_n \sqrt{1-\xi^2}} \notag \\
& \cdot(\cos(\omega_n \sqrt{xi^2-1})t) - j \sin(\omega_n \sqrt{xi^2-1}t) \bigg\} \notag 
\end{align}
\begin{align}
i(t) = &  e^{-\xi \omega_n t} \cdot \big\{ i_0 \cos(\omega_n \sqrt{1-\xi^2} t) \notag \\ 
& + \frac{\diff i_0 + i_0 \xi \omega_n}{\omega_n \sqrt{1-\xi^2}} \sin(\omega_n \sqrt{1-\xi^2} t) \big\} \label{eq:kvl_Toff_specsol}
\end{align}
Abstracting $i(t) = e^{-ct} \big( A\cos(at) + B\sin(at) \big)$, we can integrate Eq.~\eqref{eq:kvl_Toff_specsol} to obtain the transferred charge $q$.
\begin{align}
q(T) = & \frac{-A}{a^2 + c^2} \Big[c(e^{-Tc}\cos(Ta) -1) - ae^{-Tc} \sin(Ta) \Big] \notag \\
- & \frac{B}{a^2 +c^2} \Big[a(e^{-Tc} \cos(Ta) -1 ) + ce^{-Tc} \sin(Ta) \Big]
\end{align}
The remaining task is to calculate $T$.
For the charging phase, we can use $T=\Ton$, but for $T=\Toff$, we need to calculate when the inductor is actually empty, i.e., we solve the following:
\begin{align}
 & 0 =  A \cos(at) + B \sin(at) \notag \\
\Leftrightarrow & -\frac{A}{B} = \frac{\sin(at)}{\cos(at)} \notag \\
\Leftrightarrow & at = \arctan (-\frac{A}{B})
\label{eq:TonToff:underdamped}
\end{align}
Since there is no closed-form for $\Ton$ in the nonlinear model, a short binary search using the nonlinear model can be used to improve the accuracy of the linear estimation.
Fig.~\ref{fig:current:underdamped} gives an impression on how $i_{\xv{send}}$ and $i_{\xv{recv}}$ behave and how small $\Ton$, $\Toff$ are relatively to the time constants of the sine waves.


%\begin{figure}[ht]
%\begin{minipage}[b]{0.45\linewidth}
%\centering
%\begin{tikzpicture}
%\currentplot{tikz/plotdata/underdampedtxlong.txt}{0}{$\Toff$}{$\ipeak$}
%\end{tikzpicture}
%\caption{$i_{\xv{send}}$, under-damped, large-scale}
%\label{fig:underdampedtxlong}
%\end{minipage}
%\hspace{0.5cm}
%\begin{minipage}[b]{0.45\linewidth}
%\begin{tikzpicture}
%\currentplot{tikz/plotdata/underdampedtxshort.txt}{2}{$\Ton$}{$\ipeak$}
%\end{tikzpicture}
%\caption{$i_{\xv{send}}$, under-damped, small-scale}
%\label{fig:underdampedtxshort}
%\end{minipage}
%\end{figure}
%\todoall{Do we want more of these plots or are they actually bad for us?}
%
%\begin{figure}[ht]
%\begin{minipage}[b]{0.45\linewidth}
%\centering
%\begin{tikzpicture}
%\currentplot{tikz/plotdata/underdampedrxlong.txt}{0}{$\Toff$}{$\ipeak$}
%\end{tikzpicture}
%\caption{$i_{\xv{recv}}$, under-damped, large-scale}
%\label{fig:underdampedrxlong}
%\end{minipage}
%\hspace{0.5cm}
%\begin{minipage}[b]{0.45\linewidth}
%\begin{tikzpicture}
%\currentplot{tikz/plotdata/underdampedrxshort.txt}{2}{$\Toff$}{}
%\end{tikzpicture}
%\caption{$i_{\xv{recv}}$, under-damped, small-scale}
%\label{fig:underdampedrxshort}
%\end{minipage}
%\end{figure}

\begin{figure}[t]
\currentplot{tikz/plotdata/overdampedtxlong.txt}{tikz/plotdata/overdampedtxshort.txt}{tikz/plotdata/overdampedrxlong.txt}{tikz/plotdata/overdampedrxshort.txt}
\caption{Current plots for the over-damped case; upper row: charging current $\bm i_{\protect\xv{sendf}}$; lower row: discharging current $\bm i_{\protect\xv{recvf}}$.}
\label{fig:current:overdamped}
\end{figure}

\subsection{Over-Damping ($\xi>1$)}
If $s_{1/2} \in \mathbb{R}$, we can directly abstract $i(t)=e^{-ct} \big[ A e^{at} - Be^{-at}]$ from Eq.~\eqref{eq:appODE:sol} and integrate it to obtain:
\begin{align}
q(T) = &\frac{B}{a+c} \Big[e^{- Ta - Tc}- 1\Big]+ \frac{A}{a-c} \Big[e^{Ta - Tc}- 1\Big]
\end{align}
To calculate $\Toff$, we need to solve the following:
\begin{align}
& 0 =  A e^{at} - B e^{-at} \notag \\
\Leftrightarrow & \log(e^{at}) =  \log(\frac{B}{A} e^{-at}) = \log( \frac{B}{A}) + -at \notag \\
\Leftrightarrow & 2at = \log(\frac{B}{A})
\label{eq:TonToff:overdamped}
\end{align}
As illustrated in Fig.~\ref{fig:current:overdamped}, it becomes obvious that the damping actually has only little influence during the time frame we are interested in.



%
%\begin{figure}[ht]
%\begin{minipage}[b]{0.45\linewidth}
%\centering
%\begin{tikzpicture}
%\currentplot{tikz/plotdata/overdampedtxlong.txt}{0}{$\Toff$}{$\ipeak$}
%\end{tikzpicture}
%\end{minipage}
%\hspace{0.5cm}
%\begin{minipage}[b]{0.45\linewidth}
%\begin{tikzpicture}
%\currentplot{tikz/plotdata/overdampedtxshort.txt}{2}{$\Ton$}{$\ipeak$}
%\end{tikzpicture}
%\end{minipage}
%\end{figure}
%\todoall{Do we want more of these plots or are they actually bad for us?}
%
%\begin{figure}[ht]
%\begin{minipage}[b]{0.45\linewidth}
%\centering
%\begin{tikzpicture}
%\currentplot{tikz/plotdata/overdampedrxlong.txt}{0}{$\Toff$}{$\ipeak$}
%\end{tikzpicture}
%\caption{$i_{\protect\xv{recv}}$, over-damped, large-scale}
%\label{fig:current:overdamped}
%\end{minipage}
%\hspace{0.5cm}
%\begin{minipage}[b]{0.45\linewidth}
%\begin{tikzpicture}
%\currentplot{tikz/plotdata/overdampedrxshort.txt}{2}{$\Toff$}{}
%\end{tikzpicture}
%\end{minipage}
%\end{figure}


\subsection{Critical Damping ($\xi=1$)}
If $\xi=1$, the characteristic equation has co-located roots and the solution therefore becomes
\begin{align}\label{eq:current:critdamped}
i(t) = (A+ Bt) e^{-ct}
\end{align}
with $A = i_0$, $B=\diff i_0 + \omega_n i_0$, $c=\omega_n$.
Fig.~\ref{fig:current:critdamped} shows the corresponding plots.
Again, the behavior does not differ much from the other cases as far as very short intervals are concerned.
Integrating $i(t)$ yields the transferred charge as in the other cases:
\begin{equation}
%q(T) = B*(1/(c**2) - (s.exp(-T*c)*(T*c + 1))/(c**2)) - (A*(s.exp(-T*c) - 1))/c
q(T) = \frac{B}{c^2} \big( 1- (Tc + 1)e^{-Tc} \big) - \frac{A}{c} \big( e^{-Tc} -1 \big)
\end{equation}
The calculation of $\Toff$ is done with Eq.~\eqref{eq:current:critdamped}. 
We have to solve the following:
\begin{align}\label{eq:TonToff:critdamped}
0  = & A+Bt & \Rightarrow t =& \frac{-A}{B}
\end{align}

%
\begin{figure}[t]
\currentplot{tikz/plotdata/critdampedtxlong.txt}{tikz/plotdata/critdampedtxshort.txt}{tikz/plotdata/critdampedrxlong.txt}{tikz/plotdata/critdampedrxshort.txt}
\caption{Current plots for the critically damped case; upper row: charging current $\bm i_{\protect\xv{sendf}}$; lower row: discharging current $\bm i_{\protect\xv{recvf}}$.}
\label{fig:current:critdamped}
\end{figure}
%
%\begin{figure}[ht]
%\begin{minipage}[b]{0.45\linewidth}
%\centering
%\begin{tikzpicture}
%\currentplot{tikz/plotdata/critdampedtxlong.txt}{0}{$\Toff$}{$\ipeak$}
%\end{tikzpicture}
%\caption{$i_{\protect\xv{send}}$, crit-damped, large-scale}
%\label{fig:current:critdamped}
%\end{minipage}
%\hspace{0.5cm}
%\begin{minipage}[b]{0.45\linewidth}
%\begin{tikzpicture}
%\currentplot{tikz/plotdata/critdampedtxshort.txt}{2}{$\Ton$}{$\ipeak$}
%\end{tikzpicture}
%\end{minipage}
%\end{figure}
%\todoall{Do we want more of these plots or are they actually bad for us?}
%
%\begin{figure}[ht]
%\begin{minipage}[b]{0.45\linewidth}
%\centering
%\begin{tikzpicture}
%\currentplot{tikz/plotdata/critdampedrxlong.txt}{0}{$\Toff$}{$\ipeak$}
%\end{tikzpicture}
%\end{minipage}
%\hspace{0.5cm}
%\begin{minipage}[b]{0.45\linewidth}
%\begin{tikzpicture}
%\currentplot{tikz/plotdata/critdampedrxshort.txt}{2}{$\Toff$}{}
%\end{tikzpicture}
%\end{minipage}
%\end{figure}