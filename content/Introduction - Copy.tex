
\chapter{Introduction}
\label{sec:Introduction}
\indent
%\doublespacing



Batteries form the functional and important component of an electric vehicle. Recent researches in various battery technologies, prove that the Li-ion chemistry is more efficient than other types of cell chemistry , because of their high energy density and  high charge/discharge cycles. The operating voltage of an electric vehicle is around 400 V. Each Li-ion cell has an operating cell voltage of around 4 V. Therefore, the batteries in the electric vehicles are formed by connecting hundreds of Li-ion cells in series to achieve the required operating voltage as shown in figure 1.\\

The lower and upper safe operating limits of a single Li-ion cell are 3.2 V and 4.2 V respectively. The nominal operating voltage is 4 V. Li-ion cell has a threat of explosion when charged to a voltage higher than the upper safe operating voltage. Moreover, the Li-ion cell collapses when it is discharged below the lower safe operating voltage. Therefore, an accurate control system is required to maintain the operating voltages of each individual Li-ion cell under its safe operating limit. Battery Management System (BMS) is a system which helps to operate the battery within its safe operating regions and also controls the batteries for efficient operation of the electric vehicle.\\ 

\noindent The important tasks of a Battery Management System are \
\begin{itemize}
		\item  Monitor the parameters like voltage, current, temperature of each individual Li-ion cell.
		\item Calculate the important parameters of the battery like State of Charge (SOC), State of Health (SOH), Remaining Useful life (RUL).
		\item Maintain the operation of the battery within its safe operating voltage limits. 
		\item Implement safe shutdown of the battery in case of emergency situations. 
		\item Communicate the status of the battery to the vehicle overall management system. 
		\item Control the charging and discharging process of the battery.
		\item Increase the run time of the battery by performing cell balancing, to equalise the individual voltages of the cells in the battery.
		\item Range prediction of the vehicle with the remaining battery capacity. 
	\end{itemize}


The BMS system can be classified into three types based upon their architecture. They are
\begin{itemize}
		\item Centralised Battery Management System
		\item Modular Battery Management System
		\item Distributed Battery Management System(DBMS)
\end{itemize}



The amount of software in cars has been rising from about 50.000 lines of code in 1981 to 100 million lines of code in a current premium car. 
Those numbers are expected to rise to 200 or 300 million lines of code in the near future.
This software is spread over multiple \glspl{ECU}. 
A premium car currently contains about 70 to 100 \glspl{ECU}, in low-end cars this number equals about 30 to 50 \glspl{ECU}
\cite{Charette2009}.
As these ECUs are usually not working on separated tasks, but on control processes which are spread over multiple \glspl{ECU}, these \glspl{ECU} need to be interconnected.
This is done via buses.
The major upcoming communication bus for automotive applications is FlexRay. \\

This thesis describes the analysis and development of policy-based scheduling for messages in the FlexRay static segment.
In existing communication systems a choice between time-triggered or event-triggered architectures has to be made.
This is a choice between high temporal predictability and low bandwidth utilization or low predictability and high utilization.
Sometimes both approaches are not suitable in their extremes.
This thesis specifies a system combining the two approaches to a system with higher temporal predictability than an event-triggered system and higher bandwidth utilization than a time-triggered system.
The presented system ensures that all deadlines are kept in any case, while keeping the bandwidth utilization as high as possible.

\section{Motivation}
\label{sec:motivation}
When working within the domain of embedded communications, a design choice has to be made, whenever a communication system is introduced.
The communication system can either be time-triggered or event-triggered.
This choice does not always come easy.
For high-reliability systems, the common choice is time-triggered, as this allows a predictable behaviour.
Time-triggered systems are deterministic and the worst-case delay of messages is constant.
However, time-triggered systems come with the disadvantage that the reservation of bandwidth for a message or \gls{ECU} is often higher than the actually needed bandwidth.
This is to ensure the correct transfer of messages.
In a time-triggered system always the maximum amount of bandwidth necessary needs to be reserved, even though this amount might not be needed constantly.
An event-triggered system does not have this disadvantage, as there is no predefined schedule.
Participants on the bus can use the required bandwidth at any time.
But in contrast to time-triggered systems, the delays are not constant, as messages can be delayed by higher priority messages.
Often the choice between time-triggered and event-triggered boils down to a choice between excessive bandwidth requirements and timing guarantees or bandwidth efficiency without guarantees.
Higher bandwidth requirements always comes with higher costs.
To reduce the costs, while keeping guarantees, research has been concerned with combining these two approaches to allow guarantees while lowering the bandwidth requirements of a system.
In this thesis, the implementation of such a combined system shall be evaluated for the use within the FlexRay static segment. \\

In the current FlexRay specification, the static segment needs to be assigned a fixed schedule at the time of implementation.
This schedule needs to include all messages in the system at all times including their sender and receivers.
This brings all the advantages of a \gls{TDMA} system, such as defined worst-case response times and the general determinism of the communication.
But this system also has disadvantages. \\

Firstly, this schedule is fixed and needs to be completely recalculated when a change in the system occurs.
This is especially critical for prototyping.
It is not always possible to reuse the schedule when one or more messages are added.
A new schedule has to be created.
This problem extends to another area of application.
A car typically experiences many different scenarios in which different messages are of higher importance than others.
A parking situation is for example very different from a ride on the highway.
To efficiently adjust to these multi-rate applications it is necessary to adjust the schedule.
As a runtime adjustment is not defined in the FlexRay specification, the final schedule has to be a compromise between all possible scenarios.
A more flexible scheduling would allow better utilization of the bus.
Furthermore, oversampling may occur, if a message does not fit the exact timing of the schedule.
This occurs if the message period is not an integer multiple of the cycle duration.
For these messages a slot has to be reserved at the rounded lower integer multiple of the cycle duration, such that it can be send every time it is required by the \gls{ECU}.
The induced rounding error will lead to oversampling of the message and a degradation of the data transfer capacity.\\

The FlexRay dynamic segment is event-triggered and as such offers some of the needed flexibility, but can at the same time not guarantee schedulability.
Messages might be lost, as slots might be used by higher priority \glspl{ECU}.
Another compromise would be needed to accommodate all the above requirements.\\

To solve these issues, a system shall be designed and implemented, which assigns slots to ECUs instead of messages.
To keep this compatible with the current FlexRay standard, a new layer is introduced, mapping event-triggered messages to more general messages schedulable in time-triggered slots.
These general messages are called wrapper-PDUs (Protocol Data Units) as they wrap around the application layer messages.
The application layer messages can then be flexibly placed into these wrapper-PDUs.

\section{Outline}
\label{sec:outline}
In chapter \ref{sec:embeddedCommunications} a general overview over embedded communications is given.
A short overview of buses available in the automotive domain is given in chapter \ref{sec:Automotive Buses} and the FlexRay bus is described in detail in chapter \ref{sec:FlexRay}.
Chapter \ref{sec:RelatedWork} shows an outline of all research available in the domain.
In chapter \ref{sec:Analysis} an analysis of existing scheduling concepts and their applicability to FlexRay is presented.
Chapter \ref{sec:Implementation} describes the implementation of the scheduling algorithms, a simulator to test the results of the scheduling, as well as multiple tools developed throughout this thesis.
Following that, chapter \ref{sec:Applications} gives an overview of where a policy-based scheduling system can be beneficially used.
Finally chapter \ref{sec:Conclusion} draws a conclusion of the work presented and presents an outlook towards further work in this area.

\newpage