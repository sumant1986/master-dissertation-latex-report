\chapter{Sensor Module Design of Embedded System IC}
\label{sec:ESICdesign}

The heart of the Distributed Battery Management System architecture is the Embedded System IC(ESIC). The ESIC contains various blocks as shown in section \ref{ESICblocks}. In this chapter the sensor module design of the ESIC architecture is presented in detail. The sensor module is made up of three submodules 

\begin{itemize}
\item Voltage measurement module is explained in section \ref{voltagemeasurementmodule}, 
\item Current measurement module is explained in section \ref{currentmeasurement} and 
\item Temperature measurement module is explained in section \ref{temperaturemeasurementmodule}. 
\end{itemize}

\section{Voltage Measurement Module}
\label{voltagemeasurementmodule}

The cell voltage is an important parameter to be measured in a Battery Management System. 
This measurement is used to determine the SOC of the cell and also to detect the overvoltage and undervoltage limits during charging and discharging of the cell. 
The voltage measurement module measures the voltage of the individual cells in a series connected battery pack. Individual Li-ion cells have an operating cell voltage of 4 V. When 100’s of such cells are connected in series, then the top cell would have a voltage of around 400V at the positive terminal and 396V at the negative terminal with respect to the negative terminal of the battery respectively as shown in figure \ref{voltagemeasurement}. Therefore, the important task of the voltage measurement module is to accurately measure the voltage across each individual cell in a series connected pack.

\begin{figure}[h!]
\centering
\includegraphics[width=14cm]{./Images/voltagemeasurementintro.pdf}
\caption{Battery pack of an electric vehicle containing Li-ion cells connected in series to get high voltages.}
\label{voltagemeasurement}
\end{figure}

\subsection{Individual A/D Cell Voltage Measurement}

The various voltage measurement techniques as explained in section \ref{RelatedWork} provides an output value in analogue form. 
This analogue data has to be converted into a digital value in order to be handled by other higher level blocks in the ESIC architecture(communicaiton and computation), as they process only digital data. 
Therefore this method is advantageous than any other techniques for voltage measurement as this would effectively reduce one sensing level required by other techniques.
Moreover the accuracy of the measurement is increased compared to other methods because of the reduction in the number of levels of measurement.   
Each ESIC is provided with an Analogue to Digital Conversion sensor as shown in figure \ref{ADconversion}.

\begin{figure}[h!]
\centering
\includegraphics[width=8.5cm]{./Images/ADCvoltagemeasurement.pdf}
\caption{Individual cell voltage measurement using Analogue to Digital converter within the ESIC.}
\label{ADconversion}
\end{figure}

\noindent\textbf{Design}\\
Let the maximum and minimum analogue cell voltage be V\textsuperscript{+} and V\textsuperscript{-} respectively. Then the converted digital word is given by equation \ref{ADC}. 

\begin{equation}
\label{ADC}
D_{out} = \frac{(V^{+} - V^{-})}{(V_{r+} - V_{r-})} * (2^{N} - 1)
\end{equation}

\noindent where $V_{r+}$ and $V_{r-}$ represents the maximum and minimum reference voltages provided to the ADC respectively and N represents the number of bits in the digital Value. \\

\noindent\textbf{Range}\\
The range of an A/D converter is the analogue voltage range that could be effectively measured. The output transfer characteristic of a 3 bit A/D converter is shown in figure \ref{ADCchar}.\\

\begin{figure}[h!]
\centering
\includegraphics[width=12cm]{./Images/adctransfer.pdf}
\caption{Transfer characteristic of a 3 bit A/D converter.}
\label{ADCchar}
\end{figure}

The output value $D_{out} $ saturates to the maximum value when $V_{in}$ is greater than $V_{r+}$ and to the minimum value when $V_{in}$ is less than $V_{r-}$. 
Therefore the value of $V_{r+}$ and $V_{r-}$ should be selected such that the input analogue voltage is within this range. 
The range of A/D can be increased by increasing the value of $V_{r+}$. 
The maximum and minimum cell voltage are 4.2V and 2.7V respectively. 
Therefore the value of $V_{r+}$ should be greater than 4.2V and the value of $V_{r+}$ should be less than 2.7V to accurately measure the cell voltage. 
The value $V_{ref}$ = $V_{r+}$ - $V_{r+}$ is called as the reference voltage of ADC and is generated by a bandgap voltage generator \cite{Malo2001}. \\



\noindent\textbf{Resolution}\\
The resolution of a A/D converter is defined by the number of bits N of the A/D converter. 
Increasing the number of bits N will increase the resolution of A/D converter. 
According to \cite{andrea2010} the resolution of measurement and accuracy requirements for a reference voltage of 5V are provided in table \ref{ADC}.


\begin{table}[ht]
\begin{center}
\begin{tabularx}{\linewidth}{ X  c  c }
\toprule

Application & Accuracy(mV)&  No of bits(N)\\
\midrule
\midrule


For Over voltage and Under voltage detection & 100 & 6\\
For performing cell Balancing & 50 & 8\\
To estimate SOC at the either end of OCV Vs SOC curve & 10 & 10\\
To estimate SOC at flat portion of OCV Vs SOC curve  & 1 & 12\\ 

\bottomrule

\end{tabularx}
\end{center}
\caption{Resolution and Accuracy requirements of ADC.}
\label{Davidandrea}
\end{table}

Increasing the value of N will improve the resolution of the measurement of the analogue value.
The value $\frac{(V_{r+} - V_{r-})}{2^{N}}$ is called as $V_{LSB}$. 
The digital value is incremented by one bit if the corresponding analogue value increases by one $V_{LSB}$.
For example an ADC with N = 10 bits and $(V_{r+} - V_{r-})$ is 5 V then the digital value is incremented by one bit only after a change in the analogue voltage of the value 4.88mV as per equation \ref{VLSB}.

\begin{equation}
\label{VLSB}
V_{LSB} = \frac{(V_{r+} - V_{r-})}{2^{N}}
= \frac{5}{2^{10} - 1}
= 4.88mV\\
\end{equation}

The ESIC requires a high resolution in voltage measurement in order to accurately calculate the SOC of the cell. Therefore the number of bits required for the A/D converter is even more than that specified in table \ref{Davidandrea}.\\

\noindent\textbf{Rate}\\
The rate of measurement is another important factor in determining the A/D specification. This parameter determines the speed of the conversion and thereby the type of A/D converter. The cell voltage of the battery is a slowly changing variable and therefore samples per ms second range is sufficient. This implies A/D converters operating in the range of kilohertz frequencies are suitable for cell voltage measurement. Moreover increasing the speed of A/D converters will also increase the power consumption of the device and the area required by them on the die. \\

\noindent\textbf{Types of A/D Converter}\\
The A/D conversion technique is selected based upon the requirement of speed, accuracy, size and power consumption. The ADC's can be classified based on their speed and accuracy as shown in table \ref{ADclassi} \cite{johns1997analog}.\\
The Successive approximation or Oversampling type converters are best suitable for cell voltage measurement application because of their high accuracy in conversion and moderate speed required for ESIC. 

\newpage

\begin{table}[ht]
\begin{center}
\begin{tabularx}{\linewidth}{ X  X  X }
\toprule

Low - to - Medium Speed, & Medium speed, &  High Speed,\\
High Accuracy & Medium Accuracy & Low - to - Medium Accuracy\\
\midrule
\midrule


Integrating & Successive approximation & Flash\\
Oversampling & Algorithmic & Two-step\\
 &  & Interpolating \\
 &  & Floding\\ 
 &  & Pipelined\\
 &  & Time Interleaved\\
\bottomrule

\end{tabularx}
\end{center}
\caption{Comparision of ADC architectures based on speed and accuracy.}
\label{ADclassi}
\end{table}


\section{Current Measurement Module}
\label{currentmeasurement}

Measurement of Battery current is essential and allows the ESIC to perform the following functions \ref{Davidandrea},

\begin{itemize}
\item To prevent the cells from being operated outside their Safe Operating Area (SOA).
\item To calculate the Depth of Discharge (DOD).
\item To calculate internal DC resistance of the cell and perform IR compensation. 
\end{itemize}

Batteries in electric vehicle are formed by connecting several cells in series as shown in figure \ref{Batterypack}. As a result the battery pack current is same in all the cells.Therefore the measurement of current is not required in each individual cells, instead a single current sensor could be used to measure the battery pack current. 
This sensor could be connected to any of the ESIC and then that ESIC could communicate the current value to all other ESIC's in the battery pack.
To increase the accuracy and to improve the reliability, the current measurement could be performed in more than one point in the series string and the average value of the readings could be used as battery pack current. 

There are three ways to measure the battery pack current in electric vehicle applications. They are
\begin{itemize}
\item Resistive shunt
\item Current transformer
\item Hall effect
\end{itemize} 
Out of these the current transformer method can only measure AC currents \cite{CurrentTx}. 
Therefore this thesis proposes resistive shunt and Hall Effect method for DC current measurement in Batteries and they are explained in detail in the following sections.



\subsection{Resistive Shunt Current Measurement}
\label{Resistive}

This method measures the DC current by passing it through a very small value resistor called $R_{shunt}$ and measuring the differential voltage$V_{sense}$ generated as shown in figure \ref{ResCM}.\\

\begin{figure}[h!]
\centering
\includegraphics[width=9.5cm]{./Images/Rshunt.pdf}
\caption{Shunt Resistor Current Measurement.}
\label{ResCM}
\end{figure}

If the shunt resistor is placed at the top of the battery pack then it is called as high side current sensing. 
Alternatively if the shunt resistor is placed at the bottom of the battery pack then it is called as low side current sensing. 
Advantages and Dis-Advantages of both these methods is described in table \ref{Currentsensing}.


\begin{table}[h!]
\begin{center}
\begin{tabularx}{\linewidth}{ X  X  X }
\toprule

Application & Advantages  &  Dis-Advantages \\
\midrule
\midrule

High side current sensing & Battery pack ground is not altered. & High common mode input voltage for the amplifier.\\
Low side current sensing & Low common mode input as one end is referred to ground & Battery pack ground is increased to value of $V_{sense}$.\\

\bottomrule

\end{tabularx}
\end{center}
\caption{Comparision of high side and low side current sensing.}
\label{Currentsensing}
\end{table}



\noindent\textbf{Design}\\
The shunt is a very high accurate, low value resistor selected based upon the DC current value that is measured. 
The value of $R_{shunt}$ is very small for higher measuring currents, in order to minimize the heat generated across it. 
The voltage $V_{sense}$ produced across the shunt resistor is of very low value and therefore it has to be amplified by a highly accurate Current shunt monitor(CSM) amplifier before providing it to the ESIC. 
In addition the CSM should be capable of measuring bi-directional current both charging as well as discharging. \\
For example to measure a current of 200 A with a power loss of less than 1 W and with an accuracy of 1\%, then the value of $R_{shunt}$ is calculated as below
\begin{align}
P&=I^2 * R \label{first}\\
R&=\frac{P}{I^2}\\
R&=\frac{1}{200^2}\\
R&=25\mu\Omega \label{powerR}
\end{align}

The voltage resolution for the same current signal varying from 0 to 200A is calculated as below
\begin{align}
V_{sense} &= I * R \\
V_{sensemin} &= 0 * 25 \mu\Omega = 0 V\\
V_{sensemax} &= 200 * 25 \mu\Omega = 5 mV \\
V_{sense} for 1 A &= \frac{5}{200} = 25 \mu V \label{second}
\end{align}  

To get an accuracy of 1\% the offset voltage of the op-amp should be 1\% of the value $V_{sense}$. That is given by equation \ref{offset}
\begin{align}
V_{offset} = \frac{1 * 25\mu V}{100}\\
V_{offset} = 250 nV
\label{offset}
\end{align}

According to equation \ref{first}, a lower value of resistance R is required in order to reduce the power dissipated across it. 
However, equation \ref{second} shows that a lower value of R will reduce the voltage sensitivity and requires a highly accurate and very low offset amplifier to amplify the low voltage signal $V_{sense}$. 
Therefore the selection of $R_{shunt}$ is crucial in terms of accuracy, power dissipation and sensitivity. \\
Moreover the resistance value of the $R_{shunt}$ varies according to the equation \ref{Rtemp}.
\begin{align}
R_{shunt} = R_{shunt0} (1 + \alpha * T)
\label{Rtemp}
\end{align}
where $R_{shunt0}$ is the resistance at room temperature and $\alpha$ is the temperature coefficient of the material and T is the atmospheric temperature. 
Therefore temperature compensation has to be performed by ESIC based on a known value of resistance at a particular temperature and with the reading from the temperature sensor. 
 
 
\subsection{Hall Effect Current Measurement}
\label{Hall}
The Hall effect current sensor works on the principle of Hall effect. \\

\noindent\textbf{Hall effect}\\
When a current carrying conductor is placed in a direction perpendicular to a magnetic field, a small voltage is produced proportional to the value of the current and perpendicular to both the direction of current and the magnetic field \cite{CurrentTx}.\\

\noindent\textbf{Characteristics}
\begin{itemize}
\item Output voltage is amplified by an inbuilt amplifier within the sensor. Therefore no external amplification and signal conditioning circuitry is required as in the case of Resistive shunt measurement. 
\item Highly accurate and reliable in operation over a wide range of temperature. 
\item Variation of temperature doesn't have any effect on output voltage. 
\item Output voltage is isolated from the input high current side. Therefore no external isolation amplifier is required to protect the low voltage electronics from high voltage circuits. 
\item Measure bi-directional currents without any additional circuitry. 
\item Hall elements are characterized by offset error that could be eliminated by signal conditioning circuits. 
\end{itemize}

The block diagram of the hall sensor from Allegro Microsystem  is shown in figure \ref{Hallsensor}.

\begin{figure}[h!]
\centering
\includegraphics[width=11cm]{./Images/hall.pdf}
\caption{Hall Effect Current Measurement \cite{AllegroDS}.}
\label{Hallsensor}
\end{figure}

The battery pack current is passed through the input coil IP+ and IP-. The voltage generated due to hall effect on this coil is amplified and produced at the output terminal. The signal conditioning circuitry consists of a chopper stabilized amplifier to remove the offset error induced by the hall element and the associated amplifier. Moreover the output voltage is not sensitive to changes in temperature.  In addition, the sensor output is linearly proportional to the input current.\\
The sensor produces a positive output voltage for currents in one direction and negative output voltage for currents in the opposite direction as shown in figure \ref{Hallchar}. 

\begin{figure}[h!]
\centering
\includegraphics[width=13cm]{./Images/hallchar.pdf}
\caption{Transfer Characteristic of Hall Effect Current Measurement \cite{AllegroDS}.}
\label{Hallchar}
\end{figure}

The voltage sensitivity of the 200 A sensor as per datasheet \cite{AllegroDS} is 10 mV / A. This enables the ESIC to easily detect the changes in the input current.
The accuracy of the sensor is 1.2 \% of the full scale current applied.\\  
The internal resistance of the conductor is in the range of 100$\mu\Omega$ from which the power loss is calculated for a current of 200A as follows  

\begin{align}
P& = I^2 * R\\
P& = (200)^2 * 100 \mu\Omega\\
P& = 4 W \label{powerlossHall}
\end{align}

Moreover the sensor size is very small compared to that of the shunt resistor and could be easily integrated in a DBMS architecture. 
\newpage
A detail comparison between Resistive shunt and the Hall effect Current Measurement is provided in table \ref{compresandhall}.
\begin{table}[h!]
\begin{center}
\begin{tabularx}{\linewidth}{ X  X  X }
\toprule

Feature & Resistive Shunt & Hall Effect \\
\midrule
\midrule

Power loss & For measuring current of 200A with 25$\mu\Omega$ resistor the power loss is 1 W as per equation \ref{powerR}. The power loss could be scaled down by reducing R. & The resistance of the sensor is 100$\mu\Omega$ and the power loss is given by equation\ref{powerlossHall} is 4 W. The difference is not very high.\\
\midrule
Sensitivity & Very low as per equation \ref{second} the sensitivity is very low in the order of 25$\mu$V which requires a high sensitive amplification unit.  & Sensitivity is higher in the order of mV which relaxes the specification on the signal conditioning circuitry.\\
\midrule
Signal Conditioning & External signal conditioning circuitry is required to amplify the low output voltage produced by the R$_{shunt}$. & Signal conditioning circuitry is inbuilt within the sensor.\\
\midrule
Offset errors & Only offset error from the op-amp. & Offset error is from both the sensor and from the amplification unit. \\
\midrule
Measuring point difficulties & Depending upon the point of measurement the measurement is affected by either high common mode voltage for high side measurement or the battery ground is altered for low side current measurement. & Measurement is not affected by the location of the sensor. \\
\midrule
Temperature Sensitivity & Resistance changes with temperature and as a result output voltage changes. & Output voltage is not affected due to changes in temperature. \\

\bottomrule

\end{tabularx}
\end{center}
\caption{Comparision of Resistance shunt and Hall effect Current Measurement.}
\label{compresandhall}
\end{table}

\newpage

\section{Temperature Measurement Module}
\label{temperaturemeasurementmodule}

Temperature is an another important parameter of monitoring and control of a Li-ion cell. The surface temperature of a cell and its surrounding temperature have a greater impact on the life of the cell. Charging and Discharging of a Li-ion cell are electrochemical reaction which are basically temperature dependent. The working temperature range of a Li-ion is from -20 to 60$^\circ$C \cite{andrea2010}. Operating a cell beyond these limits would result in a deviation in the specified performance of the cell as shown in figure \ref{tempchar}.

\begin{figure}[h!]
\centering
\includegraphics[width=10cm]{./Images/tempcharbat.pdf}
\caption{Discharge Characteristic of Li-ion cell at various temperatures. \cite{battemp}.}
\label{tempchar}
\end{figure}

Increasing the temperature increases the rate of the electrochemical reaction and therefore more power could be extracted out of the cell.
In contrast temperature increase above the operating range of the cell would also result in unwanted chemical reactions to occur in the cell and eventually would damage the cell. 
Moreover the active chemicals in the cell would breakdown resulting in an explosion. 
Similarly at lower temperatures below the operating range of the cell Lithium plating of the anode will decrease the cell's reaction rate and thereby resulting in a reduction of capacity. 

\noindent\textbf{Thermal Runaway}\\
Several exothermic electrochemical reactions occurs inside a Li-ion cells. The heat generated by these reactions should be dissipated to the environment in order to prevent damage of the cell. Thermal Runaway of a cell occurs when the rate of heat generation due to the electrochemical reactions is higher than the rate of heat dissipation to the environment.

\noindent\textbf{Causes for thermal runaway}\\
The current flowing through the cell would heat up the cell due to the I$^{2}$R thermal loss. This will increase the temperature of the cell. 
During charging or discharging heat produced due to the exothermic chemical reactions taking place inside the cell would increase the temperature of the cell. 
Excessive ambient temperature or poor cooling would reduce the rate of heat dissipation of the cell.


%\begin{itemize}
%\item The current flowing through the cell would heat up the cell due to the I$^{2}$R thermal loss. This will increase the temperature of the cell. 
%\item During charging or discharging heat produced due to the exothermic chemical reactions taking place inside the cell would increase the temperature of the cell. 
%\item Excessive ambient temperature or poor cooling would reduce the rate of heat dissipation of the cell.
%\end{itemize}

Thermal management involves monitoring and control of the cell temperature within its safe operating range and thereby increasing the efficiency of the cell. 

\noindent\textbf{Temperature Measurement Sensors}\\
In a Distributed Battery Management System each cell is monitored with a dedicated temperature measurement sensor that accurately monitors the cell temperature and provides the input to ESIC. Depending upon the size of the cell one or more than one temperature sensor could be mounted on various locations of the cell to get the temperature profile of the cell. 
Temperature sensors should be highly reliable and accurate. Moreover the mechanical construction of the sensor should be flexible to be mounted on the surface of the cells. In addition the sensors should be smaller in size and doesn't required special mounting arrangements. 

\subsection{Resistance Temperature Detectors(RTD)}
The resistance temperature detectors work on the principle that the resistance of a metal changes with changes in temperature according to the equation \ref{Rtemperature}
\begin{align}
R_{T} &= R_{r} (1 + \alpha * \triangle T) \label{Rtemperature}
\end{align}
where $R_{T}$ is the resistance of the metal at temperature T$^\circ$C and $R_{r}$ is the resistance of the metal at T$_{r}^\circ$C, $\alpha$ is called the temperature coefficient of material and $\triangle$T is the change in temperature $\triangle T = T - T_{r}$.  
Therefore the temperature is given by 
\begin{align}
\triangle T &= \frac{1 - \frac{R_{T}}{R_{r}}}{\alpha}\\
T &= \triangle T + T_{r}
\end{align}

RTD is a positive temperature coefficient device that is the resistance of the metal increases with increase in temperature.  
The RTD's are characterized by their linear relationship with temperature input as shown in figure \ref{RTDchar}. 


\begin{figure}[h!]
\centering
\includegraphics[width=8cm]{./Images/RTDchar.pdf}
\caption{Resistance of RTD with varying temperature. \cite{RTDchar}.}
\label{RTDchar}
\end{figure}

The RTD's are small in volume and they doesn't required huge mounting space which allows them to be attached to each of the cell in a smart battery pack.
Moreover they are highly accurate and have a very fast response time compared to that of the thermocouple. 
Also the fabrication of RTD is fairly simple and could be best suited for mass production.\\

Micro RTD as explained in \cite{MicroRTD} is proposed in this thesis for individual cell temperature measurement in a smart battery pack. The Micro RTD is shown in figure \ref{micrortd}.\\

\begin{figure}[h!]
\centering
\includegraphics[width=7cm]{./Images/MicroRTD.pdf}
\caption{Flexible Micro RTD \cite{MicroRTD}.}
\label{micrortd}
\end{figure}


The inner temperature of the cell increases by 3$^\circ$C than the outer surface temperature during 1C charging and discharging. The thermal management functionality of ESIC is required to account for this temperature increase and control the heat dissipation of the cell.
The dimensions of this Micro RTD is very small in the range of 620 $\mu$m x 620 $\mu$m as shown in figure \ref{dimofmicrortd}. This allows it to be integrated inside the cell for in-situ temperature monitoring.   

\begin{figure}[h!]
\centering
\includegraphics[width=7cm]{./Images/microscopeofrtd.pdf}
\caption{Microscopic view of Micro RTD \cite{MicroRTD}.}
\label{dimofmicrortd}
\end{figure}

The Micro RTD proposed here is a gold RTDis fabricated in a planar process over Silicon substrate. The RTD is surrounded with a thin layer of parylene film to protect it from erosion and to provide mechanical strength. Detailed fabrication procedure is explained in \cite{MicroRTD}. 

\subsection{Printed Resistive Temperature Sensor}
Another temperature sensing method as explained in \cite{Lorentz} employs printed resistive sensor for temperature measurement in cells. \\
\noindent\textbf{Feautres}
\begin{itemize}
\item Fabrication of the sensor is very simple and therefore mass production of the sensor could be done efficiently and with less cost. 
\item High sensitivity to temperature variations thereby small changes in temperature could be easily sensed.
\item The low thickness profile of the sensor allows it to be placed on the surface of the cell or inside the cell with less efforts. 
\end{itemize}

\begin{figure}[h!]
\centering
\includegraphics[width=10cm]{./Images/print.pdf}
\caption{Realization of printed temperature sensor \cite{Lorentz}.}
\label{printedsensor}
\end{figure}

These sensors work on the same principle as of RTD, i.e there resistance varies with changes in temperature. 

Four layers constitute the sensor layout as shown in figure \ref{printedsensor}.\\
\noindent\textbf{Plastic substrate}\\
Polyamide foil which is highly stable, flexible and could withstand large temperatures without deformation is used as the plastic substrate.\\
\noindent\textbf{Interdigitated Metal Electrode}\\
This layer provides contact with the resistive sensor. They are formed with a low resistive material such that their resistance changes with temperature is negligible in relation with the changes in resistance of the sensor. Silver ink which has a low resistivity of 5 - 30 $\mu\Omega$ is used as a electrode layer because of cheap availability and easy processing. The electrode layer could also be used as a heating resistor to heat the cell to maintain it at optimum performance temperature in case of winter climate conditions.  \\
\noindent\textbf{Resistive layer}\\
The sensor layer is the resistive layer with a high resistivity metal. The change in temperature will result in a resistance change of the metal as shown in figure \ref{printreschar} which could be sensed by ESIC.\\

\begin{figure}[h!]
\centering

\begin{tikzpicture}
  \begin{axis}[
    scale only axis,
    xmin=20,xmax=70,
 	xlabel=$Temperature$,
    ylabel=Resistance(k$\Omega$)]
	\addplot coordinates {
( 25, 70 )
( 35, 80 )
( 45, 100 )
( 55, 130 )
( 65, 170 )
};
  \end{axis}
  

\end{tikzpicture}
\caption{Resistance Vs Temperature Characteristics of printed temperature sensor \cite{Lorentz}.}
\label{printreschar}
\end{figure}
\noindent\textbf{Passivation layer}\\
The passivation layer servers as a protective layer for the sensor from atmospheric temperature and humidity of the cooling system. 



\newpage