\section{Current Measurement Module}
\label{currentmeasurement}

Measurement of battery current is essential and allows the \ac{ESIC} to perform the following functions \cite{andrea2010}

\begin{itemize}
\item To prevent the cells from being operated outside their \ac{SOA}.
\item To calculate the \ac{DOD}.
\item To calculate the internal DC resistance of the cell and perform IR compensation. 
\end{itemize}

Batteries in electric vehicles are formed by connecting several cells in series as shown in Figure \ref{pack}. As a result the battery pack current is the same in all the cells. Therefore, the measurement of current is not required in each of the individual cells, instead a single current sensor could be used to measure the battery pack current. 
This current sensor is connected to any of the \acp{ESIC} and then that \ac{ESIC} could communicate the current value to all other \acp{ESIC} in the battery pack.
To increase the accuracy of measurement and to improve the reliability of the battery pack, the current measurement could be performed using more than one sensor in different points within the series connected battery pack and the average value of the readings could be used as battery pack current. 

\subsection{Related Work}

Cell current is an important parameter to be measured for controlling the charging, discharging process and also in calculation of the \acf{SOC} of the cell \cite{WangStuart}.
Different methods are employed in literature for current measurement in batteries \cite{currmeas}.
A detailed explanation and comparison of different possible current measurement techniques is provided in \cite{PatelA}. 
The shunt resistor current measurement technique as explained in \cite{Zetex} has high power loss in case of higher operating currents.
Moreover, both high side and low side shunt resistor current sensing have their advantages and disadvantages as explained in \cite{Highside}.
Alternative measuring techniques based on the resistive shunt method as proposed in \cite{Dost} increase the complexity of the measurement system. 
The Current Transformer method is applicable only for measurement of AC currents \cite{Radun} and therefore cannot be employed in case of DC battery current measurement. 
Magentoresistive sensors, as proposed in \cite{Laimer} and \cite{Magneto}, have good sensitivity for current measurement. 
But poor temperature stability and lower measuring range prevents them to be used for battery current measurement application in electric vehicles. 
The Hall effect sensor works on the principle of hall effect and would be the best suitable option for use in high current applications \cite{AllegroDS}. 
The inherent isolation provided between the high power battery pack and the low power measurement electronics is an added advantage for this type of sensors.
Furthermore, zero offset output and good sensitivity enables this measurement technique being suitable for measuring higher currents in electric vehicle application \cite{Infineon}. 
In this thesis the resistive shunt current measurement method is explained and its design issues are analysed. The Hall Effect method for DC current measurement in batteries is favoured in this thesis. 

\subsection{Resistive Shunt Current Measurement}
\label{Resistive}
This method measures the DC battery current by passing it through a very small value resistor called $R_{shunt}$ and measuring the differential voltage $V_{sense}$ generated across it as shown in Figure \ref{ResCM}.\\

\begin{figure}[h!]
\centering
\includegraphics[width=6cm]{./Images/Rshunt2.pdf}
\caption{Shunt Resistor Current Measurement.}
\label{ResCM}
\end{figure}

If the shunt resistor is placed at the top of the battery pack then the measurement technique is called as high side current sensing. 
Alternatively, if the shunt resistor is placed at the bottom of the battery pack then it is called as low side current sensing. 
Advantages and disadvantages of both these methods are described in Table \ref{Currentsensing}.


\begin{table}[h!]
\begin{center}
\begin{tabularx}{\linewidth}{ X  X  X }
\toprule

Application & Advantages  &  Disadvantages \\
\midrule
\midrule

High side current sensing & Battery pack ground is not altered. & High \ac{CMRR} required for the amplifier.\\
Low side current sensing & Low common mode input as one end is referred to ground & Battery pack ground is increased to the value of $V_{sense}$.\\

\bottomrule

\end{tabularx}
\end{center}
\caption{Comparison between high side and low side current sensing.}
\label{Currentsensing}
\end{table}



\noindent\textbf{Design}\\
The shunt is a very high accurate, low value resistor selected based upon the DC current value that is measured. 
The value of $R_{shunt}$ is very small for higher measuring currents, in order to minimize the heat generated across it. 
The voltage $V_{sense}$ produced across the shunt resistor is of very low value and therefore it has to be amplified by a highly accurate \ac{CSM} amplifier before providing it to the ESIC. 
In addition, the CSM should be capable of measuring currents in both the directions, in case of charging as well as discharging. \\
For example, to measure a current of 200$A$ with a power loss of less than 1$W$ and with an accuracy of 1\%, then the value of $R_{shunt}$ is calculated as
\begin{align}
P&=I^2 \cdot R \label{first}\\
R&=\frac{P}{I^2}\\
R&=\frac{1}{200^2}\\
R&=25\mu\Omega \label{powerR}
\end{align}

The resolution of $V_{sense}$ with the above $R_{shunt}$ value for the same current signal varying from 0 to 200$A$ is calculated as
\begin{align}
V_{sense} &= I \cdot R \\
V_{sensemin} &= 0 \cdot 25 \mu\Omega = 0 V\\
V_{sensemax} &= 200 \cdot 25 \mu\Omega = 5 mV \\
V_{sense} for 1 A &= \frac{5}{200} = 25 \mu V \label{second}
\end{align}


To obtain an accuracy of 1\% the offset voltage of the \ac{Op-amp} should be 1\% of $V_{sense}$. That is 
\begin{equation}
V_{offset} = \frac{1 \cdot 25\mu V}{100} = 250 nV
\label{offset}
\end{equation}

According to equation \ref{first}, a low value of resistance R is required in order to reduce the power dissipation across it. 
However, equation \ref{second} show that a low value of R reduces the voltage sensitivity and requires a highly accurate and a very low offset amplifier to amplify the low voltage signal $V_{sense}$. 
Therefore, the selection of $R_{shunt}$ is difficult to satisfy all the specifications of accuracy, power dissipation and sensitivity. \\
Additionally, the resistance value of the $R_{shunt}$ varies with temperature as
\begin{align}
R_{shunt} = R_{shunt0} (1 + \alpha \cdot T)
\label{Rtemp}
\end{align}
where $R_{shunt0}$ is the resistance at room temperature and $\alpha$ is the temperature coefficient of the material and T is the ambient temperature. 
Therefore, temperature compensation has to be performed by the \ac{ESIC} to remove the errors produced due to temperature variations. 
 
 
\subsection{Hall Effect Current Measurement}
\label{Hall}
The Hall effect current sensor works on the principle of the Hall effect. \\

\noindent\textbf{Hall effect}\\
When a current carrying-conductor is placed in a direction perpendicular to the magnetic field, a small voltage is produced proportional to the value of the current and perpendicular to both the directions of current and the magnetic field \cite{CurrentTx}.\\

\newpage

\noindent\textbf{Characteristics}
\begin{itemize}
\item Output voltage is amplified by an inbuilt chopper-stabilized differential amplifier within the sensor. Therefore, no external amplification and signal conditioning circuitry is required. 
\item Highly accurate and reliable in operation over a wide range of temperature. 
\item Output voltage is isolated from the input high current side. Therefore, no external isolation amplifier is required to protect the low voltage electronics from high voltage circuits. 
\item Currents in both the directions could be measured without any additional circuitry.
\item The offset errors are eliminated by the signal conditioning circuits. 
\end{itemize}

\begin{figure}[h!]
\centering
\includegraphics[width=12cm]{./Images/hall.pdf}
\caption[Block diagram of the Hall Effect Current Measurement Sensor]{Block diagram of the Hall Effect Current Measurement Sensor \protect\cite{AllegroDS}.}
\label{Hallsensor}
\end{figure}


The block diagram of a hall sensor from Allegro Microsystem  is shown in Figure \ref{Hallsensor}.
The battery pack current is passed through the input coil $IP+$ and $IP-$. The voltage generated due to hall effect on this coil is amplified and produced at the output terminal $VIOUT$. The signal conditioning circuitry consists of a chopper-stabilized amplifier to amplify the small voltage produced by the hall sensor and an offset correction amplifier to remove the offset error. Moreover, the output voltage is not sensitive to changes in temperature.  In addition, the sensor output is linearly proportional to the input current.
The sensor produces a positive output voltage for currents in one direction and negative output voltage for currents in the opposite direction.
The voltage sensitivity for the 200$A$ sensor as per datasheet \cite{AllegroDS} is 10$mV/A$. This enables the ESIC to easily detect the changes in the input current without any need for a high sensitivity amplification unit.
The accuracy of the sensor is 1.2\% of the full scale current applied.\\  
The internal resistance of the conductor is in the range of 100$\mu\Omega$ and the power loss for a current of 200$A$ is  

\begin{align}
P& = (200)^2 \cdot 100 \mu\Omega\\
P& = 4 W \label{powerlossHall}
\end{align}

Furthermore, the sensor size is very small and could be easily integrated in a \acf{DBMS} architecture. 
%\newpage

A detailed comparison between Resistive shunt and the Hall effect Current Measurement sensors is provided in Table \ref{compresandhall}.
\begin{table}[h!]
\begin{center}
\begin{tabularx}{\linewidth}{ X  X  X }
\toprule

Feature & Resistive Shunt & Hall Effect \\
\midrule
\midrule

Power loss and sensitivity & For measuring current of 200$A$ with 25$\mu\Omega$ resistor the power loss is 1$W$ as per equation \ref{powerR}. The voltage sensitivity is greatly reduced for the same case.  & The resistance of the sensor is 100$\mu\Omega$ and the power loss for 200$A$ is given by equation \ref{powerlossHall} is 4$W$ with a higher voltage sensitivity than the resistive shunt method.  \\
\midrule
%Sensitivity & Very low as per equation \ref{second} the sensitivity is very low in the order of $\mu$V which requires a very high sensitive amplification unit.  & Sensitivity is higher in the order of mV which relaxes the specification on the signal conditioning circuitry.\\
%\midrule
Signal Conditioning & External signal conditioning circuitry is required to amplify the low output voltage produced by the R$_{shunt}$. & Signal conditioning circuitry is inbuilt within the sensor.\\
\midrule

Measuring point & Accuracy is affected by the position of the sensor such as high side or low side. & Measurement is not affected by the location of the sensor. \\
\midrule
Temperature Sensitivity & Resistance changes with temperature and as a result output voltage changes. & Output voltage is not affected with changes in temperature. \\

\bottomrule

\end{tabularx}
\end{center}
\caption{Comparision of Resistive shunt and Hall effect Current Measurement.}
\label{compresandhall}
\end{table}

\newpage