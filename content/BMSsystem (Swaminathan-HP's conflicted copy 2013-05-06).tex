
\chapter{Battery Management System Architectures}
%\label{sec:BMS}

This chapter explains about the exisitng state of the art \ac{BMS} architectures in \acp{EV} in section \ref{BMSstateofart}. A novel \ac{BMS} architecture called \acf{DBMS} is proposed and its features are explained in section \ref{DistributedBMS}. In section \ref{RealizationDBMS} various abstraction levels for the design of the \ac{DBMS} architecture is explained. 
Various building blocks of the \ac{ESIC} are identified and introduced in section \ref{ESICblocks}.
Finally a table comparing the existing \ac{BMS} architectures and the proposed \ac{DBMS} architecture is presented in section \ref{Summary}.

\section{Existing BMS Architectures}
\label{BMSstateofart}

The state of the art \acfp{BMS} in an \ac{EV} are

\begin{itemize}
		\item Centralised Battery Management System
		\item Modular Battery Management System 
\end{itemize}


\noindent\textbf{Centralised Battery Management System}

The centralised Battery Management system has a single master controller and all the individual cells of the battery are monitored by this controller as shown in Figure \ref{CBMS}. 
The advantage of this type of architecture is that, it is compact and less expensive compared to the other approaches because the number of controllers are less \cite{andrea2010}. 
However, this type of architecture has a major disadvantage that the number of wires from the individual cell sensors to the controller are increased, thereby increasing the weight and complexity of the battery pack. 
Moreover for each addition of cells in future, the controller has to be reconfigured or it could also reach its maximum saturation limit on the number of cells count. As a result the centralised architecture is less scalable limiting the addition of new cells. 

The reliability of this architecture is greatly reduced because the master controller represents a single point of failure in the system. Moreover there  is a high probability to have a break in the cables between the sensor and the controller which would result in the shutdown of the entire battery pack. 
The controller scans all the individual cells one after another and as a result the scanning time of the entire battery pack is increased with the increasing number of cells. Moreover the power consumption of this type of architecture is very high as the central controller cannot be turned off \cite{Meyer}.

\begin{figure}[h!]
\centering
\includegraphics[width=6.5cm]{./Images/centralisedBMS1.pdf}
\caption[Centralised Battery Management System architecture]{Block diagram of a Centralised Battery Management System architecture \protect\cite{andrea2010}}
\label{CBMS}
\end{figure}


\noindent\textbf{Modular Battery Management System}

In this architecture 4 to 6 series connected cells are grouped together to form a module. 
The battery pack is made of connecting several modules in series. 
Each module is provided with a separate monitoring and controlling unit called \ac{MMU}. 
All the cells in the module are connected to the \ac{MMU} via dedicated wires. The \ac{MMU} measures the parameters such as voltage, current and temperature of the individual cells in the module. Additionally, the \ac{MMU} also performs cell balancing between the cells within the module. Moreover, the \ac{MMU} can also shutdown the module in case of any faulty conditions in the cells within the module \cite{Brandl}.

The \acp{MMU} from all the modules are connected to a master control unit called \ac{PMU} via an Internal shared \ac{CAN} bus. The \ac{PMU} is the master controller which reads the data from each of the \ac{MMU} and calculates other parameters of the battery such as \ac{SOC}, \ac{SOH} and \ac{RUL}. Additionally, the \ac{PMU} accurately calculates the driving range that could be achieved by the vehicle with the remaining battery capacity in the cells. Moreover the \ac{PMU} also communicates the status of the battery to the \ac{VMS} via an external CAN bus. The entire system architecture is as shown in Figure \ref{MBMS}. 

When compared to the centralised architecture, the wiring harness is reduced because the cells in the module are connected to the respective \ac{MMU} which is placed near the module. Moreover, the \acp{MMU} share a Common bus to the \ac{PMU}. Monitoring accuracy and speed is also increased compared to that of the centralised architecture as the \ac{MMU} monitor only a small group of cells. Power consumption of this type of architecture is lesser compared to that of centralised monitoring system because the \ac{MMU} can be put into idle mode after scanning the cells in the module. 

\begin{figure}[h!]
\centering
\includegraphics[width=11cm]{./Images/modularBMSnew2.pdf}
\caption[Modular Battery Management System architecture]{Modular Battery Management System architecture \protect\cite{Brandl}.}
\label{MBMS}
\end{figure}
 
However, the \ac{MMU} in this architecture could be larger in size depending upon the number of cells in the module and they also require special mounting arrangements near the module. The reliability of this architecture is decreased because failure of a single \ac{MMU} renders the module unresponsive and the vehicle has to be stopped immediately. Addition of new cells requires spare space to be reserved in the module and the \ac{MMU} has to be reconfigured.




\section{Novel Distributed Battery Management System}
\label{DistributedBMS}

In this thesis, a novel method of implementing a \acf{BMS} architecture called \acf{DBMS} is proposed. In this architecture, each cell is monitored and controlled by a dedicated low power \acf{ESIC}. The cell with the \ac{ESIC} attached to it is called as a smart cell. A Smart battery pack is formed by connecting several smart cells in series as shown in Figure \ref{DBMS}.Thus the whole battery pack is distributed in arrangement providing improved accuracy in monitoring and control over cell balancing.

\begin{figure}[h!]
\centering
\includegraphics[width=2.45cm]{./Images/DistributedBMS.pdf}
\caption[Distributed Battery Management System architecture]{Distributed Battery Management System architecture with ESICs attached to the cell.}
\label{DBMS}
\end{figure}

\subsection{Features of DBMS}
In contrast to the existing state of the art architectures as explained in section \ref{BMSstateofart}, where a single controller monitors a group of cells or the whole battery pack, whereas in \ac{DBMS} architecture each and every cell is monitored by a dedicated cell monitoring IC.
This novel concept increases the monitoring accuracy and reduces the scanning time compared to other architectures.
The fundamental building block of the \ac{DBMS} architecture is the \acf{ESIC} attached to each of the cells in the pack. 
The \ac{ESIC} is equipped with a real time monitoring system to accurately monitor the parameters like voltage, current and temperature of the cell.
In addition, the ESIC possess an extensive computational capability to precisely compute \ac{SOC}, \ac{SOH} and \ac{RUL} of the cell.
On a first look, the \ac{DBMS} architecture could be more expensive because the number of controllers are increased compared to the other \ac{BMS} architectures. 
However, the \ac{ESIC} is an \ac{ASIC} and due to less manufacturing cost of semiconductor IC's, the cost of this architecture is reduced compared to other architectures. 

The \ac{ESIC}'s are smaller in size which allows them to be attached directly to the cell without any additional space and mounting requirements.  
Additionaly the wiring harness and complexity is greatly reduced because the \ac{ESIC} is directly attached to the cell it is monitoring.
Also lesser number of cables will decrease the weight of the batery pack which ultimately increases the driving range of the vehicle.
The power for the operation of \ac{ESIC} is drawn from the monitoring cell and therefore the \ac{ESIC}'s are designed to operate in the $\mu$W range to reduce the power consumption. 
Moreover, the ESIC's are put into idle mode when not in operation, thereby reducing the power consumption to a great extent.

Addition of new smart cells to the existing smart battery pack is easier compared to other \ac{BMS} architectures, because it only involves an additional cell with an \ac{ESIC} attached to it. 
No additional wiring is required.
This increases the scalability of the battery pack, as the number of cells could be increased without any additional wiring or reconfiguration of the existing system. 
Depending upon the emergency requirement of the vehicle, the \ac{DBMS} architecture could have one or more spare cells in the battery pack. 
This will increase the reliability of the system and will also avoid any single point of failure that results in the shutdown of the battery pack. 
In section \ref{controllingmodule} it would be shown that instead of having one cell as spare it is advantageous to rotate the spare cell through the battery pack to avoid ageing effects. 
The \ac{DBMS} architecture is capable of safely isolating a single smart cell from the smart battery pack if in case the cell has become weak or not performing according to the specification. 
The \ac{ESIC} is provided with cell isolation switches to completely isolate the cell from the battery pack. 
The communication between the \ac{ESIC}'s is accompolished either by wireless communication or \ac{PLC} using the DC power line cable of the battery itself.
Therefore no additional wiring requirements are necessary for the implementation of communication in the \ac{DBMS} architecture.

In this thesis, a novel concurrent active cell balancing for the \ac{DBMS} architecture is proposed. 
This enables the \ac{DBMS} architecture to transfer charge between any two smart cells without any additional wiring between them.
Therefore the cell balancing is made faster and more efficient than compared to the existing state of the art approaches. 
More details on this architecture is provided in section \ref{newcellbalancing}. 


In contrast to the advantages provided by the DBMS architecture, the initial efforts required for the design and integration of the ESIC to the cell without any damage to the cell characteristics are higher.



\section{Realization of Distributed Battery Management System}
\label{RealizationDBMS}

As seen in previous section \ref{DistributedBMS} the \acf{DBMS} architecture when compared to other \ac{BMS} architectures is more accurate in measurement and efficient in controlling. In this thesis, a three level realization technique is proposed for an efficient implementation of DBMS architecture. They are
\begin{itemize}
\item Cell level (Computation)
\item Battery level (Communication)
\item System level (Algorithm)
\end{itemize}
\noindent\textbf{Cell level}\\
Cell level realization involves the design of the physical hardware layer of ESIC over which the DBMS algorithm is functioning. 
The circuit architectures required for the implementation of sensing and controlling functionality is realized in this level. 
The design of the hardware blocks of ESIC should be independent of the cell type, that is an ESIC could be attached to any of the cell in the pack to make it a smart cell thereby increasing the flexibility of the system. 
The monitoring blocks should be designed to provide high accuracy and sensitivity in measuring the cell parameters.
Moreover the signal conditioning blocks of ESIC should be capable of converting the measured cell parameters into a form that is readable by the higher realization levels of the DBMS architecture such as communication and computation modules.
The measurement module should also be immune to the noise present in the battery pack and shoulde be capable of operating under various adverse working conditions. 
Moreover, the hardware blocks should be designed to operate as low power modules to minimize the power drawn from the monitoring cell. 
Also the size of the ESIC should be very small which enables it to be mounted on the monitoring cell directly without any special arrangements.
In addition the ESIC should be of a robust design to withstand the harsh environments prevalent inside the battery pack.
The cell architecture provides increased safety and reliability by isolating the monitoring cell from the pack in case of non conformance with the specification.\\ 


\noindent\textbf{Battery level}\\
The battery level realization focusses on the communication framework required between the \acp{ESIC} in the \ac{DBMS}. 
The key advantage of the DBMS architecture is the less number of wiring within the battery pack. 
As a result, the communication between the ESIC's could be implemented as either wireless or power line communication as both of these techniques doesn't require any additional wiring between the \acp{ESIC} .
Most of the monitoring and controlling functionality performed by the ESIC is independent of the functions on other \ac{ESIC}'s.
Therefore the communication payload is very less and simple methodologies like broadcast and listen in predefined time slots for all the ESIC's could be implemented.
Instead of concentrating on the payload and the sophisticated protocols for communication, more focus is provided in establishing a highly reliable, fault tolerant and noise immune system in order to provide high degree of performance even under the noisy environment inside the battery pack. 
Each \ac{ESIC} in the \ac{DBMS} architecture is provided with a specific address for communication with the other \acp{ESIC}.
Therefore in case of addition of new smart cells, the communication system is self realisable without any need for reconfiguration of the existing \ac{ESIC}'s in the battery pack. 
In addition, a synchronous communication interface is required to implement the novel cell balancing algorithm proposed in this thesis. 
More details for the synchronization is discussed in section \ref{communication}.\\

\noindent\textbf{System level}\\
The system level or the algorithm level involves the computations and controlling functionalities of the \ac{DBMS}. 
The system level is the top level and the brain of the \ac{DBMS} architecture.
It also controls the other two levels of the architecture. 
The system increases the efficiency by controlling the charging and discharging of the cells, thereby maintaining a controlled energy flow in and out of the battery pack. 
The system level interacts with the cell level in order to get the measured cell parameters and calculates the SOC, SOH and RUL of the cell.
From the individual cell \acp{SOC} the algorithm calculates the overall \ac{SOC} of the battery pack.
These values are communicated to the \ac{VMS} indicating the remaining charge present inside the smart battery pack which is an indication for the driving range that could be covered by the vehicle.
Also during power on, the system performs a \ac{POST} after which only healthy smart cells are connected in series to form a smart battery pack. 
More details on \ac{POST} is provided in section \ref{controllingmodule}. 
The system also checks for a periodic heartbeat signal from the \acp{ESIC} through which the \ac{DBMS} algorithm distinguishes between failure of an \ac{ESIC} from that of the cell.
In addition the system level controls which cells to be balanced through the novel cell balancing architecture proposed in this thesis. 
More sophisticated control and optimization cell balancing algorithms are implemented to increase the efficiency of the battery pack as explained in section \ref{newcellbalancing}. 
The system also interacts with other controllers in the vehicle to maintain the efficiency of the battery. 
For example it communicates with the \ac{HVAC} system to cool down the battery when it is too hot or to heat it when the temperature is too low for the electrochemical reactions to take place inside the cell. 


\section{Block Diagram of ESIC}
\label{ESICblocks}
The functional block diagram of the ESIC is as shown in Figure \ref{smartcellesic}.\\ 
\begin{figure}[h!]
\centering
\includegraphics[width=14cm]{./Images/smartcellblocks.pdf}
\caption{Block diagram of ESIC.}
\label{smartcellesic}
\end{figure}

\noindent\textbf{Sensor Module}\\
The Sensor Module measures the parameters like voltage, current and temperature of the cell. Also it converters the measured data into digital form using an \ac{ADC} and provides it to the computation module. More details on each individual sensor functionality, their design and implementation are provided in chapters \ref{sec:ESICdesign}, \ref{implementation}.  \\

\noindent\textbf{Computation and Controlling module}\\
This is the brain of the ESIC. It interacts with all the blocks of the \ac{ESIC}. It reads the cell parameters from the sensor module and calculates the SOC, SOH and RUL of the cell. It also interacts with the communication module to communicate with other \ac{ESIC}'s in the pack. Detailed algorithms for computation of \ac{SOC}, \ac{SOH} and \ac{RUL} are discussed in section \ref{Computation}. 
The controlling functionality protects the cell from any adverse operating conditions. It maintains the operation of the cell within its \ac{SOA} as explained in section \ref{controllingmodule}.\\

\noindent\textbf{Cell Balancing Module}\\
The cell balancing module minimizes the voltage variations between the cells in the battery pack.
This module receives information from the controlling module on which smart cells in the battery pack are involved in the balancing process and controls the novel active cell balancing hardware block accordingly as explained in section \ref{cellbalancing}.\\

\noindent\textbf{Power Supply Module}\\
The power supply module receives power from the monitoring cell and provides constant regulated power required for the efficient functioning of all the other modules in the \ac{ESIC}. 
High efficiency in power conversion prevents the cell from being over loaded.
Moreover the module is turned off when the \ac{ESIC} is not in operation to reduce the power consumption. The type of power supply conversion and their design methodology is provided in section \ref{powersupplymodule}\\

\noindent\textbf{Communication Module}\\
This module establishes communication between all the \acp{ESIC} in the \ac{DBMS} architecture without any additional wiring requirements. 
Two possible methods of communication implementation are identified and analysed in detail in section \ref{communication}. 


\section{Summary}
\label{Summary}

From this chapter it is evident that the proposed \acf{DBMS} architecture, is more efficient in comparison with the other types of \ac{BMS} architectures. Comparison between the different BMS architectures is summarized in Table \ref{tab:myfirsttable}. Thus the DBMS architecture provides an efficient, safe and reliable Battery Management System.
Because of the accurate monitoring and controlling of the cell parameters provided in the \ac{DBMS} architecture, cells with more variance could be combined to form a battery pack. 
This reduces the overall manufacturing costs of the cell and also maintains the efficiency of the battery pack. 

\begin{table}[ht]
\begin{center}
\begin{tabular}{lccc} 
\toprule

Features & Centralised&  Modular &  Distributed\\
\midrule
\midrule


Accuracy & \tick & \tick\tick & \tick\tick\tick\\
Reliability & \tick & \tick\tick & \tick\tick\tick\\
Scalability & \tick & \tick\tick & \tick\tick\tick\\
Safety  & \tick & \tick\tick & \tick\tick\tick\\ 
Cost & \tick & \tick\tick & \tick\tick\tick\\
Wiring Complexity  & \tick & \tick\tick & \tick\tick\tick\\ 
Power consumption & \tick & \tick\tick & \tick\tick\tick\\
Design effort & \tick\tick & \tick & \tick
\\
\bottomrule
\tick = Good; \tick\tick = Better; \tick\tick\tick = Best
\end{tabular}
\end{center}
\caption{Comparision between different \ac{BMS} architectures.}
\label{tab:myfirsttable}
\end{table}


\newpage



