\section{Cell Balancing Module}
\label{cellbalancing}
In a series connected battery pack it is important to maintain the SOC of all the individual cells to be equal. 
During charging, if one of the cell has a higher value of SOC than the other cells, it would reach its full state of charge faster and this would stop the charging process, even though there are some cells that are yet to be charged fully. 
Similarly during discharging, the cell with a lower value of SOC than other cells will stop the vehicle, even though there are some cells which can still supply energy. 
These actions reduce the total useful life of the battery. 
One of the key requirement of the \ac{BMS} is to perform cell balancing to avoid SOC variations between the cells and thereby increasing the efficiency and life of the battery pack. 

\subsection{Related Work}
\label{relatedwork}
Cell balancing is an important task performed to equalise the SOC of all the individual cells in a battery. 
It has been the topic of research for many years.

Depending upong their implementation cell balancing could be classified into two types
\begin{itemize}
\item Passive 
\item Active 
\end{itemize}
A detailed analysis on the existing state of the art cell balancing techniques are provided in \cite{Daowd},\cite{moore2001}.
Also a comparison between passive cell balancing and active cell balancing is provided in \cite{Waichung}.\\
  
\noindent\textbf{Passive}\\
The passive cell balancing is done by discharging the excess energy of a cell through a resistor as shown in Figure \ref{passive}. 
In the fixed resistor method as shown in Figure \ref{passive}.(a) the resistance value cannot be changed and the cell would discharge the same amount of energy into the resistor. 
This would take place all the time and eventually the cell may get completely discharged by simply parking for a long duration of time.
This drawback is overcome by using a switch to turn on the balancing resistor only when it is required is as shown in \ref{passive}.(b). 
Eventhough this method avoids the draining of the cell when it is not in use, it is not suitable for varying the balancing current for different SOC values of the cell as the resistor value is fixed.
The third method as shown in Figure \ref{passive}.(c) uses a \ac{MOSFET} in triode region so that a variable resistance value could be used, depending upon the amount of excess energy to be removed \cite{Phung}, \cite{JianCao}. 

\begin{figure}[h!]
\centering
\includegraphics[width = 15.5cm]{./Images/passive.pdf}
\caption[Passive cell balancing]{Passive cell balancing (a) Using a fixed resistor $R_{balance}$, (b) Using the fixed resistor with a switch S1, (c) Using MOSFET in triode region as resistor.}
\label{passive}
\end{figure}

However all of these passive balancing methods waste the excess energy stored in the cell as heat across the resistance. So the overall efficiency of the battery pack is decreased. \\


\noindent\textbf{Active}\\
In contrast, the active cell balancing methods involve the transfer of excess charge from a cell with higher SOC to a cell with lower SOC.\\

\noindent\textbf{Capacitor}\\ 
One way of implementing active balancing could be using a capacitor as an active element to transfer charge between imbalanced cells \cite{Kobzev},\cite{Baughman},\cite{Pascual}. 
In these methods a capacitor is used to transfer the excess charge from one cell to another as shown in Figure \ref{indbal}.(a).  
The capacitor based methods are slower in charge transfer process and are characterised by an inherent energy loss of the capacitors.\\ 

%\begin{figure}[h!]
%\centering
%\includegraphics[width=7cm]{./Images/capbalance.pdf}
%\caption{Active cell balancing using capacitors.}
%\label{capbal}
%\end{figure}



\noindent\textbf{Inductors}\\
In \cite{kutkut} used an inductor as shown in Figure \ref{indbal}.(b) in place of capacitor to eliminate the energy loss present in the capacitors. 
Though it is faster than the capacitor based implementation, this method could only transfer charge between adjacent cells, thereby increasing the balancing time if the unbalanced cells are at the top and bottom of the pack. 
Also architectures such as \cite{Sang} which could transfer charge from any specific cell to any cell in the battery pack are available, they increase the number of wiring and complexity of the battery pack. 
The size of the system is also increased and therefore they are not applicable for a DBMS architecture.\\

\begin{figure}[h!]
\centering
\includegraphics[width=13cm]{./Images/capindbalance.pdf}
\caption[Active cell balancing]{Active cell balancing (a) Using Capacitors, (b) Using inductors}
\label{indbal}
\end{figure}




\noindent\textbf{Transformers and DC-DC converters}\\ 
Methods explained in  \cite{Jong},\cite{JianCao} uses multi winding and multiple transformers respectively to actively transfer charge from one cell to another or from the whole battery pack to one single cell. 
The multi winding transformers are custom made transformer, therefore they are expensive.
Also the size of these transformers are huge and therefore could not be integrated into ESIC. 
Moreover for each addition of new cell to the pack the transformer wiring has to be changed which increases the complexity of the system. 
In addition they require special mounting arrangements within the pack, so they are not suitable for the DBMS application. 
The DC-DC converter based balancing techniques could be used for active cell balancing as explained in \cite{Yuang},\cite{WeiHong},\cite{Xilu}. 
These methods involve complex control strategy and excessive switching actions. 
Moreover the high frequency operation of these converters increases the noise in the battery pack. 
Also the charge transfer is only possible within adjacent cells and additional wiring or transformer is required to perform non adjacent cell balancing. 
Each of these methods have their own advantages and disadvantages as explained in \cite{Daowd}. 


\subsection{Novel Concurrent Active Cell Balancing}
\label{newcellbalancing}

To overcome the above mentioned difficulties and to improve the efficiency of the battery pack by performing effective cell balancing, this thesis proposes a novel active cell balancing architecture that enables both bi-directional and non-adjacent charge transfer between cells.
For example, in a series connected battery pack of 100 cells, if the top and the bottom cell are need to be balanced, then the existing balancing methods explained in section \ref{relatedwork} would transfer charge from cell 1 to 100 through all the in between cells. 
This increases the balancing time and reduces the life of other cells as the number of charge/discharge cycles are increased.
The novel active cell balancing architecture as proposed, posses the isolation switches to isolate the in between cells and performs the charge transfer between the two specific cells that are out of balance, without disturbing the other cells in the pack. 
This significantly reduces the time that is required to perform the cell balancing, and also maintains the life of the other cells.
As a result, the lifetime, availability, and efficiency of the battery pack is significantly improved.


\begin{figure}[h!]
	\begin{center}
		\input{tikz/highlevelees.tikz}
	\end{center}
	\caption[Novel active cell balancing]{Novel active cell balancing.(a)Illustration of the battery with series connected cells (b)  Variation in SOCs of individual cells in the battery pack.}
	\label{fig:highlevel}
\end{figure}

The conceptual diagram of the smart battery pack with variations in charge among the cells is shown in Figure \ref{fig:highlevel}. 
In that B$^{1}$, B$^{2}$ to B$^{7}$ represents the individual smart cells. 
The arrows point out the charge transfer directions i.e from cell 1 to cell 2 and from cell 7 to cell 4.
The key advantage of the proposed cell balancing architecture is the ability to equalise between any two non adjacent cells without interrupting the in between cells in the pack (cells 7 and 4 are balancing cells and cells 5 and 6 are inbetween cells in this example). 



The realization of this novel active cell balancing functionality is done through the following steps 
\begin{itemize}
\item A novel homo-geneous circuit architecture for efficient charge transfer is designed. 
\item A switching scheme that controls the switches in the architecture to enable charge transfers between non-adjacent cells with a shorter time is proposed. 
\item Development of an analytical nonlinear closed-form model for the charge transfer behavior is developed. 
\item A system-level control algorithm for efficient charge transfer is proposed that equalises the cells in the battery pack. 
\end{itemize}

\noindent\textbf{Circuit Architecture for single cell}\\
The cell balancing circuit architecture consists of homo-geneous modular blocks.
Each cell level block contains six power \acp{MOSFET} and an inductor as shown in Figure \ref{cellbalancingcircuit}. 
This enables the ex-change of charge between non-adjacent cells while it also makes the overall cell integration more flexible.

\begin{figure}[h!]
\centering
\includegraphics[width=10cm]{./Images/celllevelarchitecture.pdf}
\caption{Cell level block diagram.}
\label{cellbalancingcircuit}
\end{figure}


Each cell block consists of three power node connections to the adjacent cell blocks to form a smart battery pack. 
One power MOSFET in series M$^{1}_{s}$ and an another power MOSFET in parallel M$^{1}_{p}$ to the cell, is provided to realise the cell isolation capabilities. 
During normal operation of the battery pack, all M$^{i}_{s}$ are closed while the M$^{i}_{p}$ remains open. 
This functionality enables the circuit to actively route the current through the cells. 
These MOSFETs are also used in over voltage protection and under voltage protection as explained in section \ref{controllingmodule}.
These MOSFETs are of high current rating to handle the entire charging and discharging current of the battery pack. 
Moreover the ON-resistance R$_{ds}$ of these MOSFETs should very small to reduce the power loss due to heating. 

The power loss across a MOSFET with a R$_{ds}$ of 1m$\Omega$ handling a current of 200A is given by
\begin{align}
P &= I^2 \cdot R_{ds}\\
&= (200)^2 \cdot 0.001\\
&= 40 W
\end{align}

Therefore to reduce the power loss, one or more power MOSFETs could be connected in parallel to have a low value of R$_{ds}$.

MOSFETs M$^{1}_{a}$ and M$^{1}_{b}$ are the main balancing MOSFETs which are controlled by \ac{PWM} signals from the ESIC.
These MOSFETs enable the active charge transfer in both bottom and top directions, respectively. 
Diodes $D^1_a$ and $D^1_b$ are protection diodes of the power MOSFETs M$^{1}_{a}$ and M$^{1}_{b}$ respectively, preventing from the high current and voltage spikes during switching. 
MOSFETs $M^1_e$ and $M^1_f$ are horizontal switches, that aid the cell balancing algorithm by avoiding unwanted current flow directions. 
The protection diodes of these MOSFETs should be disabled to avoid the flow of current when the MOSFET is turned OFF.
All the balancing and the horizontal MOSFETs carry only low currents  and therefore they could be low power devices.
The inductor $L^1$ is the active element, that stores and transfer charge from one cell to another cell. 
The inductor stores energy from one cell during one half of the \ac{PWM} signal and delivers the stored energy to another cell during the next half.
Each MOSFET could be either in state \emph{ON} (closed switch, logical 1) or in not conducting state \emph{OFF} (open switch, logical 0), respectively.
Also snubber circuits are provided with the MOSFETs to prevent them from the spikes arising because of the high frequency \ac{PWM} switching function and also from inductor discharge. \\

\noindent\textbf{Switching schematics for the active cell balancing}\\
To ensure an efficient charge transfer between the correct pair of cells, the MOSFETs have to be turned \emph{ON} or \emph{OFF} in the proper sequence.
The cell balancing controller in the ESIC determines the switching state of each of the MOSFETs in the architecture. 
The switching state of the MOSFET could be \emph{OFF}, \emph{ON},  $PWM$ or $\overline{PWM}$ depending upon the charge transfer path and the cells involved in the balancing process. 
\begin{itemize}

\item MOSFETs $M^i_s$ and $M^i_p$ are either kept \emph{ON} or \emph{OFF} depending upon whether the cell has to be included in charge transfer process or has to be bypassed, respectively. 

\item MOSFETs $M^i_e$ and $M^i_f$ are turned \emph{ON} or \emph{OFF} by the ESIC controller in order to direct the current through a certain path or not respectively, such that the charge transfer between two cells is not affected. 

\item Depending upon whether a cell is transferring or receiving charge the MOSFETs $M^i_a$ and $M^i_b$ are operated by either $PWM$ signal or by a $\overline{PWM}$ signal appropriately.

\end{itemize}

An example of the charge transfer process between cells 1,2 and 7,4 is shown in Figure \ref{chargetransfer} and the switching states of the MOSFET are provided in Table \ref{tab:transistorstates} . \\

\begin{figure}[h!]
\centering
\includegraphics[width=8cm]{./Images/chargetransfer.pdf}
\caption[Illustration of charge transfer in the novel active cell balancing.]{Illustration of charge transfer between adjacent cells like B$^{1}$, B$^{2}$ and between non adjacent cells like B$^{7}$, B$^{4}$ }
\label{chargetransfer}
\end{figure}

\begin{table}[h!]
%\begin{tabular}{|c|c|c|c|}\hline
\begin{center}
\begin{tabular}{ccccccc} %\hline
\toprule
$\bm i$ & $\bm M^i_\mosfeta$& $\bm M^i_\mosfetb$ & $\bm M^i_\mosfete$& $\bm M^i_\mosfetf$& $\bm M^i_\mosfetc$& $\bm M^i_\mosfetd$\\ %\hline
\midrule
\midrule
$1$ & $PWM$            & 0              & 1  & 0 & 0 & 1\\
$2$ & 0              & $\overline{PWM}$ & 0 & 1  & 0 & 1\\
$3$ & 0              & 0              & 0 & 1  & 0 & 0\\
$4$ & $\overline{PWM}$ & 0              & 0 & 0 & 0 & 1\\
$5$ & 0              & 0               & 0 & 0 & 1  & 0\\
$6$ & 0               & 0              & 1  & 0 & 1  & 0\\
$7$ & 0              & $PWM$            & 0 & 1  & 0 & 1\\
\bottomrule
\end{tabular}
\end{center}
\caption[Switching states of the MOSFETs in the novel active cell balancing]{MOSFET switch states for concurrent charge transfer from $\bm{B^1}$ to $\bm{B^2}$ and from $\bm{B^7}$ to $\bm{B^4}$. 
$\bm 0$ denotes \emph{OFF}(open) and $\bm 1$ denotes \emph{ON}(closed).}
\label{tab:transistorstates}
\end{table}

\noindent\textbf{Adjacent cells charge transfer}\\
In the example proposed cell B$^{1}$ has a higher charge compared to cell B$^{2}$. 
Therefore excess charge from B$^{1}$ is transferred to B$^{2}$ via inductor $L^1$. 
MOSFETs $M^1_s$ and $M^2_s$ are \emph{ON} and $M^1_p$ and $M^2_p$ are \emph{OFF}, respectively. 
Similarly, MOSFETs $M^1_e$ and $M^2_f$ are kept \emph{ON} by the ESIC in order to form a closed path for the balancing current to flow. 
MOSFETs $M^1_a$ and $M^2_b$ are actuated by $PWM$ and $\overline{PWM}$, respectively as shown in Figure \ref{PWM}

\begin{figure}[h!]
\centering
\includegraphics[width=8cm]{./Images/PWM1.pdf}
\caption[Non-Overlapping $PWM$ and $\overline{PWM}$ signals and inductor current]{Non-Overlapping $PWM$ and $\overline{PWM}$ signals generated by ESIC and the inductor current waveform during charge transfer.}
\label{PWM}
\end{figure}

During the period $T_{ON}$ MOSFET $M^1_a$ is turned \emph{ON} and $M^2_b$ is turned \emph{OFF} by the $PWM$ and $\overline{PWM}$ signals, respectively. 
The current in the inductor $L^1$ increases linearly as shown in Figure \ref{PWM}. 
The excess charge in the cell $B^{1}$ is stored in the inductor $L^1$ during this period. 
Similarly, during $T_{OFF}$ MOSFET $M^1_a$ is turned \emph{OFF} by the $PWM$ and $M^2_b$ is turned \emph{ON} by the $\overline{PWM}$ signals. 
Because of the property of the inductor, the current through the inductor doesn't change its direction. 
Therefore the current continues to flow in the same direction and charges the second cell $B^{2}$. 
The inductor is fully discharged before the application of the next $PWM$ signal as shown in Figure \ref{PWM}. 
The current flow directions for both $PWM$ and $\overline{PWM}$ for charge transfer between cell 1 and cell 2 are shown in pink coloured bold lines in Figure \ref{chargetransfer}. The remaining \acp{MOSFET} are turned \emph{OFF} by the ESIC. \\

\noindent\textbf{PWM ON:} $B^1$ --> $M^1_a$ --> $M^1_e$ --> $L^1$ --> $M^1_s$.\\

\noindent\textbf{PWM OFF:} $L^1$ --> $B^2$ --> $M^2_s$ --> $M^2_f$ --> $M^2_b$.\\

\noindent\textbf{Non-adjacent cells charge transfer}\\
The novel concept of this cell balancing architecture is that it could also perform charge transfer between non adjacent cells, for example as shown between $B^7$ and $B^4$ in Figure \ref{chargetransfer}.
Series \acp{MOSFET} $M^7_s$ and $M^4_s$ are turned \emph{ON} and the parallel \acp{MOSFET} $M^7_p and M^4_p$ are turned \emph{OFF} to connect them in the charge transfer path. 
MOSFET $M^6_p$ and $M^5_p$ are turned \emph{ON} and $M^5_s and M^6_s$ are turned \emph{OFF} in order to completely isolate them, from the charge transfer path as shown in Figure \ref{chargetransfer}. 
$PWM$ and $\overline{PWM}$ signals are applied to MOSFET's $M^7_b$ and $M^4_a$ respectively, to equalize them. 
Also, \acp{MOSFET} $M^4_e$ and $M^5_f$ are kept \emph{OFF}, to avoid the flow of balancing current and therefore directing it in such a way to transfer charge from $B^7$ to $B^4$.
The current flow directions during both $PWM$ and $\overline{PWM}$ are shown as blue coloured dotted arrows in Figure \ref{chargetransfer}. All the other \acp{MOSFET} are turned OFF.\\

\noindent\textbf{PWM ON:} $B^7$ --> $L^6$ --> $M^6_e$ --> $M^7_b$ --> $M^7_f$ --> $M^7_s$.\\

\noindent\textbf{PWM OFF:} $L^6$ --> $M^6_e$ --> $M^6_a$ --> $M^5_b$ --> $M^4_a$ --> $M^3_f$ --> $B^4$ --> $M^4_s$ --> $M^5_p$ --> $M^6_p$.\\

By applying the non-overlapping $PWM$ and $\overline{PWM}$ signals, the energy loss is minimized, by avoiding any short circuit current paths in the circuit. 
Also the power loss is reduced by the use of \acp{MOSFET} $M^6_a$ and $M^5_b$ for the inductor return current instead of diodes $D^6_a$ --> $D^5_b$ as in \cite{kutkut}. \\

\noindent\textbf{Behavioural design of the charge transfer circuit}\\
In this thesis, a detailed analysis of the behaviour of the cell balancing circuit is done and thereby analytical closed form model of the balancing architecture is developed as explained in section \ref{systemmodel} and \ref{linearmodel}. 
The complete charge transfer process could be split into two phases, $T_{ON}$ and $T_{OFF}$. 
For accurate modelling, the behaviour of the circuit during each switching phase has to be analysed. \\

\noindent\underline{\textbf{$\bm{T_{ON}$}}}\\
\begin{figure}[h!]
\centering
\includegraphics[width=5cm]{./Images/equi1.pdf}
\caption{Equivalent circuit during $T_{ON}$.}
\label{equi1}
\end{figure}


The equivalent circuit of the charge transfer block during phase $T_{ON}$ is shown in Figure \ref{equi1}. 
The resistance $R_{s}$ is the sum of all the resistance that comes in the current flow path, i.e the series resistance of the cell, ON resistance of the MOSFETs in the charge transfer path, and the series resistance of the inductor. 
The inductor current during $T_{ON}$ and $T_{OFF}$ is shown in Figure \ref{PWM}. 
Applying KIRCHHOFF's Voltage Law to the equivalent circuit during $T_{ON}$ will provide 
\begin{equation} \label{eq:kvl_preview}
L^1 \cdot \frac{\diff i}{\diff t} + R_{\xv{send}} \cdot i + \frac{1}{C_{B^1}}\int_0^\Ton i(\tau) \, \diff \tau - V_{B^1} = 0
\end{equation}
where $C_{B^1}$ and $V_{B^1}$ are the capacity and the voltage across the cell $B^1$. 
Solving the second order differential equation \ref{eq:kvl_preview} would provide an expression for the inductor current with time. 
By deciding upon the value of the peak current $i_{peak}$ and substituting it in the current equation \ref{eq:kvl_preview} would give the value of $T_{ON}$. \\


\noindent\underline{\textbf{$\bm{T_{OFF}$}}}\\

\begin{figure}[h!]
\centering
\includegraphics[width=5.5cm]{./Images/equi2.pdf}
\caption{Equivalent circuit during $T_{OFF}$.}
\label{equi2}
\end{figure}
Similarly, the equivalent circuit during $T_{OFF}$ is shown in Figure \ref{equi2}. 
The differential equation during the period $T_{OFF}$ is given as 
\begin{equation}\label{eq:kvl_Toff}
L^1 \cdot \frac{\diff}{\diff t} i+ R \cdot i + \frac{1}{C_{B^2}}\int_{\Ton}^\Toff i(\tau) \, \diff \tau + V_{D_b^2} + V_{B^2} = 0
\end{equation}
where $V_{D_b^2}$ and $V_{B^2}$ represent the forward voltage of diode $D_b^2$ and initial voltage of the cell $B^2$, respectively. 
The initial value of current is the same value as calculated by equation \ref{eq:kvl_preview} during time $T_{ON}$. 
The value of $V_{D_b^2}$ is set to zero because of the use of non-overlapping PWM signals that uses the \acp{MOSFET} in place of diode, during the return path of inductor current to reduce power loss. 
The value of $T_{OFF}$ is the time at which the inductor current falls to zero. 
The ESIC calculates the values of $T_{ON}$, $T_{OFF}$ based on the equations \ref{eq:kvl_preview}, \ref{eq:kvl_Toff} respectively and generates the non-overlapping $PWM$ and $\overline{PWM}$ signals with a period
\begin{align}
T = T_{ON} + T_{OFF}
\end{align} 


\noindent\textbf{Control algorithm for charge transfer}\\
The system level algorithm controls the cell balancing functionality of the proposed architecture.
The circuit enables to implement efficient charge balancing algorithms, in order to minimize the time and power loss during charge transfer. 
The ESIC calculates the difference in SOC values between the cells in the battery and determines which two cells have to be balanced.
ESICs of both the cells are time synchronized to implement the non-overlapping $PWM$ and $\overline{PWM}$ signals effectively. 
Similarly the ESICs of the cells, in between the charge transfer path are also notified, in order to keep the MOSFETs in their corresponding module either \emph{ON} or \emph{OFF} as per the switching schematics to assist the charge transfer. 
The value of $i_{peak}$ which is the balancing current is then determined depending upon various factors like the difference in SOCs between the two cells, number of in between cells in case of non adjacent charge transfer, time required for balancing, allowable power loss during transfer etc. 
Based on the value of $i_{peak}$, the ESIC calculates the values of $T_{ON}$, $T_{OFF}$ and applies the non overlapping $PWM$ and $\overline{PWM}$ to the appropriate MOSFETs to balance the SOC of the two cells.  
The ESIC also optimizes the time taken for the balancing process and also to reduce the power loss during the balancing process.



