
\chapter{Introduction}
\label{Introduction}

\acfp{EV} form the green and emission free transportation system of the future, avoiding environmental pollution and depletion of natural resources. 
The fundamental and important component of an electric vehicle is the Battery, that stores and provides energy for the powertrain and also for the operation of the \ac{EV}.
The batteries are characterized by their high power and energy density.
Important design considerations for a battery in an electric vehicle are as shown in Figure \ref{desBat}
 
\begin{figure}[h!]
\centering
\includegraphics[scale = 0.475]{./Images/batintro.pdf}
\caption[Design considerations for batteries in \acfp{EV}]{Design considerations for batteries in \acfp{EV} \protect\cite{licompare}.}
\label{desBat}
\end{figure}

\noindent\textbf{Cell Chemistry}\\
Rechargeable batteries are in general made of the following chemistries
\begin{itemize}
\item Lead acid 
\item \ac{Ni} 
\item \ac{Li} 
\end{itemize}

Various researches have been done to improve the type of cell chemistry that could be used in batteries.
Recently, \ac{Li} chemistry based cells became the market leader among rechargeable batteries, because of their high energy density over other cell chemistries as shown in Figure \ref{liadv} \cite{Brandl}.
 
\begin{figure}[h!]
\centering
\includegraphics[scale = 0.45]{./Images/liionadv.pdf}
\caption[Chart comparing the specific energy of various cell chemistries]{Chart comparing the specific energy of various cell chemistries \protect\cite{chartcell}.}
\label{liadv}
\end{figure}

\noindent\textbf{Advantages of \acf{Li} technology} \cite{batuniv}\\
The \ac{Li} cells are the lightest and provide excellent electrochemical potential and energy density compared to other cell chemistries.
Their specific energy is twice greater than that of \ac{$NiCd$} cells and four times greater than that of the Lead Acid based systems. 
Moreover the self discharge rate of the Li cells is significantly less, roughly half compared to that of \ac{Ni} chemistry based cells. 
Also their flat discharge characteristics provide wide range of operation and effective utilisation of the stored energy inside the cell. 
The cells doesn't have any memory effect and also requires low maintenance cost. 
Increased specific energy, reduced manufacturing and maintenance cost and absence of toxic materials make the \ac{Li} cells the best candidate for batteries in \ac{EV} powertrain. 
Among the \ac{Li} chemistry based cells \ac{$LiFePO_4$} provide high specific power and thermal stability which makes it suitable for use in \acp{EV}.

\section{Battery Complexity in Electric Vehicles}
\label{batterycomplexity}

The \ac{Li-ion} cell has a nominal operating voltage of around 4 V. 
In contrast the \ac{EV} requires an operating voltage as high as around 400 V.
Therefore the Batteries in \acp{EV} are formed by series connection of 100's of \ac{Li-ion} cells to achieve the required operating voltage as shown in Figure \ref{pack} \cite{Brandl}.
Charging and Discharging of a battery is done in series such that all the cells are charged and discharged simultaneously. 

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.75]{./Images/batEV.pdf}
\caption[Battery pack of an \ac{EV}]{Battery pack of an \ac{EV} containing Li-ion cells connected in series to achieve high voltages.}
\label{pack}
\end{figure}

The advantages of using \ac{Li-ion} cells in batteries doesn't come for free. 
\ac{Li-ion} cells are dangerous when operated outside its \ac{SOA}. 
The \ac{SOA} is bound by voltage, current and temperature of the cell. \\

\noindent\textbf{Safe Operating Voltage}\\
The maximum and minimum operating voltages of a single \ac{Li-ion} cell is 4.2 V and 2.7 V respectively.
The cells get damaged if they are operated outside this area. 
Charging to a voltage higher than 4.2 V will result in an explosion.
Similarly, discharging below the minimum operating voltage will result in failure of the cell.\\ 


\begin{figure}[h!]
\centering
\includegraphics[scale = 0.4]{./Images/celldischarge.pdf}
\caption[Remains of an \acf{EV} after explosion of the battery pack due to over charging.]{Remains of an Electric vehicle after explosion of the battery pack due to over charging \protect\cite{EVburning}.}
\label{explosion}
\end{figure}


Figure \ref{explosion} shows the remains of an electric vehicle after explosion because of over charging the \ac{Li-ion} cells beyond its SOA. 
The cell becomes hot when charged above the maximum operating voltage and bursts into flames destroying itself and also the neighbouring cells in the pack.
Over discharging of the cell below the minimum operating voltage, will result in loss of active material and renders the cell unresponsive for future utilisation. 
Therefore, an accurate voltage monitoring functionality is required to measure the voltage of the cell to maintain it within the safe operating limits. \\

\noindent\textbf{Safe Operating Current} \cite{andrea2010}\\
The charging and discharging currents of a \ac{Li-ion} cell directly affects the life and safety of the cell. 
High charging and discharging currents reduce the life time of the cell. 
Eventhough, the internal resistance of the cell is very low in the range of m$\Omega$s, high currents in the order of 200 A  will produce a significant voltage drop across the cell and therefore the efficiency of the cell is decreased. 
Also higher pulse currents increase the power loss and heat inside the cell causing it to go into thermal runaway. 
The \ac{Li-ion} cell is damaged if it goes into thermal runaway which will be explained in section \ref{temperaturemeasurementmodule}.
Therefore, monitoring the current during charging and discharging is necessary to maintain the cell safety and increase the efficiency.\\ 

\noindent\textbf{Safe Operating Temperature}\\
High operating temperatures will cause the cell to go into thermal runaway and results into flames. 
Elevated temperatures in a fully charged cell causes harmful reactions, that damages the insulation layer inside the cell and creates an electrical short \cite{mpoweruk}. 
Moreover, the life of the cell is reduced drastically when charged or discharged with a surrounding temperature higher than the safe limits of operation. 
Also the efficiency of the cell is greatly reduced when operated at lower temperatures, as the speed of the chemical reactions inside the cell is decreased.
Therefore, it is necessary to heat the cell when working in cold climatic regions. 
More details on temperature effects and control are provided in section \ref{temperaturemeasurementmodule}.\\ 

\noindent\textbf{Computational Complexity}\\
The indication of the cell capacity is required to estimate the driving range that could be covered by the vehicle. 
It is also necessary to properly charge the cell up to the maximum safe value and accurately discharge the cell till the minimum safe limit, thereby extracting completely the useful energy stored in the cell. 
This increases the driving range of the vehicle. 
Moreover, the estimation of cell life would enable timely replacement of the cell before it is damaged because of over cycling.
This also reduces the maintenance cost and inventory cost required for the vehicle. 
The active energy stored in the cell is measured in terms of \ac{SOC}.
The \ac{SOC} of the cell is defined as the ratio of the cell capacity at present to the capacity of the cell when it was new \cite{andrea2010}. 
It varies from 0\% to 100\%.
The capacity of the cell is decreased with usage and accurate estimation of the remaining useful capacity is necessary in order to avoid over draining of the cell. 
Also the cell resistance increases over time and appropriate compensation techniques have to be done in estimating the cell capacity. 
Therefore complex computational algorithms are required to accurately calculate the capacity of the cell and estimate the life of the cell, thereby achieving maximum benefit from the stored energy in the cell. \\

\noindent\textbf{Cell Balancing}\\
As the battery is made up of series connected cells it is important to equalise the voltage of all the individual cells. 
This increases the safety, driving range and efficiency of the vehicle. 
If one of the cell in the pack becomes weak and reaches its minimum operating voltage, the discharging process is stopped even though there are some cells with active energy which could effectively contribute to the battery.
This is shown in Figure \ref{cellbalance}.(a) where the voltage of cell 2 determines the minimum discharge point of the battery pack and the vehicle is stopped eventhough there are cells with active energy that could eventually contribute to the battery pack.
This decreases the effective driving range that could be achieved by the electric vehicle.
Similarly, the charging process is stopped, when one of the cell reaches its maximum operating voltage, although there are some cells which are yet to be charged fully.
In Figure \ref{cellbalance}.(b) cell 100 has reached the maximum operating voltage and therefore stops the charging process, eventhough there are other cells in the pack which are yet to be fully charged. 
This is a continuous process where the minimum discharge point is determined by the weakest cell in the pack(cell 2 in this example) and the maximum charging point is fixed by the more active cell in the pack(cell 100 in this example).
Continuous charging and discharging of such a battery results in a situation, where the weak cell shuts down the battery pack, indicating that it has to be charged and when the pack is connected to the charger, the more active cell cuts the charging denoting that the pack is fully charged. 
This results in replacement of the entire pack even though there are some useful cells in the pack. 
 
This drawback is overcome by performing cell balancing in the battery pack. 
Cell balancing involves equalisation of voltages of the individual cells in the pack. 
This could be done passive where the excess energy in the overcharged cell is wasted across a resistor or active where the excess energy is transferred to the weak cell in the pack without wasting it. 
More details about cell balancing is provided in section \ref{cellbalancing}. 


\begin{figure}[h!]
\centering
\includegraphics[scale = 0.9]{./Images/cellbalintro1.pdf}
\caption[Variation of voltages across individual cells in a battery pack.]{Variation of voltages across individual cells in a battery pack. (a) Minimum discharging point is determined by cell 2. (b) Maximum charging point is determined by cell 100.}
\label{cellbalance}
\end{figure}
 
Therefore an accurate real time monitoring system is required to accurately measure the voltage, current and temperature of the cells in the battery pack. 
Also the system should posses an extensive computation capability to calculate accurately the \ac{SOC} and estimate the useful energy left in the cell. 
Moreover an efficient architecture to perform cell balancing is required in order to minimize the variations between the cells and to increase the overall efficiency of the battery pack. 

\section{Battery Management System(BMS)}
\label{sec:BMS}

\ac{BMS} is a system responsible for monitoring and controlling of each individual cells in the battery pack, thereby increasing the efficiency of the battery pack. 
\ac{BMS} does this by monitoring the cell parameters like voltage, current and temperature of each individual cells and controlling them within their safe operating limits. 
In addition, the \ac{BMS} also calculates the \acf{SOC}, \acf{SOH}, \acf{RUL} and other important parameters that helps to determine the life of the cell. 
Also the \ac{BMS} accurately calculates the driving range that could be achieved by the electric vehicle with the amount of charge stored in the cells. 
Moreover, the \ac{BMS} performs cell balancing between cells, in order to minimize the charge variations between them.
Thus, the \ac{BMS} increases the energy throughput and efficiency of the battery in addition to maintaining the safety and reliability. 

\subsection{Functions of a BMS}
\noindent The important tasks of a Battery Management System are to 
\begin{itemize}
		\item  Measure and control the parameters like voltage, current and temperature of each individual cells in the battery pack.
		\item Calculate the important parameters like SOC, SOH and RUL of the battery.
		\item Maintain the operation of the cell within its safe operating voltage limits, and thereby increasing the efficiency of the battery. 
		\item Implement safe shut down of the battery pack in case of any emergency situations. 
		\item Communicate the status of the battery to the \acf{VMS}. 
		\item Control the charging and discharging process of the cell thereby providing a controlled flow of energy in and out of the battery pack. .
		\item Accurately calculate the driving range of the vehicle that could be achieved with the remaining battery capacity. 
		\item Increase the cell life and driving range of the vehicle by performing cell balancing, which minimizes the variations in charge of the individual cells in the pack.
	\end{itemize}

\subsection{Specification of BMS}

In order to efficiently monitor and control the individual cells in the battery pack and to increase the safety and reliability of the vehicle the \ac{BMS} has to satisfy the following specifications. \\

\noindent\textbf{Monitoring}\\
The \ac{BMS} should posses a real time monitoring system to monitor the cell constantly and should be prompt to react for any malfunction within the cell. 
The monitoring unit should accurately measure the voltage, current and temperature of the cells in the pack. 
The monitoring module of the \ac{BMS} should be immune to any external noise and fluctuations present inside the battery pack. 
Also the measurements made should not affect the normal operation of the cells in the battery pack. \\

\noindent\textbf{Computation}\\
The core of the \ac{BMS} should have an extensive computational capability to calculate the important cell parameters like \ac{SOC}, \ac{SOH},\ac{RUL} etc. 
Also the computation module should calculate the cell ageing and provide information to the user regarding the replacement of the cell. 
The charging and discharging process, driving range and safety functions are all dependent on the computed cell parameters.
Therefore the \ac{BMS} should have an intelligent system for error free computation.\\

\noindent\textbf{Reliability}\\
Reliability is an important feature of the \ac{BMS} because the safety and overall functionality of the vehicle is dependent upon the efficient operation of the \ac{BMS}. 
There should not be any single point of failure in the \ac{BMS} that hampers the normal functionality of the vehicle and results in an unsafe shutdown of the battery pack. 
The \ac{BMS} should also be capapble of performing auto calibration and routine updation in order to keep the system efficiency and reliability. \\

\noindent\textbf{Size and Complexity}\\
The size of the \ac{BMS} directly affects the weight of the battery pack. 
Increasing the size and number of cables will increase the weight of the pack and will result in a reduced driving range that could be achieved by the vehicle. 
Moreover bigger systems are difficult to mount inside the pack and required special mounting arrangements, which is harder to provide. 
Also the number of cables in the pack increases the complexity of the system and troubleshooting of the faulty components becomes difficult. 
This reduces the reliability of the \ac{BMS}.\\

\noindent\textbf{Power Consumption}\\ 
The power for operation of the BMS is obtained from the battery pack. 
Therefore the \ac{BMS} should consume very less power in the range of $\mu$W in order not to drain the battery. 
Also the \ac{BMS} should be capable of entering into low power mode when not in operation to reduce power consumption. \\

\noindent\textbf{Robustness}\\
The \ac{BMS} is placed near the battery pack where severe electrochemical reactions take place inside the cells.
Moreover adverse operating conditions are present inside the battery pack which can hamper the electronics of the \ac{BMS}. 
Therefore, the \ac{BMS} should be robust in nature to operate safely and efficiently even under uneven operating environment. \\

\noindent\textbf{Scalability}\\
The number of cells inside the battery pack could increase depending upon the decreased performance of cells over time.
The \ac{BMS} should be able to accomodate extra cells in future without any additional reconfiguration of the existing system. 
Also the monitoring and controlling functionality of the \ac{BMS}  should not be hampered by the addition of cells. 
Therefore the \ac{BMS} architecture should be scalable to accept additional cells in future to cater the energy demand of the vehicle.\\

\noindent\textbf{Cost}\\
Battery cost is approximately equal to 60\% of the overall vehicle cost. 
Therefore the \ac{BMS} should be cost efficient and highly accurate in monitoring the life of the cell.  
Replacement costs of the faulty component should be cheaper. 
Also the increased accuracy in monitoring and controlling functionality of the \ac{BMS} architecture should reduce the maintenance cost of the vehicle. 


\section{Contribution}
\label{contribution}

To design a \ac{BMS} system satisfying all the above mentioned features, a novel architecture called Distributed Battery Management System is proposed, designed and implemented in this thesis. 
Individual cells in the battery pack are monitored and controlled by an independent \ac{ESIC}.  
Various functional building blocks required for the implementation of \ac{ESIC} are identified and designed.
A prototype model for each of the blocks are implemented and verified with off the shelf components.  
Also this thesis proposes a novel active cell balancing architecture, which could perform concurrent and non adjacent charge transfers efficiently between any two cells in the pack.
The active cell balancing circuit is designed, simulated and implemented with off the shelf components. 
The prototype model of the cell balancing circuit is verified and has been proven to be more efficient and flexible than the existing state of the art cell balancing approaches. 
Moreover, an analytical system level model for simulating charge transfer in a battery pack is developed. 
For system level simulations with 100's of cells connected in series, this model speeds up the simulation time compared to the analog circuit level simulators.
This model also enables to implement charge transfer algorithms and optimization techniques. 


\section{Outline}
\label{sec:Outline}


In chapter 2 the existing state of the art \ac{BMS} architectures are explained in detail. The novel concept of \acf{DBMS} architecture, its advantages and realization methods are also discussed. 
Chapter 3 introduces and explains in detail the design of each individual blocks of \ac{ESIC}.
In continuation, Chapter 4 provides details about implementation of the \ac{ESIC} blocks with standard off the shelf components.
Following that, Chapter 5 analyses and validates the results from the individual blocks implementation and also proposes a system level model for cell balancing. 
Finally, chapter 6 draws a conculsion of the thesis and outlines the future work in the development of the \ac{ESIC}. 








\newpage