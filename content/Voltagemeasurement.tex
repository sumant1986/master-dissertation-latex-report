\section{Voltage Measurement Module}
\label{voltagemeasurementmodule}

Cell voltage is an important parameter to be measured in a battery pack. 
The voltage measurement module measures the voltage of the individual cells in a series connected battery pack. 
The computation module inside \ac{ESIC} uses this measurement data in combination with current and temperature sensor values to calculate the SOC of the cell. 
Also, the cell voltage is used by the controlling module to maintain the operation of the cell within its \ac{SOA}. 
Moreover, the cell balancing module performs the equalisation process based on the measured cell voltage, in order to minimise the variations in SOC of the cells. 
Individual Li-ion cells have an operating voltage of around 4$V$. 
When 100’s of such cells are connected in series, then the top cell would have a voltage of around 400$V$ at the positive terminal and 396$V$ at the negative terminal with respect to the negative terminal of the battery as shown in Figure \ref{voltagemeasurement}. Therefore, the important task of the voltage measurement module is to accurately measure the voltage across each individual cell in a series connected pack.


\begin{figure}[h!]
\centering
\includegraphics[width=15cm]{./Images/voltagemeasurementintro2.pdf}
\caption{Series connected cells in a battery pack of an electric vehicle.}
\label{voltagemeasurement}
\end{figure}

\subsection{Related Work}
\label{RelatedWork}
Many methods are discussed in literature regarding the cell voltage measurement in a series connected battery pack. 
Techniques using resistor divider circuits are explained in \cite{DBG2000}. 
Even-though this method is easy and cheap to implement, they suffer from measurement errors induced because of the variations in resistance values due to changes in temperature.
Moreover, this method requires resistors of huge size in order to limit the current flowing in them, there by reducing the power loss.
Therefore this method could not be implemented in the \ac{ESIC}. 
A similar implementation using resistors is proposed in \cite{Baronti} with a slight modification to reduce the power consumption. This method uses a microcontroller and connects the resistor divider between the cell and the analogue output pin of the microcontroller. When not in operation, the microcontroller pin is driven to high impedance state and therefore no current flows in the circuit.
Even-though the power consumption is reduced to a certain extent, this method does not provide  a solution for varying resistance values with respect to temperatures. 
Another implementation using electromechanical relays to selectively switch the individual cells is explained in \cite{Hande2006}. 
But the relays are huge in size and therefore could not be integrated into the \ac{ESIC}.

Although the measurement circuit involving \ac{BJT} transistors could be integrated into the \ac{ESIC}, the variations of $\beta$ of the transistors with temperature increases the error in measurement \cite{DBG2000}. 
The isolation amplifier method for voltage measurement, as described in \cite{dobkin2011analog}, requires a separate isolated power supply. 
Therefore, this method is not suitable, because the ESIC is powered from the cell it is monitoring and using a separate power supply would increase the wiring complexity and cost of the system.
The pulsed transformer based method as explained in \cite{dobkin2011analog} can be advantageous as the same transformer can be implemented for both voltage measurement and cell balancing also. 
But the size of the transformer is huge and does not allow it to be integrated into a single chip rather than to be implemented as an off chip component.
The \ac{Op-amp} based transfer circuits \cite{Wang2002253} are easier to integrate, and less power consuming. 
But designing an \ac{Op-amp} with high \ac{CMRR} is difficult and also the output of the \ac{Op-amp} is an analogue value which requires an additional Analog to Digital Converter to convert it into digital data. 
This increases the number of components and also design complexity accordingly.
Individual \ac{ADC} cell voltage measurement as explained in \cite{DBG2000} could be implemented for voltage measurement in ESIC, because it is highly accurate and could be easily integrated into the ESIC.\\

\subsection{Individual ADC Cell Voltage Measurement}
\label{ADCvoltage}

The various voltage measurement techniques explained in section \ref{RelatedWork} provide an output value in analogue form. 
This analogue value has to be converted into digital data in order to be handled by other higher level blocks in the \ac{ESIC} architecture (communication and computation), as they process only digital data. 
Therefore, using an \ac{ADC} directly to measure the analogue cell voltage is advantageous over any other techniques described in section \ref{RelatedWork}, as this would effectively reduce one sensing level.
Furthermore, the accuracy of the measurement is higher and also the \ac{ADC} could be easily integrated into the \ac{ESIC}.
Each ESIC is provided with an \ac{ADC} sensor as shown in Figure \ref{ADconversion}.

\begin{figure}[h!]
\centering
\includegraphics[width=6cm]{./Images/newadcmeasurement.pdf}
\caption{Individual cell voltage measurement using \ac{ADC} within the \ac{ESIC}.}
\label{ADconversion}
\end{figure}

\noindent\textbf{Design}\\
Let the maximum and minimum analogue cell voltages be $V^+$ and $V^-$ respectively. Then the converted digital word is given by 

\begin{equation}
\label{ADC}
D_{out} = \frac{(V^{+} - V^{-})}{(V_{r^+} - V_{r^-})} \cdot (2^{N} - 1)
\end{equation}

\noindent where $V_{r^+}$ and $V_{r^-}$ represents the maximum and minimum reference voltages respectively and N represents the number of bits in the digital data. \\

\noindent\textbf{Range}\\
The range of an \ac{ADC} is the analogue voltage that could be effectively measured. The output transfer characteristic of a 3-bit \ac{ADC} is shown in Figure \ref{ADCchar}.\\

\begin{figure}[h!]
\centering
\includegraphics[width=9.5cm]{./Images/adctransfer.pdf}
\caption{Transfer characteristic of a 3 bit \acf{ADC}.}
\label{ADCchar}
\end{figure}

From Figure \ref{ADCchar} the output value $D_{out}$ saturates at the maximum value when $V_{in}$ is greater than $V_{r^+}$ and at the minimum value when $V_{in}$ is less than $V_{r^-}$. 
Therefore, the value of $V_{r^+}$ and $V_{r^-}$ should be selected such that the input analogue voltage is within this range. 
The range of the \ac{ADC} could be increased by increasing the value of $V_{r^+}$ or by decreasing the value of $V_{r^-}$. 
The maximum and minimum operating cell voltages are 4.2$V$ and 2.7$V$ respectively. 
Therefore, the minimum value of $V_{r^+}$ should be greater than 4.2$V$ and the maximum value of $V_{r^-}$ should be lesser than 2.7$V$ to cover the entire range of the cell's operating voltage. 
The value $V_{ref}$ = $V_{r^+}$ - $V_{r^-}$ is called the reference voltage of the \ac{ADC} and is provided by the power supply module in the \ac{ESIC}. \\



\noindent\textbf{Resolution}\\
The resolution of an \ac{ADC} is defined by the number of bits N.
Depending upon the applications, the requirement for the resolution and accuracy of an \ac{ADC} with a $V_{ref}$ of 5$V$ are provided in Table \ref{Davidandrea}.


\begin{table}[ht]
\begin{center}
\begin{tabularx}{\linewidth}{ X  c  c }
\toprule

Application & Accuracy(mV)&  No of bits(N)\\
\midrule
\midrule


For over voltage and under voltage protection & 100 & 6\\
For performing cell balancing & 50 & 8\\
To estimate the \ac{SOC}  & 1 & 12\\ 

\bottomrule

\end{tabularx}
\end{center}
\caption[Resolution and accuracy requirements of \acf{ADC}]{Resolution and accuracy requirements of \acf{ADC} \protect\cite{andrea2010}.}
\label{Davidandrea}
\end{table}

Increasing the value of N will improve the resolution of the \acf{ADC}.
The value $\frac{(V_{r^+} - V_{r^-})}{2^{N}}$ is called as $V_{LSB}$. 
The digital data is incremented by one bit if the corresponding analogue value increases by one $V_{LSB}$.
For example, an ADC with N = 10 bits and $(V_{r^+} - V_{r^-})$ is 5$V$ then the digital data is incremented by one bit if the cell voltage changes by 

\begin{equation}
\label{VLSB}
V_{LSB} = \frac{(V_{r^+} - V_{r^-})}{2^{N}}
= \frac{5}{2^{10} - 1}
= 4.88mV\\
\end{equation}

The \ac{ESIC} requires a high resolution in voltage measurement in order to accurately calculate the \ac{SOC} of the cell. Therefore, the number of bits required for the \ac{ADC} may be even more than the ones specified in Table \ref{Davidandrea}.\\

\noindent\textbf{Rate}\\
The rate of measurement is an another important factor in determining the \ac{ADC} specification. This parameter determines the speed of the conversion and thereby the type of \ac{ADC}. The cell voltage is a slowly changing variable and therefore samples in the millisecond second range are sufficient to operate the cell within its \ac{SOA}. This implies that the \ac{ADC} with operating frequencies in the kilohertz range are best suitable for cell voltage measurement. Furthermore, increasing the speed of \ac{ADC} will also increase the power consumption and the area required by them on the die.\\

\noindent\textbf{Types of \acfp{ADC}}\\
The type \ac{ADC} is selected based upon the requirements of speed, accuracy, size and power consumption. The \ac{ADC} could be classified based on their speed and accuracy as shown in Table \ref{ADclassi} \cite{johns1997analog}.\\
Either the successive approximation or the oversampling type converters are best suitable for cell voltage measurement application because of their high accuracy in conversion and moderate speed of operation. 



\begin{table}[ht]
\begin{center}
\begin{tabularx}{\linewidth}{ X  X  X }
\toprule

Low to Medium speed, & Medium speed, &  High speed,\\
High accuracy & Medium accuracy & Low to Medium accuracy\\
\midrule
\midrule


Integrating & Successive approximation & Flash\\
Oversampling & Algorithmic & Two-step\\
 &  & Interpolating \\
 &  & Folding\\ 
 &  & Pipelined\\
 &  & Time Interleaved\\
\bottomrule

\end{tabularx}
\end{center}
\caption{Comparision of ADC architectures based on speed and accuracy.}
\label{ADclassi}
\end{table}

