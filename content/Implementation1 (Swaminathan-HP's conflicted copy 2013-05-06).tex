\chapter{Implementation}
\label{implementation}

In this chapter a prototypical implementation of various blocks of ESIC, which were designed in chapter \ref{sec:ESICdesign} is presented. 
Section \ref{sensorimp} discuss the hardware implementation of the sensor module of the ESIC. 
It explains in detail the implementation of the voltage, current and temperature measurement modules. 
Implementation of the novel active cell balancing architecture is presented in section \ref{cellbalimp}. 
The results of these implementations are presented and analysed in chapter \ref{results}. 

\section{Sensor Module}
\label{sensorimp}
The sensor module consists of voltage, current and temperature measurement modules. 
\subsection{Voltage Measurement}
\label{voltageimp}
Different possible methods of cell voltage measurement is analysed in section \ref{voltagemeasurementmodule}. 
The measurement technique proposed in the design was directly employing an \acf{ADC} to each cell in the pack, is implemented in this section.
The A/D converter is powered by the monitoring cell itself. 
The type of \acf{ADC} implemented in this thesis is Successive approximation technique which is moderate in speed and accuracy of conversion as shown in Table \ref{ADclassi}. 

Off the shelf ADC's, which could measure the individual cell voltage in a pack is considered for a prototypical implementation of the voltage measurement module in ESIC. 
The digital data output of the ADC is measured using a microcontroller. 
In this thesis, ATmega328p with the Arduino platform \cite{arduino} is used as a microcontroller. 
The ATmega328p microcontroller contains an internal 10-bit ADC that could be used for cell voltage measurement. 
An external voltage of 5 V is provided through the Arduino board, as reference voltage for the conversion process. 
The input cell voltage to be measured is applied between the analog input pin and ground, on the Arduino board. 
The microcontroller computation core initiates the data conversion process and reads the result from ADC data registers after the conversion. 


For a 10-bit ADC with a reference voltage of 5 V, the cell voltage could be calculated from the output data of the A/D converter using the equation \ref{ADC} as follows

\begin{equation}
 D_{out} = \frac{V^+_{cell} - V^-_{cell}}{5} \cdot (2^{10} - 1)
\end{equation}

\begin{equation}
 V^+_{cell} - V^-_{cell} = \frac{D_{out} \cdot 5}{1023}
\end{equation}

\begin{equation}
\label{cellout}
 V^+_{cell} - V^-_{cell} = D_{out}  \cdot 4.887mV
\end{equation}

\begin{equation}
 V_{LSB} = \frac{5}{1023} = 4.887mV
\end{equation}

Therefore each increment of 4.887mV in the cell voltage would be measured by the ADC inside the microcontroller. 
The Arduino core is programmed to calculate the cell voltage from the digital output data of the ADC using the equation \ref{cellout}.
Figure \ref{ardvol} shows the connection of the Arduino to the cell for voltage measurement.

\begin{figure}[h!]
\centering
\includegraphics[width=7.5cm]{./Images/newvoltagemeasurement2.pdf}
\caption[Connection diagram for voltage measurement module]{Example connection diagram for voltage measurement where each cell is provided with a single ADC.}
\label{ardvol}
\end{figure}


\subsection{Current Measurement} 
\label{currentmeas}

Hall effect sensors are used for current measurement in the battery pack. 
The principle of operation of Hall sensors and their design considerations are explained in section \ref{Hall}. 
ACS758X050B Hall Sensors from Allegro Microsystems \cite{AllegroDS} are used to implement the current measurement functionality in the ESIC. \\

\noindent\textbf{Features} \cite{AllegroDS}\\
The sensor could measure bidirectional currents, both during charging and discharging.
The internal resistance of the sensor is very less in the order of 100 $\mu\Omega$, which results in a very low power loss. 
Also, the sensor has an inherent isolation of the low power electronics measurement side from the high power side of the battery pack.
Therefore the opto couplers and bulky isolation transformers with seperate power supply  for isolation is not required.
The sensor is shielded and protected from stray high electric fields and thereby guarantees low output voltage ripples when measuring high input currents. 
The sensor has an inbuilt low offset chopper stabilized BiCMOS amplifier, which provides an amplified output voltage that could be converted in to digital data directly by an A/D converter.   
The fast response time of 3 $\mu$s enables to measure the varying currents flowing in and out of the cells. 
Excellent noise performance over wide temperature range makes this sensor ideal for use with a battery pack. 
The device is smaller in size and could be easily integrated.
Also, the sensor is stable for wide range of temperatures from -40 $^\circ$C to 150 $^\circ$C.
The device operates with a power supply voltage of either 3.3 V or 5 V. 

\noindent The break out board of the sensor is shown in Figure \ref{acs758}. 

\begin{figure}[h!]
\centering
\includegraphics[width=6cm]{./Images/hallsensorpic.png}
\caption[Breakout Board of ACS 758 Hall effect current sensor]{Breakout Board of ACS 758 Hall effect current sensor \protect\cite{currentsensor}.}
\label{acs758}
\end{figure}


The sensor board consists of an \emph{IN} and \emph{OUT} connection, which determines the positive direction of the current flow.
A power supply voltage of 5 V is provided from the Arduino board to the $V_{cc}$ pin of the sensor. 
The sensor provides an analog output signal at $V_{OUT}$, which is fixed at a value of $V_{cc} / 2$ for zero current and varies linearly with the input current.
For a change in the current value of 1 A the sensor output changes by a value of 40mV, thereby providing a sensitivity of 40mV/A.
The output voltage of the sensor for 0A current with a supply of 5 V is 2.5 V.
For currents flowing in the forward direction, the output voltage increases from 2.5 V and for currents in the reverse direction, the output voltage decreases from 2.5V. 
The output of the sensor is connected to the analog input pins of the Arduino board and the internal ADC of the ATmega 328p microcontroller is used to convert the analog value to digital as explained  by \cite{currentsensor}

\begin{equation}
\text{ Output voltage} = \frac{(\text{DigitalValue}-510) \cdot 5 \cdot 0.04}{1024} - 0.04
\end{equation}  

\noindent where the DigitalValue represents the output of the internal A/D converter.
It is subtracted with a value of 510 which is equal to the DigitalValue of $V_{cc} / 2$, thereby producing a 0V output for 0A current. 
Then, the output is multiplied with 0.04 which is the sensitivity of the sensor and the offset voltage of 0.04 is subtracted from the output. 

An example connection of the ACS 758 current sensor measuring the pack current and connection to Arduino is shown in Figure \ref{halltransfer}. 

\begin{figure}[h!]
\centering
\includegraphics[width=11cm]{./Images/newcurrentmeasurement1.pdf}
\caption{ACS758 current sensor measuring the battery pack current and connected to Arduino.}
\label{halltransfer}
\end{figure}

\subsection{Temperature Measurement}
\label{tempimp}

The temperature sensor as designed in the section \ref{temperaturemeasurementmodule}, is a research topic and is not available as an off the shelf product for implementation. 
Therefore, in this thesis a Thermistor temperature sensor, which has a similar output configuration like RTD, is used as a prototype product for implementation. 
A thermistor is a temperature sensing device whose resistance varies with changes in temperature.
A first order approximation provides the change in resistance $\triangle R$ with respect to change in temperature $\triangle T$ as \cite{therm}

\begin{equation}
\label{firstordertherm}
\triangle R = k \cdot \triangle T
\end{equation} 

\noindent where k is the temperature coefficient of resistance. 
The thermistor could be either a \ac{PTC} device, where the resistance value increases with temperature or could be a \ac{NTC} device, where in the resistance decreases with temperature. 
The change in resistance value is detected by a voltage divider network as shown in Figure \ref{voltagedivider}. 

\begin{figure}[h!]
\centering
\includegraphics[width=10cm]{./Images/tempmeasurement2.pdf}
\caption{Voltage divider network for measuring thermistor resistance.}
\label{voltagedivider}
\end{figure}

\noindent where $R_T$ represents the thermistor resistance and $R_1$ is the balancing resistance in series for measurement and $V_s$ is the supply voltage. 

\noindent The resistance value is calculated as  
\begin{equation}
V_1 = \frac{R_1}{R_T + R_1} \cdot V_s 
\end{equation}
\begin{equation}
\label{therm}
R_T = R_1 \cdot \left(\frac{V_s}{V_1} - 1 \right)
\end{equation}

From equation \ref{therm} the resistance of the thermistor could be calculated and thereby the temperature value. 

The linear approximation explained in equation \ref{firstordertherm} could only be applied over a limited temperature range and beyond that range the variation of resistance is a non-linear function and follows an exponential relationship. 
The exponential behaviour is modelled using Steinhart-Hart equation as 

\begin{align}
\label{SH}
\frac{1}{T} = A + B \cdot ln(R) + C \cdot ln^3(R)
\end{align}

where A, B and C are Steinhart parameters and depends upon the model of the thermistor used. 

The thermistor used for implementation is a \ac{NTC} thermistor with a resistance of 100k$\Omega$ at 25$^\circ$C.
The parameters of the Steinhart equations are provided from the manufacturer datasheet and they are \\
A = $8.271111 \times 10^{-4}$\\
B = $2.088020 \times 10^{-4}$\\
C = $8.059200 \times 10^{-8}$\\

The $V_s$ pin in Figure \ref{voltagedivider} is connected to the 5V supply provided from the Arduino board. 
The $V_1$ pin is connected to the analog read pins of the ATmega328p microcontroller and is converted into digital data by the internal 10 bit ADC. 
$R_1$ is chosen to be the same value of thermistor resistance at 25$^\circ$C, therefore it is 100k$\Omega$. 

The Arduino core is programmed to calculate the thermistor resistance based on equation \ref{therm} as 
\begin{align}
R_T = 100000 \cdot \left( \frac{1024}{\text{DigitalValue}} - 1 \right) 
\end{align}

\noindent where DigitalValue is the digital output from the 10-bit ADC of the Arduino and 1024 is the digital value for a supply of 5 V. 
Then the temperature value is calculated from equation \ref{SH}.  

\section{Cell Balancing Module}  
\label{cellbalimp}
The key contribution to this thesis is, the development of the novel active cell balancing architecture described in section \ref{newcellbalancing}, which can transfer charge between any two cells in the pack without any need of additional wiring between them. 
This active cell balancing transfers the excess charge from the over charged cell to an inductor and then discharges to the cell with a lower charge. 
This novel architecture is proposed to the $50^{th}$ \ac{DAC} to be held in June 2013 and got accepted.

Prior to the implementation of the hardware prototype model of the novel active cell balancing circuit, simulations were done in LTspice IV \cite{engelhardt2008ltspice} and MATLAB \cite{guide1998mathworks} to establish the proof of concept and also in calculating the component parameters required to build the hardware prototype. 
Figure \ref{spiceckt} shows a single cell level block that is modelled in LTspice IV. 

\begin{figure}[h!]
\centering
\includegraphics[width=13cm]{./Images/cellblockltspicefinal.png}
\caption{Schematic of the single cell module modelled in LTspice IV.}
\label{spiceckt}
\end{figure}

To a first order approximation, the cell is modelled as a capacitor with an internal resistance in the simulation. 
The charge stored in a cell is depicted as the initial voltage across the capacitor. 
The capacitance value is calculated from the capacity and voltage of the cell when it is fully charged. For example, a 2.2 Ah cell with a maximum cell voltage of 4.2V, the capacitance is  
\begin{align}
\text{Capacitance B1} &= \frac{2.2 \cdot 3600}{4.2} \\
&= 1885.75 F
\end{align}


The switches in the active cell balancing architecture shown in Figure \ref{cellbalancingcircuit} are implemented with power MOSFETs available in the LTspice IV library. 
The $PWM$ and $\overline{PWM}$ signals that are used to actuate $M_a^1$ and $M_b^1$ for charge transfer are generated with the help of the pulse voltage sources, that are available as standard components in the library. 
The control voltage for turning ON and OFF the isolation and horizontal switches ($M_s^1, M_p^1, M_e^1, M_f^1$)  are generated with the help of the standard DC voltage source from the LTspice IV library. 

The inductance value is calculated based on the balancing current and frequency of operation as 
\begin{align}
 V_L &= L \cdot \frac{\diff I}{\diff T} \\
 &= L \cdot \frac{\diff I}{D \cdot T}\\
 L &= \frac{V_L \cdot D \cdot T}{\diff I} \label{indcal}
\end{align}

where $V_L$ is the voltage across the inductor, dI is the inductor current ripple. $T = T_{ON} + T_{OFF}$ is the time period of the $PWM$ signal and $D = \frac{T_{ON}}{T_{ON} + T_{OFF}}$ is the duty cycle of the $PWM$ signal.  

For example, with an initial cell voltage of 3.6 V and a 50kHz $PWM$ waveform with a 45\% duty cycle, the inductance value for a balancing current of 2.25 A is calculated using equation \ref{indcal} as 
\begin{align}
L &= \frac{3.6 \cdot 0.45 \cdot 20 X 10^{-6}}{2.25}\\
L &= 14.4 \mu H \label{lval}
\end{align}

The balancing current and the time period of the $PWM$ waveform is decided based on the difference in the cell voltages and the balancing time required.
The operation of the circuit could be considered as either continuous mode or discontinuous mode based upon the inductor current. 
If the inductor current falls to zero before the activation of the next $PWM$ \emph{ON} signal, then it is called as Dis-continuous mode of operation.
Instead, if the inductor current is allowed to ramp up and down between a fixed non zero current limits, then it is called as Continuous mode of operation. 

The cell balancing circuit is maintained in Dis-continuous mode of operation in order to minimize the power loss and also to make sure the energy stored in the inductor is completely transferred to the cell. 
To maintain the operation in Dis-continuous mode, the maximum allowable duty cycle of the $PWM$ signal is 50\%. 
With this requirement, the minimum operating frequency of the $PWM$ signal required to provide a Dis-continuous mode of operation for a cell voltage of $V_B^1$, with a balancing current of I and an inductance of $L_1$ could be calculated as explained in \cite{kutkut}

\begin{align}
f_s \hspace{0.2cm} \underline{>} \hspace{0.2cm} \frac{V_B^1}{2 \cdot I \cdot L}
\end{align}

Maintaining the $PWM$ frequency greater than this value would provide a Dis-continuous mode of operation. 


\begin{figure}[h!]
\centering
\includegraphics[width=15cm]{./Images/seriesblockltspice.pdf}
\caption[LTspice simulation of battery pack]{Schematic of the series connection of single cell modules to simulate a battery pack in LTspice IV.}
\label{serbatltspice}
\end{figure}

Figure \ref{serbatltspice} shows an example battery pack consisting of three cells in series, where charge is transferred from cell $B^1$ to cell $B^3$. 
The switches are turned \emph{ON} and \emph{OFF}, according to the switching schematics explained in section \ref{newcellbalancing}. 
The non-overlapping $PWM$ and $\overline{PWM}$ signals for the balancing MOSFETS, are generated with standard pulse voltage sources and the \emph{ON}, \emph{OFF} signals for other switches are produced by the DC voltage sources from the LTspice IV library respectively.
The inductance value is taken from equation \ref{lval} and the $T_{ON}$, $T_{OFF}$ values of the pulse voltage source is calculated assuming a switching frequency of 50 kHz and a duty cycle ratio of 45\%. 
The initial voltages of the cells $B^1$, $B^2$ and $B^3$ are assumed to be 3.6 V, 3.4 V and 3.2 V respectively. 
Switch $M_a^1$ is operated with $PWM$ input and switches $M_b^2$ and $M_a^3$ are activated by $\overline{PWM}$ signal, to allow transfer of charge from cell $B^1$ to cell $B^3$. 
The key advantage of the proposed novel active cell balancing architecture, is that it completely isolates the in-between cell $B^2$ through switches $M_s^2$ and $M_p^2$, such that it is not involved in the charge transfer process. 
This reduces the balancing time and prevents the ageing of cells that are not involved in cell balancing, compared to other architectures \cite{kutkut}. 
 
Upon understanding the circuit behaviour and calculating the individual parameters in the cell level block using LTspice, higher levels of abstraction is done using MATLAB.
Simulation in MATLAB also enables to replace the first order capacitance model of the cell with a real Li-ion battery cell model, available in SimPower system tool box in MATLAB \cite{sybille2001simpower}.
Moreover, the $PWM$ and the $\overline{PWM}$ voltages generated using pulse voltage sources in LTspice IV are replaced with a dedicated $PWM$ generation unit available in the Simscape tool box \cite{miller2008modeling}. 
This block produces pulse width modulation based on the difference in the voltage level of two signals. 
This allows to run control algorithms to manage the cell balancing process between the cells. 
Modelling in MATLAB also makes the simulation faster and easier compared to LTspice IV.
Moreover, it enables to run multiple simulations faster with different set of inputs and also the calculation of power loss and energy of the cells is made easier. 
Also, the modularity of the architecture helps in creating a single cell level block and connecting them in series to form a battery pack. 

\begin{figure}[h!]
\centering
\includegraphics[width=12cm]{./Images/matlabcell.pdf}
\caption[Model of single cell block in MATLAB]{Model of a single cell architecture in MATLAB using a real Li-ion battery model and MOSFETs from SimPowerSystems toolbox.}
\label{cellmatlab}
\end{figure}

A single Li-ion cell with six power MOSFETs is modelled as shown in Figure \ref{cellmatlab}. 
This represents one smart cell in the smart battery pack. 
Many of these smart cell blocks could be connected together to form a smart battery pack. \\



\noindent\textbf{Analytical modelling and Control algorithms}\\
To verify the behaviour of cell balancing in a system level perspective, consisting of 100's of cells connected in series, simulation in LTspice IV and MATLAB would be very much time consuming. 
Therefore, linear and non-linear analytical models of the system are developed, to speed up the simulations for a whole battery pack. 
The equivalent circuits during both the $PWM$ and $\overline{PWM}$ signals are analyzed seperately by formulating differential equations. 
More details regarding the development of the system level model and its validation are explained in section \ref{systemmodel}. 
Also a control algorithm for cell balancing is developed, implemented and validated in \cite{ourpaper}, with the existing state of the art approaches. 
The control strategy could be implemented as two approaches, either $fast$ or $slow$. 
The $fast$ charge balancing algorithm allows concurrent charge transfers between any two cells, as long as their current directions are not overlapping with other charge transfers. 
Balancing time is the prime concern in the fast balancing algorithm and therefore the energy efficiency is slightly lesser compared to that of the $slow$ approach. 
On the other hand, the $slow$ balancing strategy determines the best charge transfer pair of cells, path and performs balancing between this pair in prior to balancing between other pairs. 
Therefore, the slow charge transfer increases the energy efficiency and relaxes the balancing time. 
Performing an initial $slow$ charge transfer between two cells with a huge variation in SOC and doing concurrent $fast$ transfers afterwards would provide an increased performance in both balancing time and energy efficiency.
Results of the control algorithm is presented in section \ref{casestudy}. \\


\noindent\textbf{Hardware Implementation}\\
After analysing the circuit behaviour in LTspice IV and modelling the system with analytical equations, a hardware prototype is built for each of the basic cell level block using standard off the shelf components. 
High power Lithium ion phosphate cells APR18650 provided by A123 systems \cite{A123}, are used in the implementation of the cell balancing prototype. 
The value of inductance is calculated assuming a fixed frequency for the $PWM$ signal as explained in equation \ref{indcal}. 
The cell voltage is read using the Arduino as explained in section \ref{voltageimp} and the $PWM$ waveform is generated with the help of TIMER A of the ATmega328p microcontroller. 
Power MOSFETs from NXP semiconductors PSMN1R6-30PL \cite{mosfet} are used as switches. 
The MOSFETs are controlled by the digital output pins of the Arduino board. 
For each step of the $PWM$ signal, charge is transferred from the over charged cell to the under charged one. 
The controller reads the voltage of the two cells and calculates the difference between them after each step of the $PWM$ signal.
The balancing process was continued until the difference in the cell voltage is reduced to a minimum value of 10mV.
The results are analysed in section \ref{balresults}. 







\section{Power supply Module}

As explained in section \ref{powersupplymodule}, the power supply module is required to provide a constant power supply for the operation of all the individual blocks of ESIC. 
Also the power supply module provides the reference voltage for the A/D converter in the voltage sensing module.
Boost switching regulators are implemented as designed in section \ref{powersupplymodule}, which are efficient in providing a stable output voltage and consuming less power compared to their linear counterparts.
Switching regulators from ON Semiconductors such as NCP1402 is selected to provide voltage regulation for the prototypic model.\\

\noindent\textbf{Features}\\
The device provides a constant output voltage of 5V, with an input signal range of 1 to 4V. 
The converter has an overall fficiency of 85\%. 
The IC contains a Chip Enable(CE) signal, through which it could be disabled when the ESIC is not in operation. 
The operating current is of 30 $\mu$A and when during idle mode the current consumption is 0.6 $\mu$A. 
This reduces power loss to a great extent and prevents draining of the cell. 
The low profile of the IC with very minimal external components(one inductor and two capacitors), makes the NCP1402 as an ideal choice for the implementation of power supply module.\\

\noindent\textbf{Block Diagram}

\begin{figure}[h!]
\centering
\includegraphics[width=11cm]{./Images/stepupblockdia.pdf}
\caption[Block Diagram of NCP1402 Micropower Switching Regulator]{Block Diagram of NCP1402 Micropower Switching Regulator from ON Semiconductors \protect\cite{ncp1402}.}
\label{ncp}
\end{figure}

The block diagram of the IC is shown in Figure \ref{ncp}.
When the voltage on CE pin is higher than 0.9 V, the IC is enabled and the voltage at the output pin rises to 5V, with an input ranging from 1 to 4 V. 
The output voltage is constantly monitored using the resistive divider network $R_1$,$R_2$ and compared with a fixed Voltage Reference output. 
The \ac{PFM} produces variable width pulses as per the difference between the output voltage and the reference voltage. 
The frequency of these pulses are controlled by the \ac{PFM} oscillator and controller. 
The pulse output is used to drive the MOSFET power switch, through the driver to control the output voltage. 
An external inductor is connected between the LX terminal and the input. 
Filter capacitances at both input and output, filters out the ripple components and provides a constant regulation at the output. 
The values of the discrete components (inductor and capacitor) used are calculated based on the equations provided in the datasheet \cite{ncp1402}.
The device works fine with an inductance value of 47$\mu$H. 
Decreasing the inductor value will increase the output current but also increases the ripple voltage, which affects the efficiency of the converter. 
Too high an inductance will improve the regulation but decreases the output current.
Therefore an accurately sized inductor is required to maintain the efficiency and the output current value to meet the specification.
A small surface mount inductor with very low DC resistance is used to limit the power loss. 

\noindent A breakout board for the power converter produced by sparkfun electronics is shown in Figure \ref{breakoutboardpc}.

\begin{figure}[h!]
\centering
\includegraphics[width=5.5cm]{./Images/breakoutboard.png}
\caption[Breakout board for the 5V Step up converter]{Breakout board for the 5V Step up converter \protect\cite{break}.}
\label{breakoutboardpc}
\end{figure}


\section{Communication Module}
As stated in section \ref{communication}, the communication between ESIC could be either via \acf{PLC} or Wireless.
An implementation based on Wireless communication, using Zigbee protocol is implemented for hardware prototype.
Implementation of Zigbee for Battery Management System is presented and analysed in literature \cite{zigbee}.
Zigbee is a low cost, low power consuming wireless interface protocol that also offers synchronization between the sensor nodes \cite{ferrari2008non}. 
Microcontrollers with RF communication chips from Texas Instruments (TI) are used for developing the prototype model. 
MSP430f2274 microcontroller with CC2500 RF Transceivers are available in a single board called as eZ430-RF2500 wireless development tool, is used to test the communication implementation \cite{ez430}.
The kit could be directly powered from the Li-ion battery cell. \\

\noindent Figure \ref{ez430rf2500} shows the wireless development tool provided from TI, that has an USB interface programmer module and a battery pack module for powering the development board. 
 

\begin{figure}[h!]
\centering
\includegraphics[width=8cm]{./Images/ez430_rf2500_cont.png}
\caption[TI wireless development tool]{eZ430-RF2500 wireless development tool with USB interface emulator module and a battery power module from TI \protect\cite{ez430}.}
\label{ez430rf2500}
\end{figure}

The sensor board is connected to the PC via the USB interface emulator module for programming and debugging. 
A propriety low power wireless network stack called SimpliciTI, is available from TI, over which user required functions are added. 
A simple example for testing the proof of concept for communication was successfully implemented, with two sensor nodes attached with the series connected cells.
The sensor nodes are programmed to measure the cell voltage and communicate with each other the measured value. 

\section{Summary}

All the individual modules of ESIC are implemented as per the design outlined in chapter \ref{sec:ESICdesign}. 
The performance of each module is analysed and validated in chapter \ref{results}. 
Table \ref{imp} summarizes the individual implementation techniques employed in this chapter. 

\begin{table}[ht]
\begin{center}
\begin{tabularx}{\linewidth}{ X  X }
\toprule

ESIC Modules & Implementation\\
\midrule
\midrule


Voltage Measurement & Internal 10 bit ADC of the Arduino.\\
\midrule

Current Measurement & ACS758X050B 50 A bidirectional Hall effect current sensor.\\
\midrule

Temperature Measurement & BETATHERM thermistor sensor - 100K6A372I. \\
\midrule

Cell Balancing & Simulations in LTspice, MATLAB and hardware prototype implementation using discrete MOSFETs PSMN1R6-30PL from NXP semiconductors.\\
\midrule

Power supply Module & NCP1402 Micropower Switching regulator from ON semiconductors.\\
\midrule

Communication Module & Wireless communication with eZ430-RF2500 development board from Texas Instruments. \\

\bottomrule

\end{tabularx}
\end{center}
\caption{Summary of Implementation methods for ESIC blocks}
\label{imp}
\end{table}


 







