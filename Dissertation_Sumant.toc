\select@language {ngerman}
\select@language {english}
\select@language {english}
\contentsline {chapter}{Abstract}{ii}{chapter*.1}
\contentsline {chapter}{Acknowledgements}{iii}{chapter*.3}
\contentsline {chapter}{List of Tables}{vi}{chapter*.5}
\contentsline {chapter}{List of Figures}{viii}{chapter*.6}
\contentsline {chapter}{List of Acronyms}{viii}{chapter*.7}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Battery Management System (BMS)}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Cell Balancing}{1}{subsection.1.1.1}
\contentsline {section}{\numberline {1.2}What is PWM and why is it used?}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}PWM Signals for cell balancing}{4}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Why PWM is required and how is it useful?}{4}{subsection.1.3.1}
\contentsline {section}{\numberline {1.4}Complexity in generating PWM}{4}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Complex Overlapping Signals}{5}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Computational effort}{5}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Power Consumption}{5}{subsection.1.4.3}
\contentsline {subsection}{\numberline {1.4.4}IO ports saturation}{5}{subsection.1.4.4}
\contentsline {subsection}{\numberline {1.4.5}Extensibility}{5}{subsection.1.4.5}
\contentsline {section}{\numberline {1.5}Contributions}{5}{section.1.5}
\contentsline {section}{\numberline {1.6}Organization}{6}{section.1.6}
\contentsline {chapter}{\numberline {2}Related Work}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Cell Balancing and PWM}{7}{section.2.1}
\contentsline {chapter}{\numberline {3}Idea and Architecture}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}Idea}{12}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}System Architecture}{13}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Hardware Architecture}{13}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}BMS Controller}{13}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}PWM Controller}{14}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Communication Bus}{14}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Cell Balancing HW}{14}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Software Architecture}{16}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Application Layer}{17}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Middleware Layer}{17}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Driver Layer}{17}{subsection.3.3.3}
\contentsline {chapter}{\numberline {4}Design and Implementation}{18}{chapter.4}
\contentsline {section}{\numberline {4.1}Overall System Design}{18}{section.4.1}
\contentsline {section}{\numberline {4.2}Hardware}{19}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}STM32E407 Olimex development board}{20}{subsection.4.2.1}
\contentsline {subsubsection}{Features}{20}{section*.29}
\contentsline {subsection}{\numberline {4.2.2}Microcontroller STM32F407ZGT6}{21}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Software}{21}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Command Structure and PWM Channels}{22}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Application Layer}{22}{subsection.4.3.2}
\contentsline {subsubsection}{Application Program Interfaces (APIs)}{23}{section*.32}
\contentsline {subsubsection}{Implementation}{23}{section*.34}
\contentsline {subsubsection}{Implementation}{25}{section*.38}
\contentsline {subsubsection}{Implementation}{28}{section*.43}
\contentsline {subsubsection}{Implementation}{30}{section*.47}
\contentsline {subsection}{\numberline {4.3.3}Middleware Layer}{31}{subsection.4.3.3}
\contentsline {subsubsection}{Encoding}{31}{section*.49}
\contentsline {subsubsection}{Design}{31}{section*.50}
\contentsline {subsubsection}{Implementation}{32}{section*.52}
\contentsline {subsubsection}{Decoding}{32}{section*.53}
\contentsline {subsubsection}{Implementation}{32}{section*.54}
\contentsline {subsubsection}{Storing the information}{32}{section*.55}
\contentsline {subsubsection}{Design}{32}{section*.56}
\contentsline {subsubsection}{Implementation}{32}{section*.58}
\contentsline {subsection}{\numberline {4.3.4}Driver Layer}{34}{subsection.4.3.4}
\contentsline {subsubsection}{PWM Initialization}{34}{section*.60}
\contentsline {subsubsection}{Design}{34}{section*.61}
\contentsline {subsubsection}{Implementation}{34}{section*.62}
\contentsline {subsubsection}{PWM Signals Generation}{35}{section*.63}
\contentsline {subsubsection}{Design}{35}{section*.64}
\contentsline {subsubsection}{Implementation}{35}{section*.65}
\contentsline {subsection}{\numberline {4.3.5}Command Acceptance and Acknowledgement}{37}{subsection.4.3.5}
\contentsline {subsubsection}{Design}{37}{section*.67}
\contentsline {subsubsection}{Implementation}{37}{section*.69}
\contentsline {subsection}{\numberline {4.3.6}Stop PWM}{38}{subsection.4.3.6}
\contentsline {subsubsection}{Design}{38}{section*.70}
\contentsline {subsubsection}{Implementation}{39}{section*.72}
\contentsline {chapter}{\numberline {5}Results and Verifications}{40}{chapter.5}
\contentsline {section}{\numberline {5.1}Experimental Setup}{40}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Setting up the SPI}{41}{subsection.5.1.1}
\contentsline {section}{\numberline {5.2}Communication level}{42}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Sender}{42}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Receiver}{42}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Communication channel}{42}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Methods}{42}{subsection.5.2.4}
\contentsline {subsubsection}{Results and verifications}{44}{section*.77}
\contentsline {section}{\numberline {5.3}System level}{44}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Methods}{44}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Results and verifications}{48}{subsection.5.3.2}
\contentsline {section}{\numberline {5.4}Summary and limitation}{48}{section.5.4}
\contentsline {chapter}{\numberline {6}Conclusion}{52}{chapter.6}
\contentsline {chapter}{Bibliography}{54}{section*.81}
